<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ru_comments".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $name
 * @property string $email
 * @property int|null $hotel_id
 * @property string $message
 * @property string|null $date
 * @property int|null $parent
 * @property int|null $tour_id
 * @property int|null $approved
 *
 * @property RuHotel $hotel
 * @property RuTour $tour
 * @property User $user
 */
class Comments extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ru_comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'hotel_id', 'tour_id'], 'default', 'value' => null],
            [['user_id', 'hotel_id', 'parent', 'tour_id', 'approved'], 'integer'],
            [['parent', 'approved'], 'default', 'value' => 0],
            [['name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            [['date'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'hotel_id' => 'Hotel ID',
            'message' => 'Message',
            'date' => 'Date',
            'parent' => 'Parent',
            'tour_id' => 'Tour ID',
            'approved' => 'Approved',
        ];
    }

    /**
     * Gets query for [[Hotel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }

    /**
     * Gets query for [[Tour]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getChild(){
        return $this->hasMany(self::className(), ['parent' => 'id'])
            ->onCondition(['approved' => 1]);
    }
}
