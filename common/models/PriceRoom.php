<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "price_room".
 *
 * @property int $id
 * @property int $room_id
 * @property int|null $count_people
 * @property int|null $count_child
 * @property int|null $count_day
 * @property string|null $date_from
 * @property string|null $date_to
 * @property float|null $price
 *
 * @property Room $room
 */
class PriceRoom extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'price_room';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id'], 'required'],
            [['room_id', 'room_id', 'count_people', 'services', 'count_child', 'count_day'], 'default', 'value' => null],
            [['room_id', 'room_id', 'count_people', 'count_child', 'count_day', 'hotel_id', 'type_room'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['price'], 'number'],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Room ID',
            'count_people' => 'Count People',
            'count_child' => 'Count Child',
            'count_day' => 'Count Day',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'price' => 'Price',
        ];
    }

    /**
     * Gets query for [[Room]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }
}
