<?php

namespace common\models;

use common\components\behavior\PgArrayBehavior;
use common\components\behavior\UploadBehavior;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "ru_hotel".
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $site
 * @property string|null $email
 * @property string|null $description
 * @property string|null $body
 * @property string|null $url
 * @property string|null $image
 * @property string|null $images
 * @property int $user_id
 * @property int|null $type_id
 * @property int|null $category_id
 * @property int|null $discount
 * @property string|null $date_create
 * @property int|null $country_id
 * @property int|null $region_id
 * @property int|null $city_id
 * @property int|null $min_people
 * @property int|null $max_people
 * @property int|null $minimum_quantity
 * @property float|null $deposite
 * @property string|null $extra
 * @property int|null $type_hotel
 * @property int|null $identify
 * @property int|null $included
 * @property string|null $excluded
 * @property string|null $fax
 * @property string|null $toll_free
 * @property string|null $certification
 * @property string|null $video
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 *
 * @property RuCategory $category
 * @property RuCity $city
 * @property RuCountry $category0
 * @property RuRegion $region
 * @property RuType $type
 * @property User $user
 */
class Hotel extends MyActiveRecord
{

    public $minPrice;
    public $rating;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'hotel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'category_id', 'region_id'], 'required'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['type_id', 'category_id', 'discount', 'country_id', 'region_id', 'city_id', 'min_age', 'max_age', 'minimum_quantity', 'type_hotel', 'identify'], 'default', 'value' => null],
            [['services', 'excluded'], 'each', 'rule' => ['integer']],
            [['included', 'payment_methods'], 'each', 'rule' => ['in', 'range' => array_keys(Yii::$app->params['included'])]],
            [['type_id', 'discount', 'country_id', 'region_id', 'city_id', 'min_age', 'max_age', 'minimum_quantity'], 'integer'],
            [['body', 'seo_text'], 'string'],
            [['deposite'], 'number'],
            ['images', 'isArrayUploadImage'],
            ['image', 'isUploadImage'],
            [['title'], 'unique'],
            [['url'], 'unique'],
            [['email'], 'unique'],
            [['address'], 'string', 'max' => 255],
            [['address', 'phone', 'site', 'email', 'description', 'url', 'extra', 'fax', 'toll_free', 'certification', 'video', 'h1', 'meta_title', 'meta_description', 'established', 'identify'], 'string', 'max' => 255],
//            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            ['user_id', 'default', 'value' => Yii::$app->user->id],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименования санатория',
            'Established' => 'Год открытия',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'site' => 'Сайт',
            'email' => 'Email',
            'description' => 'Описание',
            'body' => 'Текст',
            'url' => 'Url',
            'image' => 'Логотип',
            'images' => 'Фото',
            'type_id' => 'Тип отеля',
            'category_id' => 'Категории',
            'discount' => 'Скидка',
            'date_create' => 'Date Create',
            'country_id' => 'Страна',
            'region_id' => 'Регион',
            'city_id' => 'Населённый пункт',
            'min_age' => 'Возможность отдыха с детьми',
            'max_age' => 'Максимальный возраст',
            'minimum_quantity' => 'Минимальное количество',
            'deposite' => 'Предоплата',
            'extra' => 'Дополнительно',
            'type_hotel' => 'Тип отеля',
            'identify' => 'Идентифицирование',
            'included' => 'Включено',
            'excluded' => 'Исключено',
            'payment_methods' => 'Методы оплаты',
            'fax' => 'Факс',
            'toll_free' => 'Бесплатно',
            'certification' => 'Сертификат',
            'video' => 'Видео Youtube',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function getRoom(){
        return $this->hasMany(Room::className(), ['id_hotel' => 'id']);
    }

    public function getPriceRoom(){
        return $this->hasMany(PriceRoom::className(), ['room_id' => 'id'])
            ->via('room');
    }
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getLocation(){
        return $this->city ? $this->city->title : ($this->region ? $this->region->title : $this->country ? $this->country->title : null);
    }

    public function getCategories(){
        return $this->hasMany(Category::className(), ['id' => 'category_id']);
    }

    public function getCountComments(){
        return \common\models\Comments::find()->where(['hotel_id' => $this->id])->count();
    }
}
