<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tour_program".
 *
 * @property int $id
 * @property string|null $title
 * @property int $tour_id
 * @property string|null $image
 * @property string|null $description
 * @property int|null $order
 *
 * @property RuTour $tour
 */
class TourProgram extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'tour_program';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id'], 'required'],
            [['tour_id', 'order'], 'default', 'value' => null],
            [['tour_id', 'order'], 'integer'],
            [['description'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'tour_id' => 'Tour ID',
            'image' => 'Image',
            'description' => 'Description',
            'order' => 'Order',
        ];
    }

    /**
     * Gets query for [[Tour]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(RuTour::className(), ['id' => 'tour_id']);
    }
}
