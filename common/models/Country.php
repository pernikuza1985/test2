<?php

namespace common\models;

use common\components\behavior\UploadBehavior;
use Yii;
use yii\web\UploadedFile;
use yii\base\Model;
use yii\helpers\Inflector;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $title
 * @property string|null $url
 * @property string|null $description
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 */
class Country extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        $table = 'country';
        return self::getLanguage().$table;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['seo_text'], 'string'],
            [['title', 'description', 'h1', 'meta_title', 'meta_description'], 'string', 'max' => 255],
            [['title', 'description', 'h1', 'meta_title', 'meta_description', 'url', 'image'], 'safe'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['title'], 'unique'],
            [['url'], 'unique'],
            ['image', 'isUploadImage'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'description' => 'Description',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
        ];
    }

}