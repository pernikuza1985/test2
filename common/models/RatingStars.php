<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ru_rating_stars".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $hotel_id
 * @property int|null $tour_id
 * @property string|null $date_create
 * @property int $count_stars
 *
 * @property Hotel $hotel
 * @property Tour $tour
 * @property User $user
 */
class RatingStars extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ru_rating_stars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'count_stars'], 'required'],
            [['user_id', 'hotel_id', 'tour_id', 'count_stars'], 'default', 'value' => null],
            [['user_id', 'hotel_id', 'tour_id', 'count_stars'], 'integer'],
            [['date_create'], 'safe'],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'hotel_id' => 'Hotel ID',
            'tour_id' => 'Tour ID',
            'date_create' => 'Date Create',
            'count_stars' => 'Count Stars',
        ];
    }

    /**
     * Gets query for [[Hotel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(RuHotel::className(), ['id' => 'hotel_id']);
    }

    /**
     * Gets query for [[Tour]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(RuTour::className(), ['id' => 'tour_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
