<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tour_places".
 *
 * @property int $id
 * @property string $title
 * @property string|null $images
 */
class TourPlaces extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'tour_places';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            ['images', 'isArrayUploadImage'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'images' => 'Images',
        ];
    }
}
