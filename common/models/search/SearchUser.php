<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserCity represents the model behind the search form of `common\models\User`.
 */
class SearchUser extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role', 'city_id', 'country_id', 'postal_code', 'status', 'region_id'], 'integer'],
            [['user_name', 'first_name', 'last_name', 'website', 'password', 'facebook', 'linkedin', 'instagram', 'phone', 'info', 'image', 'company', 'email_subscribe', 'address_main', 'address_mail', 'date_create', 'date_last_login', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'city_id' => $this->city_id,
            'country_id' => $this->country_id,
            'postal_code' => $this->postal_code,
            'status' => $this->status,
            'date_create' => $this->date_create,
            'date_last_login' => $this->date_last_login,
            'region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['ilike', 'user_name', $this->user_name])
            ->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'website', $this->website])
            ->andFilterWhere(['ilike', 'password', $this->password])
            ->andFilterWhere(['ilike', 'facebook', $this->facebook])
            ->andFilterWhere(['ilike', 'linkedin', $this->linkedin])
            ->andFilterWhere(['ilike', 'instagram', $this->instagram])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'info', $this->info])
            ->andFilterWhere(['ilike', 'image', $this->image])
            ->andFilterWhere(['ilike', 'company', $this->company])
            ->andFilterWhere(['ilike', 'email_subscribe', $this->email_subscribe])
            ->andFilterWhere(['ilike', 'address_main', $this->address_main])
            ->andFilterWhere(['ilike', 'address_mail', $this->address_mail])
            ->andFilterWhere(['ilike', 'email', $this->email]);

        return $dataProvider;
    }
}
