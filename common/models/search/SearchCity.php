<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\City;

/**
 * SearchCity represents the model behind the search form of `common\models\City`.
 */
class SearchCity extends City
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'region_id'], 'integer'],
            [['title', 'url', 'description', 'h1', 'meta_title', 'meta_description', 'seo_text', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'url', $this->url])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'h1', $this->h1])
            ->andFilterWhere(['ilike', 'meta_title', $this->meta_title])
            ->andFilterWhere(['ilike', 'meta_description', $this->meta_description])
            ->andFilterWhere(['ilike', 'seo_text', $this->seo_text])
            ->andFilterWhere(['ilike', 'image', $this->image]);

        return $dataProvider;
    }
}
