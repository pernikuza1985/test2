<?php

namespace common\models\search;

use common\models\Hotel;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Room;

/**
 * SearchRoom represents the model behind the search form of `common\models\Room`.
 */
class SearchRoom extends Room
{
    public $hotel;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'services', 'id_hotel', 'user_id'], 'integer'],
            [['title', 'hotel', 'images', 'url'], 'safe'],
            [['id_room_type', 'price', 'count_room'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Room::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_room_type' => $this->id_room_type,
            'services' => $this->services,
            'id_hotel' => $this->id_hotel,
            'user_id' => $this->user_id,
            'price' => $this->price,
            'count_room' => $this->count_room,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'images', $this->images])
            ->andFilterWhere(['ilike', 'url', $this->url]);

        if($this->hotel){
            $hotel = $this->hotel;
            $query->joinWith([
                'hotel' => function ($query) use ($hotel) {
                    /** @var yii\db\ActiveQuery $query */
                    $query->andWhere(Hotel::tableName() . ".title LIKE '%".$hotel."%'");
                }
            ]);
        }


        return $dataProvider;
    }
}
