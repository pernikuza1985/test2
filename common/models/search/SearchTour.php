<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tour;

/**
 * SearchTour represents the model behind the search form of `common\models\Tour`.
 */
class SearchTour extends Tour
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'discount', 'country_id', 'region_id', 'city_id', 'min_age', 'max_age', 'minimum_quantity', 'included', 'excluded', 'type_id', 'category_id', 'services', 'payment_methods'], 'integer'],
            [['address', 'phone', 'site', 'email', 'description', 'body', 'url', 'image', 'date_create', 'required', 'trip', 'identify', 'fax', 'toll_free', 'certification', 'video', 'h1', 'meta_title', 'meta_description', 'seo_text', 'title', 'established', 'images'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tour::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'discount' => $this->discount,
            'date_create' => $this->date_create,
            'country_id' => $this->country_id,
            'region_id' => $this->region_id,
            'city_id' => $this->city_id,
            'min_age' => $this->min_age,
            'max_age' => $this->max_age,
            'minimum_quantity' => $this->minimum_quantity,
            'included' => $this->included,
            'excluded' => $this->excluded,
            'type_id' => $this->type_id,
            'category_id' => $this->category_id,
            'services' => $this->services,
            'payment_methods' => $this->payment_methods,
        ]);

        $query->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'site', $this->site])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'body', $this->body])
            ->andFilterWhere(['ilike', 'url', $this->url])
            ->andFilterWhere(['ilike', 'image', $this->image])
            ->andFilterWhere(['ilike', 'required', $this->required])
            ->andFilterWhere(['ilike', 'trip', $this->trip])
            ->andFilterWhere(['ilike', 'identify', $this->identify])
            ->andFilterWhere(['ilike', 'fax', $this->fax])
            ->andFilterWhere(['ilike', 'toll_free', $this->toll_free])
            ->andFilterWhere(['ilike', 'certification', $this->certification])
            ->andFilterWhere(['ilike', 'video', $this->video])
            ->andFilterWhere(['ilike', 'h1', $this->h1])
            ->andFilterWhere(['ilike', 'meta_title', $this->meta_title])
            ->andFilterWhere(['ilike', 'meta_description', $this->meta_description])
            ->andFilterWhere(['ilike', 'seo_text', $this->seo_text])
            ->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'established', $this->established])
            ->andFilterWhere(['ilike', 'images', $this->images]);

        return $dataProvider;
    }
}
