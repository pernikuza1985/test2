<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "complaint".
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $email
 * @property string $message
 * @property int|null $type
 */
class Complaint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'complaint';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'email'], 'required'],
            [['message'], 'string'],
            [['type'], 'default', 'value' => null],
            [['type'], 'integer'],
            ['type', 'in', 'range' => array_keys(Yii::$app->params['type-complaint'])],
            ['email', 'email'],
            [['url', 'email'], 'string', 'max' => 255],
            ['url', 'match', 'pattern' => '/^(http|https):\/\/'.$_SERVER['HTTP_HOST'].'+/i']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'email' => 'Email',
            'message' => 'Message',
            'type' => 'Type',
        ];
    }
}