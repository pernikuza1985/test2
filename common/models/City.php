<?php

namespace common\models;

use common\components\behavior\UploadBehavior;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string|null $description
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 * @property string|null $image
 * @property int $country_id
 * @property int|null $region_id
 *
 * @property Country $country
 * @property Region $region
 */
class City extends MyActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        $table = self::getLanguage().'city';
        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'country_id'], 'required'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['seo_text'], 'string'],
            [['region_id'], 'default', 'value' => null],
            [['country_id', 'region_id'], 'integer'],
            [['title', 'description', 'h1', 'meta_title', 'meta_description'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['url'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            ['image', 'isUploadImage'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'description' => 'Description',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
            'image' => 'Image',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
        ];
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
