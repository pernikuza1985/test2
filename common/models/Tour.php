<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "ru_tour".
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $site
 * @property string|null $email
 * @property string|null $description
 * @property string|null $body
 * @property string|null $url
 * @property string|null $image
 * @property int $user_id
 * @property int|null $discount
 * @property string|null $date_create
 * @property int|null $country_id
 * @property int|null $region_id
 * @property int|null $city_id
 * @property int|null $min_people
 * @property int|null $max_people
 * @property int|null $minimum_quantity
 * @property string|null $required
 * @property string|null $trip
 * @property string|null $identify
 * @property string|null $fax
 * @property string|null $toll_free
 * @property string|null $certification
 * @property string|null $video
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 * @property string $title
 * @property int|null $included
 * @property int|null $excluded
 * @property int|null $type_id
 * @property int|null $category_id
 * @property string|null $established
 * @property int|null $services
 * @property int|null $payment_methods
 * @property string|null $images
 *
 * @property RuCity $city
 * @property RuRegion $region
 * @property User $user
 */
class Tour extends MyActiveRecord
{
    public $places_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'tour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body', 'seo_text', 'landing'], 'string'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['title', 'category_id', 'region_id'], 'required'],
            [['user_id', 'discount', 'country_id', 'region_id', 'city_id', 'min_age', 'max_age', 'minimum_quantity', 'included', 'excluded', 'type_id', 'category_id', 'services', 'payment_methods', 'landing'], 'default', 'value' => null],
            [['user_id', 'discount', 'country_id', 'region_id', 'city_id', 'min_age', 'max_age', 'minimum_quantity', 'type_id'], 'integer'],
            [['date_create'], 'safe'],
            [['address', 'phone', 'site', 'email', 'description', 'url', 'image', 'trip', 'fax', 'toll_free', 'certification', 'video', 'h1', 'meta_title', 'meta_description', 'title', 'established'], 'string', 'max' => 255],
            [['required'], 'string', 'max' => 24],
            [['identify'], 'string', 'max' => 64],
            ['images', 'isArrayUploadImage'],
            ['image', 'isUploadImage'],
            ['places_id', 'validPlaces'],
            [['title'], 'unique'],
            [['url'], 'unique'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            ['user_id', 'default', 'value' => Yii::$app->user->id],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'phone' => 'Phone',
            'site' => 'Site',
            'email' => 'Email',
            'description' => 'Description',
            'body' => 'Body',
            'url' => 'Url',
            'image' => 'Image',
            'user_id' => 'User ID',
            'discount' => 'Discount',
            'date_create' => 'Date Create',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'city_id' => 'City ID',
            'min_age' => 'Min age',
            'max_age' => 'Max age',
            'minimum_quantity' => 'Minimum Quantity',
            'required' => 'Required',
            'trip' => 'Trip',
            'identify' => 'Identify',
            'fax' => 'Fax',
            'toll_free' => 'Toll Free',
            'certification' => 'Certification',
            'video' => 'Video',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
            'title' => 'Title',
            'included' => 'Included',
            'excluded' => 'Excluded',
            'type_id' => 'Type ID',
            'category_id' => 'Category ID',
            'established' => 'Established',
            'services' => 'Services',
            'payment_methods' => 'Payment Methods',
            'images' => 'Images',
            'landing' => 'Место отправки',
        ];
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'region_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function validPlaces(){
        $places = explode('|',$this->places_id);
        $badPlaces = [];
        $goodPlaces = [];
        foreach ($places as $place){
            $place = trim($place);
            if (!$place){
                continue;
            }
            $model = TourPlaces::find()->where(['title' => $place])->one();
            if(!$model){
                $badPlaces[] = $place;
            } else {
                $goodPlaces[] = $model->id;
            }
        }
        if(!empty($badPlaces)){
            $this->addError('places', 'Не верные места туров - '.implode(',', $badPlaces));
        } else {
            $this->places_id = $goodPlaces;
            return true;
        }
    }

    public function getPlaces(){
        return $this->hasMany(TourPlaces::className(), ['id' => 'places_id']);
    }
}
