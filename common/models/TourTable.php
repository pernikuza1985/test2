<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tour_table".
 *
 * @property int $id
 * @property int|null $tour_type
 * @property int|null $tour_price_type
 * @property float|null $price
 * @property float|null $price_old
 * @property int $tour_id
 * @property string|null $description
 *
 * @property Tour $tour
 */
class TourTable extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'tour_table';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_type', 'tour_price_type', 'tour_id'], 'default', 'value' => null],
            [['tour_type', 'tour_price_type', 'tour_id'], 'integer'],
            [['price', 'price_old'], 'number'],
            [['tour_id'], 'required'],
            [['date'], 'safe'],
            [['description'], 'string', 'max' => 255],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_type' => 'Tour Type',
            'tour_price_type' => 'Tour Price Type',
            'price' => 'Price',
            'price_old' => 'Price Old',
            'tour_id' => 'Tour ID',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Tour]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }
}
