<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "ru_news".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $url
 * @property string|null $image
 * @property string|null $description
 * @property string|null $date_create
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 */
class News extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'meta_description', 'seo_text'], 'string'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['date_create'], 'safe'],
            [['title', 'url', 'image', 'h1', 'meta_title'], 'string', 'max' => 255],
            [['url'], 'unique'],
            ['image', 'isUploadImage'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'image' => 'Image',
            'description' => 'Description',
            'date_create' => 'Date Create',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
        ];
    }
}
