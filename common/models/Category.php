<?php

namespace common\models;

use common\components\behavior\UploadBehavior;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "ru_category".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $url
 * @property string|null $image
 * @property string|null $description
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 */
class Category extends MyActiveRecord
{
    public $toursData;
    public $hotelsData;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'seo_text', 'url'], 'string'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['title'], 'unique'],
            [['url'], 'unique'],
            [['title', 'url', 'h1', 'meta_title', 'meta_description'], 'string', 'max' => 255],
            ['image', 'isUploadImage'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'image' => 'Image',
            'description' => 'Description',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
        ];
    }

    public function getHotels(){
        return $this->hotelsData ? $this->hotelsData : $this->hotelsData =
         Hotel::find()
            ->where($this->id.' = ANY(category_id)')
            ->orderBy('title')
            ->all();
    }

    public function getTours(){
        return $this->toursData ? $this->toursData : $this->toursData =
        Tour::find()
            ->where($this->id.' = ANY(category_id)')
            ->orderBy('title')
            ->all();
    }

    public function getRegionsHotel(){
        return Region::find()
            ->select(Region::tableName().'.*')
            ->join('JOIN', Hotel::tableName(), $this->id.' = ANY(category_id)')
            ->where($this->id.' = ANY(category_id)')
            ->orderBy('title')
            ->groupBy(Region::tableName().'.id')
            ->all();
    }

    public function getRegionsTour(){
        return Region::find()
            ->select(Region::tableName().'.*')
            ->join('JOIN', Tour::tableName(), $this->id.' = ANY(category_id)')
            ->where($this->id.' = ANY(category_id)')
            ->orderBy('title')
            ->groupBy(Region::tableName().'.id')
            ->all();
    }

}
