<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;
use common\models\PriceRoom;

/**
 * This is the model class for table "ru_room".
 *
 * @property int $id
 * @property string $title
 * @property float|null $id_room_type
 * @property int|null $services
 * @property int|null $id_hotel
 * @property int|null $user_id
 * @property string|null $images
 * @property float|null $price
 * @property float|null $old_price
 * @property string $url
 *
 * @property User $user
 */
class Room extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'room';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'id_hotel'], 'required'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['id_room_type', 'services', 'user_id', 'id_hotel', 'description'], 'default', 'value' => null],
            [['id_room_type', 'user_id', 'id_hotel'], 'integer'],
            ['images', 'isArrayUploadImage'],
            ['image', 'isUploadImage'],
            [['price', 'count_room'], 'number'],
            [['title', 'url'], 'string', 'max' => 255],
            [['title', 'id_hotel'], 'unique', 'targetAttribute' => ['title', 'id_hotel']],
            [['url', 'id_hotel'], 'unique', 'targetAttribute' => ['url', 'id_hotel']],
            [['id_hotel'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['id_hotel' => 'id']],
            [['id_room_type'], 'exist', 'skipOnError' => true, 'targetClass' => RoomType::className(), 'targetAttribute' => ['id_room_type' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'id_room_type' => 'Room Type',
            'services' => 'Services',
            'user_id' => 'User',
            'images' => 'Images',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'url' => 'Url',
            'id_hotel' => 'Hotel',
            'count_room' => 'Количество комнат',
        ];
    }

    /**
     * Gets query for [[Hotel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'id_hotel']);
    }
    /**
     * Gets query for [[RoomType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoomType()
    {
        return $this->hasOne(RoomType::className(), ['id' => 'id_room_type']);
    }
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

//
//    public function getPriceRoom(){
//        return $this->hasOne(PriceRoom::className(), ['room_id' => 'id'])
//            ->min('price');
//    }
}
