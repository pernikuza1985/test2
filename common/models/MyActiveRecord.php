<?php


namespace common\models;

use \yii\db\ActiveRecord ;
class MyActiveRecord extends ActiveRecord
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function isArrayUploadImage($attr){
        $val = $this->$attr;
        $images = [];
        if(is_array($val) ){
            foreach ($val as $key => $image){
                if($image){
                    $file = rawurldecode(\Yii::$app->params['uploadDir'].$image);
                    $size = getimagesize($file);
                    if(strtolower(substr($size['mime'], 0, 5)) == 'image') {
                        $images[] = $image;
                    }
                }
            }
        }

        $this->$attr = $images;
        return true;
    }

    public function isUploadImage($attr){
        $val = $this->$attr;
        $image = '';
        if(is_string($val) ){
            $file = rawurldecode(\Yii::$app->params['uploadDir'].$val);
            if(!file_exists($file)){
                $this->$attr = "";
                return true;
            }
            $size = getimagesize($file);
            if(strtolower(substr($size['mime'], 0, 5)) == 'image') {
                $image = $val;
            }
        }
        $this->$attr = $image;
        return true;
    }



    public function validateFile($attribute, $params)
    {
        if($this->$attribute && in_array($this->$attribute->extension, $params)) {
            $this->$attribute = time().'.'.$this->$attribute->extension;
            return true;
        } else {
            $this->addError($attribute, 'Не верный тип файла, только '.implode(',', $params));
        }
    }

    static public function getLanguage(){
        if(!empty(\Yii::$app->language)){
            return \Yii::$app->language.'_';
        }
        return '';
    }

    public function getTree($types = null, $parent = 0, $onlyParent = false)
    {
        $result = array();
        foreach ($types as $type) {
            if ($type->parent_id == $parent) {
                $result[$type->id] = $type->attributes;
                if (!$onlyParent) {
                    $result[$type->id]['children'] = $this->getTree($types, $type->id, false);
                }
            }

        }
        return $result;
    }
}