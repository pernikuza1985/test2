<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property string $date_from
 * @property string $date_to
 * @property int|null $adults
 * @property int|null $children
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'date_from', 'date_to'], 'required'],
            [['date_from', 'date_to'], 'safe'],
            [['adults', 'children'], 'default', 'value' => null],
            [['adults', 'children', 'hotel_id'], 'integer'],
            [['name', 'email', 'phone', 'table_hotel'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'adults' => 'Adults',
            'children' => 'Children',
        ];
    }
}
