<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ru_hotel_question_answer".
 *
 * @property int $id
 * @property string $question
 * @property string $answer 
 */
class HotelQuestionAnswer extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'hotel_question_answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'question', 'answer'], 'required'],
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['answer'], 'string'],
            [['question'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
        ];
    }
}
