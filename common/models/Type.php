<?php

namespace common\models;

use common\components\behavior\UploadBehavior;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $url
 * @property string|null $image
 * @property string|null $description
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 */
class Type extends MyActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attributes' => ['image'],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'seo_text', 'url'], 'string'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['title', 'url', 'image', 'h1', 'meta_title', 'meta_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'image' => 'Image',
            'description' => 'Description',
            'h1' => 'H1',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'seo_text' => 'Seo Text',
        ];
    }
}
