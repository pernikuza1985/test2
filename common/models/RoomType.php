<?php

namespace common\models;

use common\components\behavior\UploadBehavior;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "ru_room_type".
 *
 * @property int $id
 * @property string $title
 * @property string|null $image
 */
class RoomType extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'room_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['title'], 'required'],
            ['image', 'isUploadImage'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
        ];
    }

    public function getRooms(){
        return $this->hasMany(Room::className(), ['id_room_type' => 'id']);
    }
}
