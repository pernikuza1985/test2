<?php

namespace common\models;

use common\models\Country;
use Yii;
use common\components\behavior\UploadBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "region".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string|null $description
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $seo_text
 * @property string|null $image
 * @property int $country_id
 *
 * @property City[] $cities
 * @property Country $country
 */
class Region extends MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::getLanguage().'region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'country_id'], 'required'],
            [['url'], 'default', 'value' => function ($value) {
                return Inflector::slug($this->title);
            }],
            [['seo_text'], 'string'],
            [['country_id'], 'default', 'value' => null],
            [['country_id'], 'integer'],
            [['title', 'description', 'h1', 'meta_title', 'meta_description'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['url'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            ['image', 'isUploadImage'],
//            [['image'], 'validateFile', 'params' => ['png', 'jpg', 'jpeg', 'gif']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'description' => Yii::t('app', 'Description'),
            'h1' => Yii::t('app', 'H1'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'seo_text' => Yii::t('app', 'Seo Text'),
            'image' => Yii::t('app', 'Image'),
            'country_id' => Yii::t('app', 'Country ID'),
        ];
    }

    /**
     * Gets query for [[Cities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['region_id' => 'id']);
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
