<?php
return [
    'Зайти в аккаунт' => 'Sign In Account',
    'Пароль' => 'Password',
    'Зайти' => 'Login',
    'Нет аккаунта?' => "Don't have account?",
    'Регистрация' => "Sign UP",
    'Забыл пароль' => "Forgot Password",
    'Зарегистрироваться' => "Sign Up Account",
    'Уже есть аккаунт?' => "Already have an account?",
    'Войти' => "Sign In",
];