<?php
namespace common\components\behavior;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\pgsql\ArrayParser;
use yii\db\pgsql\ArrayExpressionBuilder;

class PgArrayBehavior extends Behavior
{
    /**
     * @var string Тип массива Integer/text
     */
    public $type = 'int';
    /**
     * @var null массив полей котрые
     */
    public $arrayAttributes = null;
    /**
     * @var null Если attributes не имеет значений берем значение от этого атрибута
     */
    public $defaultAttribute = null;


    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
//            ActiveRecord::EVENT_AFTER_INSERT =>  'afterSave',
//            ActiveRecord::EVENT_AFTER_UPDATE =>  'afterSave',
        ];
    }


    public function beforeInsert($event){
        $this->beforeSave();
    }

    public function beforeUpdate($event){
        $this->beforeSave();
    }
    /**
     * Преобразует  массив php в массив PostgreSQL перед сохранением
     * @return bool
     */
    public function beforeSave(){
        $this->owner->payment_methods = 'ARRAY[25000,25000,27000,27000]::int[]';
        $this->owner->title = '{1,5}';
        if(is_array($this->arrayAttributes) && !empty($this->arrayAttributes)){
//            die(pr($this->owner->attributes));
            foreach($this->arrayAttributes as $nameAttribute => $type){
                $data = $this->owner->$nameAttribute;
                $this->owner->$nameAttribute = '{1,5}';
//                if(is_array($data) && count($data) > 0){
//                    foreach($data as $key => $value){
//                        if(empty($value)){
//                            unset($data[$key]);
//                        }
//                    }
//                    if(!isset($data) || empty($data)){
//                        $defaultAttribute = $this->defaultAttribute;
//                        $data = $this->defaultAttribute ? array($this->owner->$defaultAttribute) : array();
//                    }
//                } elseif($this->defaultAttribute){
//                    $defaultAttribute = $this->defaultAttribute;
//                    $data = array($this->owner->$defaultAttribute);
//                } else {
//                    $data = array();
//                }
//
//                if($type == 'int'){
//                    $this->owner->$nameAttribute = '{' . join(",", $data) . '}';
//                } else {
//                    array_walk($data, function(&$val, $key){
//                        $val = str_replace('"', '&quot;', $val);
//                    });
//                    $this->owner->$nameAttribute = '{"' . join('","', $data) . '"}';
//                }
            }
//            die(pr($this->owner->attributes));
        }
    }

    /**
     * Преобразует массив PostgreSQL в массив php после сохранения
     */
    public function afterSave($event){
        $this->stringToArray();
    }

    /**
     * Преобразует массив PostgreSQL в массив php после поиска
     */
    public function afterFind($event){
        $this->stringToArray();
    }

    /**
     * Преобразует массив PostgreSQL в массив php
     */
    private function stringToArray(){
        foreach($this->arrayAttributes as $nameAttribute => $type){
            if(!$this->owner->$nameAttribute || empty($this->owner->$nameAttribute)){
                $this->owner->$nameAttribute = array();
                continue;
            }

            if($type == 'text'){
                preg_match('/^{(.*)}$/', $this->owner->$nameAttribute, $matches);
                $phpArr = array();
                if(isset($matches[1]) && !empty($matches[1])){
                    $phpArr = str_getcsv($matches[1]);

                }
                $this->owner->$nameAttribute = $phpArr;
            } else {
                $attribute = str_replace(array('{', '}'), '', $this->owner->$nameAttribute);
                $this->owner->$nameAttribute = trim($attribute) ? explode(',', $attribute) : array();

            }

        }
    }
}
