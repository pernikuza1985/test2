<?php
namespace common\components\behavior;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class UploadBehavior extends Behavior{

    public $attributes;
    public $files = [];
    public $oldDataModel = null;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_AFTER_INSERT =>  'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE =>  'afterUpdate',
        ];
    }

    public function beforeInsert($event){
        $this->beforeSave();
    }

    public function beforeUpdate($event){
        $this->oldDataModel = $this->owner->getOldAttributes();
        $this->beforeSave();
    }

    public function afterInsert($event){
        $this->uploadImage();
    }

    public function afterUpdate($event){
        $this->uploadImage();
    }

    public function beforeSave(){

        if(is_array($this->attributes) && !empty($this->attributes)){
            foreach($this->attributes as $nameAttribute => $val){
                $nameAttribute = is_string($val) ? $val :  $nameAttribute;
                if(isset($val['type']) && $val['type'] == 'multiple'){
                    $upload = UploadedFile::getInstances($this->owner, $nameAttribute);
                } else {
                    $upload = UploadedFile::getInstance($this->owner, $nameAttribute);
                }
                if (is_array($upload)){
                    $images = [];
                    foreach ($upload as $file){
                        $images[] = time().'_'.$file->name;
                    }
                    $this->owner->$nameAttribute = $images;
                    $this->files[$nameAttribute] = $upload;
                } elseif ($upload) {
                    $this->owner->$nameAttribute = time().'_'.$upload->name;
                    $this->files[$nameAttribute] = $upload;
                } elseif(isset($this->oldDataModel[$nameAttribute])) {
                    $this->owner->$nameAttribute = $this->oldDataModel[$nameAttribute];
                }
            }
        }
    }

    public function uploadImage(){
        if(!empty($this->files)){
            foreach($this->files as $attribute => $file){
                if(is_array($file)){
                    foreach ($file as $val){
                        $file->saveAs(\Yii::$app->params['imageDir'].$this->owner->$attribute);
                        $this->deleteImage($attribute);
                    }
                } else {
                    $file->saveAs(\Yii::$app->params['imageDir'].$this->owner->$attribute);
                    $this->deleteImage($attribute);
                }

            }
        }
    }

    private function deleteImage($attribute){
        if($this->oldDataModel && !empty($this->oldDataModel[$attribute])){
            if(is_string($this->oldDataModel[$attribute]) && is_file(\Yii::$app->params['imageDir'].$this->oldDataModel[$attribute])){
                unlink(\Yii::$app->params['imageDir'].$this->oldDataModel[$attribute]);
            }
        }
    }
}