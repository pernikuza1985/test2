<?php


namespace common\components\helper;

use common\models\Category;
use \Yii;

class FilterHelper
{
    public static $delimiter = ',';

    public static $params = [
        'country',
        'date_from',
        'date_to',
        'room_type',
        'price_from',
        'price_to',
        'person',
        'kids',
        'category',
        'date',
    ];

    public static function getData($filter){
        $filterData = explode('/', $filter);
        $filter = [];
        if(isset($filterData[0])){
            $category = Category::find()->where(['url' => $filterData[0]])->one();
            if($category){
                $filter['category'] = $category->id;
                unset($filterData[0]);
            }
        }

        foreach ($filterData as $parameter){
            $filterParams = explode('=', $parameter);
            if(in_array($filterParams[0], self::$params)){
                $filter[$filterParams[0]] = strpos($filterParams[1], self::$delimiter) === false ? $filterParams[1] : explode(self::$delimiter, $filterParams[1]);
            }
        }
        return $filter;
    }

    public static function urlPage($val){
        $dataFilter = explode('/', $_GET['filter']);
        $isSearchFilter = false;
        foreach ($dataFilter as &$filter){
            $filterData = explode('=', $filter);
            if($filterData[0] == 'page'){
                $isSearchFilter = true;
                $filter = 'page='.$val;
            }
        }

        if(!$isSearchFilter){
            $dataFilter[] = 'page ='.$val;
        }
    }
}