<?php


namespace common\components\helper;

use \Yii;

class PriceHelper
{
    private static $dataCurrency = [
        'uah' => ['course' => 1, 'code' => '&#8372;'],
        'eur' => ['course' => 33, 'code' => '&#8364;'],
        'usd' => ['course' => 27.5, 'code' => '$'],
    ];

    public static function Currency($price){
        $currency = isset(Yii::$app->session['currency']) && isset(self::$dataCurrency[Yii::$app->session['currency']]) ? Yii::$app->session['currency'] : 'uah';
        $price = $price / (self::$dataCurrency[$currency]['course']);
        return \Yii::$app->formatter->asCurrency($price, $currency, [\NumberFormatter::FRACTION_DIGITS => 0]);
    }
}