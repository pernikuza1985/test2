<?php


namespace common\components\helper;


class aDate
{
    static private $nameMonth = [
        1 => 'Январе',
        2 => 'Феврале',
        3 => 'Марте',
        4 => 'Апреле',
        5 => 'Мае',
        6 => 'Июне',
        7 => 'Июле',
        8 => 'Августе',
        9 => 'Сентябре',
        10 => 'Октябре',
        11 => 'Ноябре',
        12 => 'Декабре',
    ];
    static private $ua_nameMonth = [
        1 => 'Cічні',
        2 => 'Лютому',
        3 => 'Березні',
        4 => 'Квітні',
        5 => 'Травні',
        6 => 'Червні',
        7 => 'Липні',
        8 => 'Серпні',
        9 => 'Вересні',
        10 => 'Жовтні',
        11 => 'Листопаді',
        12 => 'Грудні',
    ];

    /**
     * @param $time - time()
     * @param $type отображать всё значение(long) месяца или первые три буквы (short)
     * @return string
     */
    static public function getFormatter($time, $type){
        if($type == 'long'){
            $month = self::$nameMonth[(int)date('m', $time)];
            $date = date('d', $time).' '.mb_strtolower($month, 'utf-8');
        }
        if($type == 'short'){
            $month = self::$nameMonth[(int)date('m', $time)];
            $date = date('d', $time).' '.mb_substr($month, 0, 3, 'utf-8');
        }
        return $date;
    }

    static public function add_month($time, $num=1) {
        $d=date('j',$time);  // день
        $m=date('n',$time);  // месяц
        $y=date('Y',$time);  // год

        // Прибавить месяц(ы)
        $m+=$num;
        if ($m>12) {
            $y+=floor($m/12);
            $m=($m%12);
            // Дополнительная проверка на декабрь
            if (!$m) {
                $m=12;
                $y--;
            }
        }

        // Это последний день месяца?
        if ($d==date('t',$time)) {
            $d=31;
        }
        // Открутить дату, пока она не станет корректной
        while(true) {
            if (checkdate($m,$d,$y)){
                break;
            }
            $d--;
        }
        // Вернуть новую дату в TIMESTAMP
        return mktime(0,0,0,$m,$d,$y);
    }
}