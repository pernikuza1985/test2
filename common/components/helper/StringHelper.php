<?php

namespace common\components\helper;

class StringHelper extends \yii\helpers\StringHelper
{
    /**
     * Преобразует первую букву строки в нижний регистр
     * 
     * @param $string
     * @return string
     */
    public static function lcfirst($string)
    {
        if (mb_substr($string, 1, 1) !== mb_strtoupper(mb_substr($string, 1, 1))) {
            return mb_strtolower(mb_substr($string, 0, 1)) . mb_substr($string, 1, mb_strlen($string));
        } else {
            return $string;
        }
    }
    /*
     * Скланяет курорт в зависимости от количества
     *
     * @param $age - integer
     * @param $string
     */
    public static function decliningString(int $count){
        $string = 'Курорта';
        $lastDigit = substr($count, -1);
        if(($lastDigit >= 5 && $count <= 20) || $lastDigit == 0){
            $string = 'Курортов';
        } elseif($lastDigit == 1){
            $string = 'Курорт';
        }
        return $string;
    }


}