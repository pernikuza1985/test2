<?php


namespace common\components\helper;

use \Yii;

class ImageHelper
{
    public static function path($image){
        return $image ? $image : '../../assets/images/products/products/h1.jpg';
    }
}