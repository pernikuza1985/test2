<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
//            'dsn' => 'mysql:host=localhost;dbname=yii2advanced',
//            'connectionString' => 'pgsql:host=localhost;port=5432;dbname=sonata_merged',
            'dsn' => 'pgsql:host=localhost;port=5432;dbname=limedtravel',
            'username' => 'postgres',
            'password' => '1',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
