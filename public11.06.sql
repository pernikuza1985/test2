/*
Navicat PGSQL Data Transfer

Source Server         : localhost
Source Server Version : 90617
Source Host           : localhost:5432
Source Database       : limedtravel
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90617
File Encoding         : 65001

Date: 2021-06-11 19:32:48
*/


-- ----------------------------
-- Sequence structure for city_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."city_id_seq";
CREATE SEQUENCE "public"."city_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."city_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for complaint_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."complaint_id_seq";
CREATE SEQUENCE "public"."complaint_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."complaint_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for country_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."country_id_seq";
CREATE SEQUENCE "public"."country_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 46
 CACHE 1;
SELECT setval('"public"."country_id_seq"', 46, true);

-- ----------------------------
-- Sequence structure for en_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_category_id_seq";
CREATE SEQUENCE "public"."en_category_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_city_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_city_id_seq";
CREATE SEQUENCE "public"."en_city_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_country_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_country_id_seq";
CREATE SEQUENCE "public"."en_country_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_hotel_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_hotel_id_seq";
CREATE SEQUENCE "public"."en_hotel_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_region_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_region_id_seq";
CREATE SEQUENCE "public"."en_region_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_room_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_room_type_id_seq";
CREATE SEQUENCE "public"."en_room_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_tour_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_tour_id_seq";
CREATE SEQUENCE "public"."en_tour_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for en_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."en_type_id_seq";
CREATE SEQUENCE "public"."en_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for price_room_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."price_room_id_seq";
CREATE SEQUENCE "public"."price_room_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 33
 CACHE 1;
SELECT setval('"public"."price_room_id_seq"', 33, true);

-- ----------------------------
-- Sequence structure for region_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."region_id_seq";
CREATE SEQUENCE "public"."region_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"public"."region_id_seq"', 7, true);

-- ----------------------------
-- Sequence structure for ru_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_category_id_seq";
CREATE SEQUENCE "public"."ru_category_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."ru_category_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for ru_hotel_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_hotel_id_seq";
CREATE SEQUENCE "public"."ru_hotel_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 73
 CACHE 1;
SELECT setval('"public"."ru_hotel_id_seq"', 73, true);

-- ----------------------------
-- Sequence structure for ru_hotel_rating_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_hotel_rating_id_seq";
CREATE SEQUENCE "public"."ru_hotel_rating_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18
 CACHE 1;
SELECT setval('"public"."ru_hotel_rating_id_seq"', 18, true);

-- ----------------------------
-- Sequence structure for ru_rating_stars_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_rating_stars_seq";
CREATE SEQUENCE "public"."ru_rating_stars_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."ru_rating_stars_seq"', 1, true);

-- ----------------------------
-- Sequence structure for ru_room_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_room_id_seq";
CREATE SEQUENCE "public"."ru_room_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"public"."ru_room_id_seq"', 7, true);

-- ----------------------------
-- Sequence structure for ru_room_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_room_type_id_seq";
CREATE SEQUENCE "public"."ru_room_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 6
 CACHE 1;
SELECT setval('"public"."ru_room_type_id_seq"', 6, true);

-- ----------------------------
-- Sequence structure for ru_tour_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_tour_id_seq";
CREATE SEQUENCE "public"."ru_tour_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."ru_tour_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for ru_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ru_type_id_seq";
CREATE SEQUENCE "public"."ru_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for tour_places_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tour_places_id_seq";
CREATE SEQUENCE "public"."tour_places_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."tour_places_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for tour_program_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tour_program_id_seq";
CREATE SEQUENCE "public"."tour_program_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."tour_program_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for tour_table_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tour_table_id_seq";
CREATE SEQUENCE "public"."tour_table_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."tour_table_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for ua_city_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_city_id_seq";
CREATE SEQUENCE "public"."ua_city_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ua_country_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_country_id_seq";
CREATE SEQUENCE "public"."ua_country_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."ua_country_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for ua_hotel_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_hotel_id_seq";
CREATE SEQUENCE "public"."ua_hotel_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ua_region_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_region_id_seq";
CREATE SEQUENCE "public"."ua_region_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ua_room_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_room_type_id_seq";
CREATE SEQUENCE "public"."ua_room_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ua_tour_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_tour_id_seq";
CREATE SEQUENCE "public"."ua_tour_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ua_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ua_type_id_seq";
CREATE SEQUENCE "public"."ua_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_id_seq";
CREATE SEQUENCE "public"."user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 9
 CACHE 1;
SELECT setval('"public"."user_id_seq"', 9, true);

-- ----------------------------
-- Table structure for complaint
-- ----------------------------
DROP TABLE IF EXISTS "public"."complaint";
CREATE TABLE "public"."complaint" (
"id" int4 DEFAULT nextval('complaint_id_seq'::regclass) NOT NULL,
"url" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default" NOT NULL,
"message" text COLLATE "default" NOT NULL,
"type" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of complaint
-- ----------------------------
INSERT INTO "public"."complaint" VALUES ('1', '', '', 'sAD', '2');
INSERT INTO "public"."complaint" VALUES ('2', '', 'wqeq@asda.asd', 'adadad', '1');

-- ----------------------------
-- Table structure for en_city
-- ----------------------------
DROP TABLE IF EXISTS "public"."en_city";
CREATE TABLE "public"."en_city" (
"id" int4 DEFAULT nextval('en_city_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default",
"country_id" int4 NOT NULL,
"region_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of en_city
-- ----------------------------

-- ----------------------------
-- Table structure for en_country
-- ----------------------------
DROP TABLE IF EXISTS "public"."en_country";
CREATE TABLE "public"."en_country" (
"id" int4 DEFAULT nextval('en_country_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass),
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of en_country
-- ----------------------------

-- ----------------------------
-- Table structure for en_hotel
-- ----------------------------
DROP TABLE IF EXISTS "public"."en_hotel";
CREATE TABLE "public"."en_hotel" (
"id" int4 DEFAULT nextval('en_hotel_id_seq'::regclass) NOT NULL,
"address" varchar(255) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"site" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"body" text COLLATE "default" DEFAULT ''::text,
"url" varchar(255) COLLATE "default",
"image" varchar(255) COLLATE "default",
"user_id" int4 NOT NULL,
"discount" int4,
"date_create" date DEFAULT now(),
"country_id" int4,
"region_id" int4,
"city_id" int4,
"min_people" int4,
"max_people" int4,
"minimum_quantity" int4,
"deposite" float4,
"extra" varchar(255) COLLATE "default",
"identify" varchar(64) COLLATE "default",
"fax" varchar(255) COLLATE "default",
"toll_free" varchar(255) COLLATE "default",
"certification" varchar(255) COLLATE "default",
"video" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"title" varchar(255) COLLATE "default" NOT NULL,
"included" int4[],
"excluded" int4[],
"type_id" int4[],
"type_hotel" int2[],
"category_id" int4[],
"established" varchar(255) COLLATE "default",
"services" int4[],
"payment_methods" int4[],
"images" text[] COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of en_hotel
-- ----------------------------

-- ----------------------------
-- Table structure for en_region
-- ----------------------------
DROP TABLE IF EXISTS "public"."en_region";
CREATE TABLE "public"."en_region" (
"id" int4 DEFAULT nextval('en_region_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default",
"country_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of en_region
-- ----------------------------

-- ----------------------------
-- Table structure for en_room_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."en_room_type";
CREATE TABLE "public"."en_room_type" (
"id" int4 DEFAULT nextval('en_room_type_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"image" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of en_room_type
-- ----------------------------

-- ----------------------------
-- Table structure for price_room
-- ----------------------------
DROP TABLE IF EXISTS "public"."price_room";
CREATE TABLE "public"."price_room" (
"id" int4 DEFAULT nextval('price_room_id_seq'::regclass) NOT NULL,
"room_id" int4 NOT NULL,
"count_people" int4,
"count_child" int4,
"count_day" int4,
"date_from" date,
"date_to" date,
"price" numeric(10,2),
"hotel_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of price_room
-- ----------------------------
INSERT INTO "public"."price_room" VALUES ('27', '6', '3', '2', '5', '2021-06-14', '2021-06-30', '150.00', '71');
INSERT INTO "public"."price_room" VALUES ('28', '6', '4', '1', '9', '2021-06-17', '2021-06-30', '155.00', '71');
INSERT INTO "public"."price_room" VALUES ('29', '6', '1', '4', '15', '2021-06-08', '2021-06-25', '955.00', '71');
INSERT INTO "public"."price_room" VALUES ('30', '5', '2', '1', '5', '2021-06-17', '2021-06-26', '59.00', '73');
INSERT INTO "public"."price_room" VALUES ('31', '5', '3', '2', '5', '2021-06-16', '2021-06-17', '957.00', '73');
INSERT INTO "public"."price_room" VALUES ('32', '7', '2', '0', '9', '2021-06-09', '2021-06-30', '555.00', '70');
INSERT INTO "public"."price_room" VALUES ('33', '7', '1', '1', '5', '2021-06-16', '2021-06-17', '155.00', '70');

-- ----------------------------
-- Table structure for ru_category
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_category";
CREATE TABLE "public"."ru_category" (
"id" int4 DEFAULT nextval('ru_category_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default",
"url" varchar(255) COLLATE "default" NOT NULL,
"image" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_category
-- ----------------------------
INSERT INTO "public"."ru_category" VALUES ('2', 'оздоравитель1', 'ozdoravitel1', '1622461897_03_01_яковчук.jpg', '', 'вааывва', 'ывавыаыв', 'ываыа', 'ываыв');
INSERT INTO "public"."ru_category" VALUES ('3', 'оздоравитель', 'ozdoravitel', '1622548729_30_01_20-.jpg', 'ывфвф', 'H1 - оздоравитель', 'Meta Title - оздоравитель', 'Meta Description - оздоравитель', 'Seo Text - оздоравитель');
INSERT INTO "public"."ru_category" VALUES ('4', 'Всё для детей', 'vse-dla-detej', '1622561198_14_01_20.jpg', 'Description Всё для детей', 'Description Всё для детей', 'Meta Title Всё для детей', ' Meta Description Всё для детей', 'Seo Text Всё для детей');
INSERT INTO "public"."ru_category" VALUES ('5', 'Всё для молодёжи', 'vse-dla-molodezi', '1622561378_28_01_20.jpg', 'Description Всё для молодёжи', ' H1 Всё для молодёжи', ' Meta Title Всё для молодёжи', 'Meta Description Всё для молодёжи', 'Seo Text Всё для молодёжи');
INSERT INTO "public"."ru_category" VALUES ('6', 'Всё для пенсионев', 'vse-dla-pensionev', '1622561509_20_01_вв.jpg', 'Всё для пенсионев', 'H1 Всё для пенсионев', ' Meta Title  Всё для пенсионев', ' Meta Description  Всё для пенсионев', ' Seo Text
 Всё для пенсионев');
INSERT INTO "public"."ru_category" VALUES ('8', 'оздоравитель-2', 'ozdoravitel-2', null, 'Description - fsdfs', '', '', '', '');

-- ----------------------------
-- Table structure for ru_city
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_city";
CREATE TABLE "public"."ru_city" (
"id" int4 DEFAULT nextval('city_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default",
"country_id" int4 NOT NULL,
"region_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_city
-- ----------------------------
INSERT INTO "public"."ru_city" VALUES ('3', 'Киев', 'kiev', 'Description Киев', '', '', '', '', null, '46', '7');
INSERT INTO "public"."ru_city" VALUES ('4', 'Киев1', 'kiev1', 'Description Киев1', 'H1 Киев', 'Meta Title Киев', 'Meta DescriptionКиев', 'sdasda', '1622455054_10_01_20.jpg', '46', '7');

-- ----------------------------
-- Table structure for ru_comments
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_comments";
CREATE TABLE "public"."ru_comments" (
"id" int4 DEFAULT nextval('ru_hotel_rating_id_seq'::regclass) NOT NULL,
"user_id" int4,
"name" varchar(255) COLLATE "default" NOT NULL,
"email" varchar(255) COLLATE "default" NOT NULL,
"hotel_id" int4,
"message" text COLLATE "default" NOT NULL,
"date" timestamp(6) DEFAULT now(),
"parent" int4 DEFAULT 0 NOT NULL,
"tour_id" int4,
"approved" int4 DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_comments
-- ----------------------------
INSERT INTO "public"."ru_comments" VALUES ('9', '7', 'superadmin', 'superadmin@limedtravel.com', '70', '111', '2021-06-10 13:51:58.699692', '0', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('10', '7', 'superadmin', 'superadmin@limedtravel.com', '70', '222', '2021-06-10 13:52:48.117804', '9', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('13', '7', 'superadmin', 'superadmin@limedtravel.com', '70', '333333', '2021-06-10 14:07:29.277424', '0', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('14', '7', 'superadmin', 'superadmin@limedtravel.com', '70', 'sadada', '2021-06-11 12:33:20.268324', '9', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('15', '7', 'superadmin', 'superadmin@limedtravel.com', '70', 'sadada', '2021-06-11 12:43:13.850859', '9', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('16', '7', 'superadmin', 'superadmin@limedtravel.com', '70', 'ывфыыв', '2021-06-11 12:44:50.425812', '0', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('17', '7', 'superadmin', 'superadmin@limedtravel.com', '70', '2131у1', '2021-06-11 12:45:01.637898', '13', null, '1');
INSERT INTO "public"."ru_comments" VALUES ('18', '7', 'superadmin', 'superadmin@limedtravel.com', '70', 'sdasda', '2021-06-11 12:48:38.163078', '0', null, '0');

-- ----------------------------
-- Table structure for ru_country
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_country";
CREATE TABLE "public"."ru_country" (
"id" int4 DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass),
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_country
-- ----------------------------
INSERT INTO "public"."ru_country" VALUES ('46', 'Украина', 'ukraina', 'Description Украина', 'H1Украина', 'Meta Title Украина', 'Meta Description Украина', 'Seo Text Украина', '1622454413_03_01_яковчук.jpg');

-- ----------------------------
-- Table structure for ru_hotel
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_hotel";
CREATE TABLE "public"."ru_hotel" (
"id" int4 DEFAULT nextval('ru_hotel_id_seq'::regclass) NOT NULL,
"address" varchar(255) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"site" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"body" text COLLATE "default" DEFAULT ''::text,
"url" varchar(255) COLLATE "default",
"image" varchar(255) COLLATE "default",
"user_id" int4 NOT NULL,
"discount" int4,
"date_create" date DEFAULT now(),
"country_id" int4,
"region_id" int4,
"city_id" int4,
"min_people" int4,
"max_people" int4,
"minimum_quantity" int4,
"deposite" float4,
"extra" varchar(255) COLLATE "default",
"identify" varchar(64) COLLATE "default",
"fax" varchar(255) COLLATE "default",
"toll_free" varchar(255) COLLATE "default",
"certification" varchar(255) COLLATE "default",
"video" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"title" varchar(255) COLLATE "default" NOT NULL,
"included" int4[],
"excluded" int4[],
"type_id" int4[],
"category_id" int4[],
"established" varchar(255) COLLATE "default",
"services" int4[],
"payment_methods" int4[],
"images" text[] COLLATE "default",
"type_hotel" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_hotel
-- ----------------------------
INSERT INTO "public"."ru_hotel" VALUES ('70', 'Address 1', '555-55-55', 'Site1', 'Site1@tre.wer', 'sdfsdfs', '<div>asdfsafsd<br></div>', 'sanatorij-veselka', '/image/1622454502_16_12_19%20rus.jpg', '9', '10', '2021-06-02', '46', '7', '3', '5', '10', '12', '15', '555', 'dffdsfsfwer34r3', '555-111-11', 'Toll Free', 'Certification', 'Video', 'H1', 'Meta Title', 'Meta Description', 'asdad', 'санаторий веселка', '{1,2,3}', '{1,2,3}', null, '{4}', 'sdasdfsaf', '{1,2}', '{1,2,3}', '{/image/1622454413_03_01_%D1%8F%D0%BA%D0%BE%D0%B2%D1%87%D1%83%D0%BA.jpg,/image/1622454502_16_12_19%20rus.jpg,/image/1622455054_10_01_20%20copy%201.jpg,/image/1622458296_20_01_%D0%B2%D0%B2.jpg,/image/1622561198_14_01_20.jpg,/image/1622561378_28_01_20.jpg,/image/avatar-1.png.png,/image/joker-1.png}', '1');
INSERT INTO "public"."ru_hotel" VALUES ('71', '', '', 'Site4', '', '', '<div><br></div>', 'sanatorij-qwerty', '', '9', null, '2021-06-02', '46', '7', '3', null, null, null, null, '', null, '', '', '', '', '', '', '', '', 'санаторий qwerty', '{}', '{}', null, '{5,3}', '', '{}', '{}', '{}', null);
INSERT INTO "public"."ru_hotel" VALUES ('72', '', '', 'Site2', '', '', '<div><br></div>', 'sanatorij-qqq', '', '9', null, '2021-06-02', '0', null, null, null, null, null, null, '', null, '', '', '', '', '', '', '', '', 'санаторий qqq', '{}', '{}', null, '{4,5}', '', '{}', '{}', '{}', null);
INSERT INTO "public"."ru_hotel" VALUES ('73', '', '', 'Site3', '', '', '<div><br></div>', 'sanatorij-kolokolcik', '', '9', null, '2021-06-02', '0', null, null, null, null, null, null, '', null, '', '', '', 'Video', 'H1', 'Meta Title', '', '', 'санаторий колокольчик', '{}', '{}', null, '{5,6}', '', '{}', '{}', '{}', null);

-- ----------------------------
-- Table structure for ru_rating_stars
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_rating_stars";
CREATE TABLE "public"."ru_rating_stars" (
"id" int4 DEFAULT nextval('ru_rating_stars_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"hotel_id" int4,
"tour_id" int4,
"date_create" timestamp(6) DEFAULT now(),
"count_stars" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_rating_stars
-- ----------------------------
INSERT INTO "public"."ru_rating_stars" VALUES ('1', '7', '70', null, '2021-06-10 12:28:38.191513', '5');

-- ----------------------------
-- Table structure for ru_region
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_region";
CREATE TABLE "public"."ru_region" (
"id" int4 DEFAULT nextval('region_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default",
"country_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_region
-- ----------------------------
INSERT INTO "public"."ru_region" VALUES ('7', 'Киевский', 'kievskij', 'Description Киевский', 'H1 Киевский', 'Meta Title Киевский', 'Meta Description Киевский', 'Seo Text Киевский', '1622454502_16_12_19 rus.jpg', '46');

-- ----------------------------
-- Table structure for ru_room
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_room";
CREATE TABLE "public"."ru_room" (
"id" int4 DEFAULT nextval('ru_room_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"id_room_type" int2,
"services" int4[],
"user_id" int4,
"images" text[] COLLATE "default",
"price" numeric(10,2),
"old_price" numeric(10,2),
"url" varchar(255) COLLATE "default" NOT NULL,
"id_hotel" int2 NOT NULL,
"image" varchar(255) COLLATE "default",
"description" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_room
-- ----------------------------
INSERT INTO "public"."ru_room" VALUES ('5', 'маленькая', '5', '{1,2}', null, '{/image/1622561509_20_01.jpg,/image/joker-1.png,/image/joker-4.png}', '159.00', '951.00', 'malenkaa', '73', null, null);
INSERT INTO "public"."ru_room" VALUES ('6', 'двух комнатная', '5', '{1,2}', null, '{/image/joker-5.jpg}', '159.00', '951.00', 'dvuh-komnatnaa', '71', null, null);
INSERT INTO "public"."ru_room" VALUES ('7', 'трёхкомнатная', '5', '{1,2}', null, '{/image/1622561509_20_01.jpg}', '159.00', '951.00', 'trehkomnatnaa', '70', null, null);

-- ----------------------------
-- Table structure for ru_room_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_room_type";
CREATE TABLE "public"."ru_room_type" (
"id" int4 DEFAULT nextval('ru_room_type_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"image" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_room_type
-- ----------------------------
INSERT INTO "public"."ru_room_type" VALUES ('5', 'масандра', '/image/1621519593_heder_slider_3.png');
INSERT INTO "public"."ru_room_type" VALUES ('6', 'масандра-2к', '/image/1622561198_14_01_20.jpg');

-- ----------------------------
-- Table structure for ru_tour
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_tour";
CREATE TABLE "public"."ru_tour" (
"id" int4 DEFAULT nextval('ru_tour_id_seq'::regclass) NOT NULL,
"address" varchar(255) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"site" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"body" text COLLATE "default" DEFAULT ''::text,
"url" varchar(255) COLLATE "default",
"image" varchar(255) COLLATE "default",
"user_id" int4 NOT NULL,
"discount" int4,
"date_create" date DEFAULT now(),
"country_id" int4,
"region_id" int4,
"city_id" int4,
"min_people" int4,
"max_people" int4,
"minimum_quantity" int4,
"required" varchar(24) COLLATE "default",
"trip" varchar(255) COLLATE "default",
"identify" varchar(64) COLLATE "default",
"fax" varchar(255) COLLATE "default",
"toll_free" varchar(255) COLLATE "default",
"certification" varchar(255) COLLATE "default",
"video" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"title" varchar(255) COLLATE "default" NOT NULL,
"included" int4[],
"excluded" int4[],
"type_id" int4[],
"category_id" int4[],
"established" varchar(255) COLLATE "default",
"services" int4[],
"payment_methods" int4[],
"images" text[] COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_tour
-- ----------------------------
INSERT INTO "public"."ru_tour" VALUES ('2', 'пчелки 5', '555-55-55', 'супертур.ua', 'aaa@супертур.ua', 'Description - супер тур', '<div><br></div>', 'super-tur', '', '9', '30', '2021-05-31', '46', null, null, null, null, null, '', '', '', '', '', '', '', '', '', '', '', 'супер тур', null, null, null, '{2}', '', null, null, '{}');
INSERT INTO "public"."ru_tour" VALUES ('3', 'пчелки 51', '555-55-11', 'супертур.ua', 'aaa@супертур.ua', 'Description - супер тур', '<div>fsfsf<br></div>', 'super-tur2', '/image/joker-5.jpg', '9', null, '2021-05-31', '46', null, null, null, null, null, '', '', '', 'dsfs', 'sdfsf', 'fsdfs', 'Video', 'H1', 'Meta Title', 'Meta Title', '', 'супер тур2', null, null, null, '{2}', 'sdasda', '{1}', '{1,2,3}', '{}');
INSERT INTO "public"."ru_tour" VALUES ('4', 'пчелки 5фыф', '555-55-55', 'супертур.ua', 'aaa@супертур.ua', 'Description - супер тур', '<div>asdada<br></div>', 'super-tur-888', '/image/1621841812_20_01vv.jpg', '9', '15', '2021-05-31', '46', '7', '3', '5', '90', '5', 'Required', '15 дней', 'Identify', 'dsfs', 'sdfsf', 'Certification', 'Video', 'H1afwfw', 'Meta Title супер тур 888', 'Meta Title супер тур 888', 'zxcacas', 'супер тур 888', '{1,2,3}', '{1,2,3}', null, '{2}', 'sdasda', '{1,2}', '{1,2,3}', '{/image/joker-1.png,/image/joker-4.png,/image/joker-5.jpg}');

-- ----------------------------
-- Table structure for ru_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."ru_type";
CREATE TABLE "public"."ru_type" (
"id" int4 DEFAULT nextval('ru_type_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default",
"url" varchar(255) COLLATE "default" NOT NULL,
"image" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ru_type
-- ----------------------------

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS "public"."test";
CREATE TABLE "public"."test" (
"array1" int4[]
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO "public"."test" VALUES ('{1}');

-- ----------------------------
-- Table structure for tour_places
-- ----------------------------
DROP TABLE IF EXISTS "public"."tour_places";
CREATE TABLE "public"."tour_places" (
"id" int4 DEFAULT nextval('tour_places_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"images" text[] COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tour_places
-- ----------------------------
INSERT INTO "public"."tour_places" VALUES ('2', 'Лагерь', '{/image/joker-1.png}');

-- ----------------------------
-- Table structure for tour_program
-- ----------------------------
DROP TABLE IF EXISTS "public"."tour_program";
CREATE TABLE "public"."tour_program" (
"id" int4 DEFAULT nextval('tour_program_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default",
"tour_id" int4 NOT NULL,
"image" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"order" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tour_program
-- ----------------------------

-- ----------------------------
-- Table structure for tour_table
-- ----------------------------
DROP TABLE IF EXISTS "public"."tour_table";
CREATE TABLE "public"."tour_table" (
"id" int4 DEFAULT nextval('tour_table_id_seq'::regclass) NOT NULL,
"tour_type" int4,
"tour_price_type" int4,
"price" numeric(10,2),
"price_old" numeric(255),
"tour_id" int4 NOT NULL,
"description" varchar(255) COLLATE "default",
"date" date DEFAULT now() NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tour_table
-- ----------------------------
INSERT INTO "public"."tour_table" VALUES ('1', '2', '1', '500.00', '900', '2', 'Description', '2021-06-30');
INSERT INTO "public"."tour_table" VALUES ('2', '3', '3', '350.00', '500', '4', 'Description 888', '2021-06-15');
INSERT INTO "public"."tour_table" VALUES ('3', '0', '0', '3500.00', '5000', '3', 'Description тур 2', '2021-06-18');

-- ----------------------------
-- Table structure for ua_city
-- ----------------------------
DROP TABLE IF EXISTS "public"."ua_city";
CREATE TABLE "public"."ua_city" (
"id" int4 DEFAULT nextval('ua_city_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default",
"country_id" int4 NOT NULL,
"region_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ua_city
-- ----------------------------

-- ----------------------------
-- Table structure for ua_country
-- ----------------------------
DROP TABLE IF EXISTS "public"."ua_country";
CREATE TABLE "public"."ua_country" (
"id" int4 DEFAULT nextval('ua_country_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass),
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ua_country
-- ----------------------------

-- ----------------------------
-- Table structure for ua_hotel
-- ----------------------------
DROP TABLE IF EXISTS "public"."ua_hotel";
CREATE TABLE "public"."ua_hotel" (
"id" int4 DEFAULT nextval('ua_hotel_id_seq'::regclass) NOT NULL,
"address" varchar(255) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"site" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"body" text COLLATE "default" DEFAULT ''::text,
"url" varchar(255) COLLATE "default",
"image" varchar(255) COLLATE "default",
"user_id" int4 NOT NULL,
"discount" int4,
"date_create" date DEFAULT now(),
"country_id" int4,
"region_id" int4,
"city_id" int4,
"min_people" int4,
"max_people" int4,
"minimum_quantity" int4,
"deposite" float4,
"extra" varchar(255) COLLATE "default",
"identify" varchar(64) COLLATE "default",
"fax" varchar(255) COLLATE "default",
"toll_free" varchar(255) COLLATE "default",
"certification" varchar(255) COLLATE "default",
"video" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"title" varchar(255) COLLATE "default" NOT NULL,
"included" int4[],
"excluded" int4[],
"type_id" int4[],
"type_hotel" int2[],
"category_id" int4[],
"established" varchar(255) COLLATE "default",
"services" int4[],
"payment_methods" int4[],
"images" text[] COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ua_hotel
-- ----------------------------

-- ----------------------------
-- Table structure for ua_region
-- ----------------------------
DROP TABLE IF EXISTS "public"."ua_region";
CREATE TABLE "public"."ua_region" (
"id" int4 DEFAULT nextval('region_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default" NOT NULL,
"url" varchar(255) COLLATE "default" DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"description" varchar(255) COLLATE "default",
"h1" varchar(255) COLLATE "default",
"meta_title" varchar(255) COLLATE "default",
"meta_description" varchar(255) COLLATE "default",
"seo_text" text COLLATE "default",
"image" varchar(255) COLLATE "default",
"country_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ua_region
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."user";
CREATE TABLE "public"."user" (
"id" int4 DEFAULT nextval('user_id_seq'::regclass) NOT NULL,
"user_name" varchar(255) COLLATE "default",
"first_name" varchar(255) COLLATE "default",
"last_name" varchar(255) COLLATE "default",
"website" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default" NOT NULL,
"facebook" varchar(255) COLLATE "default",
"linkedin" varchar(255) COLLATE "default",
"instagram" varchar(255) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"info" text COLLATE "default",
"role" int2 NOT NULL,
"image" varchar(255) COLLATE "default",
"company" varchar(255) COLLATE "default",
"city_id" int4,
"country_id" int4 NOT NULL,
"postal_code" int4,
"email_subscribe" varchar(255) COLLATE "C",
"address_main" varchar(255) COLLATE "default",
"address_mail" varchar(255) COLLATE "default",
"status" int2,
"date_create" timestamp(6) DEFAULT now(),
"date_last_login" timestamp(6),
"email" varchar(255) COLLATE "default",
"region_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO "public"."user" VALUES ('7', 'superadmin', '', '', '', '$2y$13$nhUAPHAIn.38yh.glTxC7.gPZsu1Z8ITygxgHnEC21ETCtEnKotsC', '', '', '', '', '', '5', '1623404426_logo.png', '', '3', '46', null, '', '', '', '1', '2021-05-31 10:17:48.501523', null, 'superadmin@limedtravel.com', '7');
INSERT INTO "public"."user" VALUES ('9', 'otelier', '', '', '', '$2y$13$GpZrcYBwesKsWMFdT9mcMufn2xUkKLTId8P3g3m/WbU3UjMOHzh/O', '', '', '', '', '', '3', '1622461735_16_12_19 rus.jpg', '', '3', '46', null, '', '', '', '1', '2021-05-31 11:48:55.506947', null, '', '7');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table complaint
-- ----------------------------
ALTER TABLE "public"."complaint" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table en_city
-- ----------------------------
CREATE UNIQUE INDEX "index_url_copy_copy" ON "public"."en_city" USING btree ("url");

-- ----------------------------
-- Uniques structure for table en_city
-- ----------------------------
ALTER TABLE "public"."en_city" ADD UNIQUE ("title");
ALTER TABLE "public"."en_city" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table en_city
-- ----------------------------
ALTER TABLE "public"."en_city" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table en_country
-- ----------------------------
CREATE INDEX "index_en_country_url" ON "public"."en_country" USING btree ("url");

-- ----------------------------
-- Uniques structure for table en_country
-- ----------------------------
ALTER TABLE "public"."en_country" ADD UNIQUE ("title");
ALTER TABLE "public"."en_country" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table en_country
-- ----------------------------
ALTER TABLE "public"."en_country" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table en_hotel
-- ----------------------------
CREATE INDEX "ru_hotel_url_idx_copy_copy" ON "public"."en_hotel" USING btree ("url");

-- ----------------------------
-- Primary Key structure for table en_hotel
-- ----------------------------
ALTER TABLE "public"."en_hotel" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table en_region
-- ----------------------------
CREATE INDEX "region_url_idx_copy_copy" ON "public"."en_region" USING btree ("url");

-- ----------------------------
-- Uniques structure for table en_region
-- ----------------------------
ALTER TABLE "public"."en_region" ADD UNIQUE ("title");
ALTER TABLE "public"."en_region" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table en_region
-- ----------------------------
ALTER TABLE "public"."en_region" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table en_room_type
-- ----------------------------
ALTER TABLE "public"."en_room_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table price_room
-- ----------------------------
ALTER TABLE "public"."price_room" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ru_category
-- ----------------------------
CREATE INDEX "ru_category_url_idx" ON "public"."ru_category" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ru_category
-- ----------------------------
ALTER TABLE "public"."ru_category" ADD UNIQUE ("title");
ALTER TABLE "public"."ru_category" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ru_category
-- ----------------------------
ALTER TABLE "public"."ru_category" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ru_city
-- ----------------------------
CREATE UNIQUE INDEX "index_url" ON "public"."ru_city" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ru_city
-- ----------------------------
ALTER TABLE "public"."ru_city" ADD UNIQUE ("title");
ALTER TABLE "public"."ru_city" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ru_city
-- ----------------------------
ALTER TABLE "public"."ru_city" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ru_comments
-- ----------------------------
ALTER TABLE "public"."ru_comments" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ru_country
-- ----------------------------
ALTER TABLE "public"."ru_country" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ru_hotel
-- ----------------------------
CREATE INDEX "ru_hotel_url_idx" ON "public"."ru_hotel" USING btree ("url");

-- ----------------------------
-- Primary Key structure for table ru_hotel
-- ----------------------------
ALTER TABLE "public"."ru_hotel" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ru_rating_stars
-- ----------------------------
ALTER TABLE "public"."ru_rating_stars" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ru_region
-- ----------------------------
CREATE INDEX "region_url_idx" ON "public"."ru_region" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ru_region
-- ----------------------------
ALTER TABLE "public"."ru_region" ADD UNIQUE ("title");
ALTER TABLE "public"."ru_region" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ru_region
-- ----------------------------
ALTER TABLE "public"."ru_region" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table ru_room
-- ----------------------------
ALTER TABLE "public"."ru_room" ADD UNIQUE ("title");
ALTER TABLE "public"."ru_room" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ru_room
-- ----------------------------
ALTER TABLE "public"."ru_room" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ru_room_type
-- ----------------------------
ALTER TABLE "public"."ru_room_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ru_tour
-- ----------------------------
CREATE INDEX "ru_hotel_url_idx_copy1" ON "public"."ru_tour" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ru_tour
-- ----------------------------
ALTER TABLE "public"."ru_tour" ADD UNIQUE ("title");
ALTER TABLE "public"."ru_tour" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ru_tour
-- ----------------------------
ALTER TABLE "public"."ru_tour" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ru_type
-- ----------------------------
CREATE INDEX "ru_type_url_idx_copy" ON "public"."ru_type" USING btree ("url");

-- ----------------------------
-- Primary Key structure for table ru_type
-- ----------------------------
ALTER TABLE "public"."ru_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table tour_places
-- ----------------------------
ALTER TABLE "public"."tour_places" ADD UNIQUE ("title");

-- ----------------------------
-- Primary Key structure for table tour_places
-- ----------------------------
ALTER TABLE "public"."tour_places" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tour_program
-- ----------------------------
ALTER TABLE "public"."tour_program" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table tour_table
-- ----------------------------
CREATE INDEX "tour_table_tour_id_index" ON "public"."tour_table" USING btree ("tour_id");

-- ----------------------------
-- Primary Key structure for table tour_table
-- ----------------------------
ALTER TABLE "public"."tour_table" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ua_city
-- ----------------------------
CREATE UNIQUE INDEX "index_url_copy" ON "public"."ua_city" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ua_city
-- ----------------------------
ALTER TABLE "public"."ua_city" ADD UNIQUE ("title");
ALTER TABLE "public"."ua_city" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ua_city
-- ----------------------------
ALTER TABLE "public"."ua_city" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ua_country
-- ----------------------------
CREATE INDEX "index_ua_country_url" ON "public"."ua_country" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ua_country
-- ----------------------------
ALTER TABLE "public"."ua_country" ADD UNIQUE ("title");
ALTER TABLE "public"."ua_country" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ua_country
-- ----------------------------
ALTER TABLE "public"."ua_country" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ua_hotel
-- ----------------------------
CREATE INDEX "ru_hotel_url_idx_copy" ON "public"."ua_hotel" USING btree ("url");

-- ----------------------------
-- Primary Key structure for table ua_hotel
-- ----------------------------
ALTER TABLE "public"."ua_hotel" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ua_region
-- ----------------------------
CREATE INDEX "region_url_idx_copy" ON "public"."ua_region" USING btree ("url");

-- ----------------------------
-- Uniques structure for table ua_region
-- ----------------------------
ALTER TABLE "public"."ua_region" ADD UNIQUE ("title");
ALTER TABLE "public"."ua_region" ADD UNIQUE ("url");

-- ----------------------------
-- Primary Key structure for table ua_region
-- ----------------------------
ALTER TABLE "public"."ua_region" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD UNIQUE ("user_name");
ALTER TABLE "public"."user" ADD UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."en_city"
-- ----------------------------
ALTER TABLE "public"."en_city" ADD FOREIGN KEY ("country_id") REFERENCES "public"."en_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."en_city" ADD FOREIGN KEY ("region_id") REFERENCES "public"."en_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."en_hotel"
-- ----------------------------
ALTER TABLE "public"."en_hotel" ADD FOREIGN KEY ("city_id") REFERENCES "public"."ru_city" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."en_hotel" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."en_hotel" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."en_region"
-- ----------------------------
ALTER TABLE "public"."en_region" ADD FOREIGN KEY ("country_id") REFERENCES "public"."en_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."price_room"
-- ----------------------------
ALTER TABLE "public"."price_room" ADD FOREIGN KEY ("hotel_id") REFERENCES "public"."ru_hotel" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."price_room" ADD FOREIGN KEY ("room_id") REFERENCES "public"."ru_room" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_city"
-- ----------------------------
ALTER TABLE "public"."ru_city" ADD FOREIGN KEY ("country_id") REFERENCES "public"."ru_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_city" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_comments"
-- ----------------------------
ALTER TABLE "public"."ru_comments" ADD FOREIGN KEY ("tour_id") REFERENCES "public"."ru_tour" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_comments" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_comments" ADD FOREIGN KEY ("hotel_id") REFERENCES "public"."ru_hotel" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_hotel"
-- ----------------------------
ALTER TABLE "public"."ru_hotel" ADD FOREIGN KEY ("city_id") REFERENCES "public"."ru_city" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_hotel" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_hotel" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_rating_stars"
-- ----------------------------
ALTER TABLE "public"."ru_rating_stars" ADD FOREIGN KEY ("tour_id") REFERENCES "public"."ru_tour" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_rating_stars" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_rating_stars" ADD FOREIGN KEY ("hotel_id") REFERENCES "public"."ru_hotel" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_region"
-- ----------------------------
ALTER TABLE "public"."ru_region" ADD FOREIGN KEY ("country_id") REFERENCES "public"."ru_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_room"
-- ----------------------------
ALTER TABLE "public"."ru_room" ADD FOREIGN KEY ("id_room_type") REFERENCES "public"."ru_room_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_room" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_room" ADD FOREIGN KEY ("id_hotel") REFERENCES "public"."ru_hotel" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ru_tour"
-- ----------------------------
ALTER TABLE "public"."ru_tour" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_tour" ADD FOREIGN KEY ("city_id") REFERENCES "public"."ru_city" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ru_tour" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."tour_program"
-- ----------------------------
ALTER TABLE "public"."tour_program" ADD FOREIGN KEY ("tour_id") REFERENCES "public"."ru_tour" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."tour_table"
-- ----------------------------
ALTER TABLE "public"."tour_table" ADD FOREIGN KEY ("tour_id") REFERENCES "public"."ru_tour" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ua_city"
-- ----------------------------
ALTER TABLE "public"."ua_city" ADD FOREIGN KEY ("country_id") REFERENCES "public"."ru_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ua_city" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ua_hotel"
-- ----------------------------
ALTER TABLE "public"."ua_hotel" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ua_hotel" ADD FOREIGN KEY ("city_id") REFERENCES "public"."ru_city" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."ua_hotel" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ua_region"
-- ----------------------------
ALTER TABLE "public"."ua_region" ADD FOREIGN KEY ("country_id") REFERENCES "public"."ru_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."user"
-- ----------------------------
ALTER TABLE "public"."user" ADD FOREIGN KEY ("region_id") REFERENCES "public"."ru_region" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("city_id") REFERENCES "public"."ru_city" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("country_id") REFERENCES "public"."ru_country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
