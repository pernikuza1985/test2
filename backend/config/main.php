<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'modules' => [
        'languages' => [
            'class' => 'common\modules\languages\Module',
            //Языки используемые в приложении
            'languages' => [
                'English' => 'en',
                'Русский' => 'ru',
                'Українська' => 'uk',
            ],
            'default_language' => 'ru', //основной язык (по-умолчанию)
            'show_default' => false, //true - показывать в URL основной язык, false - нет
        ],
    ],
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'controllerMap' => [
        'elfinder' => [
            'access' => ['@'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
            'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
            'class' => '\mihaildev\elfinder\PathController',
            'root' => [
                'baseUrl' => '',
                'basePath' => $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR.'upload',
                'access' => ['read' => '*', 'write' => '*'],
                'name' => 'Папка',
                'options' => [
                    'uploadOverwrite' => false,
                    'uploadAllow' => array('image'),
                    'uploadOrder' => array('allow', 'deny'),
                    'uploadMaxSize' => '3000K',
                    'disabled' => array('rename', 'mkfile'),
                ],
            ],
            'watermark' => [
                'marginRight'    => 500,          // Margin right pixel
                'marginBottom'   => 500,          // Margin bottom pixel
                'quality'        => 95,         // JPEG image save quality
                'transparency'   => 70,         // Water mark image transparency ( other than PNG )
                'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP, // Target image formats ( bit-field )
                'targetMinPixel' => 200         // Target image minimum pixel size
            ]
        ]
    ],
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
//            'class' => 'common\components\Request'
            'csrfParam' => '_csrf-site',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'common\components\UrlManager',
            'rules' => [
//                'languages' => 'languages/default/index',
                '<lang:(ru|en|ua)>/<controller:[\w\-]+>/<action:\w+>' => '<controller>/<action>',
                '' => 'user/profile',
                '<lang:(ru|en|ua)>' => 'user/profile',
                '<controller:[\w\-]+>/<action:\w+>/' => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
