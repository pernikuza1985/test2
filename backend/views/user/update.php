<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = 'Редактировать пользователя' . ' #' . $model->user_name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['admin/user']];
$this->params['breadcrumbs'][] = 'Редактировать пользователя';
?>
<div class="city-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
