<?php
use \yii\widgets\ActiveForm;
use \yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <p>Создайте нового пользователя и добавьте его на этот сайт </p>
                <?php $form = ActiveForm::begin();?>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2" id="examplenameInputname2">UserName <i>(required)</i></label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'user_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2" id="inputEmail3"> Email <i>(required)</i></label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2" id="inputEmail3"> Email subscribe <i>(required)</i></label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'email_subscribe')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">First Name</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Last Name</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>

                    <?= $form->field($model, 'country_id')->dropDownList(['' => 'Выбрать страну'] + array_column(\common\models\Country::find()->orderBy(['title' => SORT_ASC])->asArray()->all(), 'title', 'id')) ?>

                    <?php $cityList = $model->country_id ? array_column(\common\models\Region::find()->orderBy(['title' => SORT_ASC])->where(['country_id' => (int)$model->country_id])->asArray()->all(), 'title', 'id') : [] ?>
                    <?= $form->field($model, 'region_id')->dropDownList($cityList) ?>

                <?php $cityList = $model->country_id ? array_column(\common\models\City::find()->orderBy(['title' => SORT_ASC])->where(['country_id' => (int)$model->country_id])->asArray()->all(), 'title', 'id') : [] ?>
                    <?= $form->field($model, 'city_id')->dropDownList($cityList) ?>

                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Website</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'website')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Company</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'company')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Address main</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'address_main')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Address for mail</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'address_mail')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Password</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'class' => 'form-control', 'value' => $model->password ? $model->useOldPassword  : ''])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Retype Password</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true, 'class' => 'form-control', 'value' => $model->password ? $model->useOldPassword  : ''])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group justify-content-end">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Send User Notification</label>
                            </div>
                            <div class="col-md-9">
                                <label class="custom-control custom-checkbox mb-0">
                                    <input type="checkbox" class="custom-control-input" name="send_message">
                                    <span class="custom-control-label text-dark">Send The New User an Email About thier Account</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label">Facebook</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'facebook')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label">Linkedin</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label">Instagram</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'instagram')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label">Phone</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label">Biographical Info</label>
                            </div>
                            <div class="col-md-9">
<!--                                <textarea class="form-control" name="example-textarea-input" rows="4" placeholder="">idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</textarea>-->
                                <?= $form->field($model, 'info')->textarea(['maxlength' => true, 'class' => 'form-control', 'rows' => '4'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label">Права</label>
                            </div>
                            <div class="col-md-9">
                                <?= $form->field($model, 'role')->dropDownList($model->getRoles(),['maxlength' => true, 'class' => 'form-control', 'rows' => '4'])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-label mt-2">Изображение</label>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control browse-file" placeholder="Choose" readonly>
                                        <label class="input-group-btn">
                                        <span class="btn btn-primary br-tl-0 br-bl-0">
                                            Загрузить
                                            <?= $form->field($model, 'image')->fileInput(['style' => "display: none;", 'accept' => 'image/*'])->label(false); ?>
                                        </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="p-4 border mb-4 br-4">
                                    <div class="upload-images d-flex">
                                        <div>
                                            <img src="/assets/images/faces/male/25.jpg" alt="img" class="w73 h73 border p-0">
                                        </div>
                                        <div class="ml-3 mt-2">
                                            <h6 class="mb-0 mt-3 font-weight-bold">25.jpg</h6>
                                            <small>4.5kb</small>
                                        </div>
                                        <div class="float-right ml-auto">
                                            <a href="#" class="float-right btn btn-icon btn-danger btn-sm mt-5"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0 row justify-content-end">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Добавить нового пользователя</button>
                        </div>
                    </div>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>
<?php
$csrf_param = Yii::$app->request->csrfParam;
$csrf_token = Yii::$app->request->csrfToken;
$this->registerJs(
    "
    $('#user-country_id').on('change', function () {
        countryId = $(this).val();
        data = {'country_id': countryId, '$csrf_param' : '$csrf_token'}; 

        $.ajax({
            method: 'Post',
            dataType: 'json',
            data: data,
            url: '".Url::to(['country/regions'])."',
            success: function(result){
                if(result.status == 'ok'){
                    $('#user-region_id').html(result.content);
                } else {
                    $('#user-region_id').html('');
                }
            }
        });

        $.ajax({
            method: 'Post',
            dataType: 'json',
            data: data,
            url: '".Url::to(['country/cities'])."',
            success: function(result){
                if(result.status == 'ok'){
                    $('#user-city_id').html(result.content);
                } else {
                    $('#user-city_id').html('');
                }
            }
        });
    });
    $('#user-region_id').on('change', function () {
        regionId = $(this).val();
        data = {'region_id': regionId, '$csrf_param' : '$csrf_token'}; 
        $.ajax({
            method: 'Post',
            dataType: 'json',
            data: data,
            url: '".Url::to(['region/cities'])."',
            success: function(result){
                if(result.status == 'ok'){
                    $('#user-city_id').html(result.content);
                } else {
                    $('#user-city_id').html('');
                }
            }
        });
    });
    ",
    \yii\web\View::POS_END,
    'form-city'
)?>