<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserCity */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_name',
            'first_name',
            'last_name',
            'website',
            //'password',
            //'facebook',
            //'linkedin',
            //'instagram',
            //'phone',
            //'info:ntext',
            //'role',
            //'image',
            //'company',
            //'city_id',
            //'country_id',
            //'postal_code',
            //'email_subscribe:email',
            //'address_main',
            //'address_mail',
            //'status',
            //'date_create',
            //'date_last_login',
            //'email:email',
            //'region_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>