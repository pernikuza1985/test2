<?php
use \yii\widgets\ActiveForm;
?>
<div class="col-lg-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Мой профиль</h3>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin()?>
                <div class="d-flex mb-3">
                    <div class="">
                        <img class="avatar brround avatar-xl" src="<?=$model->image ? '/image/'.$model->image : ''?>" alt="Avatar-img">
                    </div>
                    <div class="mt-3 ml-3">
                        <h3 class="mb-1 fs-16 font-weight-bold"><?=$model->first_name ?></h3>
                        <p class="mb-4 fs-12 text-muted"><?=$model->getRole($model->role)?></p>
                    </div>
                </div>
                <div class="input-group">
                    <input type="text" class="form-control browse-file" placeholder="Choose" readonly>
                    <label class="input-group-btn">
                        <span class="btn btn-primary br-tl-0 br-bl-0">
                            Browse <?= $form->field($model, 'image')->fileInput(['style' => "display: none;", 'multiple' => true, 'accept' => 'image/*'])->label(false); ?>
                        </span>
                    </label>
                </div>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'your-email@domain.com'])?>

                <?= $form->field($model, 'password')->passwordInput(['value' => $model->password ? $model->useOldPassword : ''])?>

                <?= $form->field($model, 'password_repeat')->passwordInput(['value' => $model->password ? $model->useOldPassword : ''])?>

                <?= $form->field($model, 'phone')->textInput(['placeholder' => 'placeholder'])?>
                <div class="form-footer">
                    <button class="btn btn-primary btn-block">Save</button>
                </div>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>