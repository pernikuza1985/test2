<?php

use common\models\PriceRoom;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use mihaildev\elfinder\InputFile;
use lo\widgets\modal\ModalAjax;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
//echo ModalAjax::widget([
//    'id' => 'createPriceRoom',
//    'header' => 'Create Company',
//    'toggleButton' => [
//        'label' => 'New Company',
//        'class' => 'btn btn-primary pull-right'
//    ],
//    'url' => \yii\helpers\Url::to(['/price-room/create?']), // Ajax view with form to load
//    'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
//    'size' => ModalAjax::SIZE_LARGE,
//    'options' => ['class' => 'header-primary'],
//    'autoClose' => true,
//    'pjaxContainer' => '#grid-price-room-pjax',
//
//]);
//->where(['room_id' => 0])->all()
$dataProviderPriceRoom = new ActiveDataProvider([
    'query' => PriceRoom::find()->where(['room_id' => $model->id]),
]);
?>
<?php if($type == 'update'): ?>
    <span class="btn btn-success tab-room" data-tab="info">Комната</span>
    <span class="btn btn-primary tab-room" data-tab="price">Цены Комнаты</span>
    <div class="block-price-room tab-block-room" id="tab-price" style="display: none">
        <?= $this->render('_list_price_room', ['dataProvider' => $dataProviderPriceRoom, 'modelRoom' => $model]); ?>
    </div>
<?php endif; ?>
<div class="room-form tab-block-room" id="tab-info">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php $roomType = \common\models\RoomType::find()->orderBy(['title' => SORT_ASC])->asArray()->all();?>
    <?php $roomType = !empty($roomType) ? array_column($roomType, 'title', 'id') : []; ?>
    <?= $form->field($model, 'id_room_type')->dropDownList($roomType) ?>

    <?php $services = Yii::$app->params['services']?>
    <?= $form->field($model, 'services')->dropDownList($services, ['class' => 'form-control select2', 'multiple'=> true, 'data-placeholder' =>"Choose Browser"]); ?>

    <?php if(!empty($model->id_hotels)): ?>
        <?php $hotels = \common\models\Hotel::find()->select('title')->where(['id' => $model->id_hotels->getValue()])->asArray()->all(); ?>
        <?php $hotels =  array_column($hotels, 'title', null); ?>
        <?php $model->id_hotels = !empty($hotels) ? implode('| ', $hotels) : '' ?>
    <?php endif; ?>

    <?php
        $hotels = \common\models\Hotel::find()->select('title, id')->orderBy('title')->asArray()->all();
        $hotels = array_column($hotels, 'title', 'id');
    ?>
    <?= $form->field($model, 'id_hotel')->dropDownList($hotels) ?>

    <?php $users = User::find()->orderBy('first_name')->where(['role'=>User::ROLE_HOTELIER])->all(); ?>

    <?php $users = $users ? array_column($users, 'first_name', 'id') : [];?>
    <?= $form->field($model, 'user_id')->dropDownList(['' => 'Выбрать пользователя'] + $users) ?>

    <?php
    echo InputFile::widget([
        'language'   => 'ru',
        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        'path'       => 'image',    // будет открыта папка из настроек контроллера с добавлением указанной под деритории
        'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
        'name'          => 'Room[images][]',
        'value'          => is_object($model->images) ? implode(',', $model->images->getValue()) : '',
        'buttonName'    => 'Загрузить',
        'multiple'      => true
    ]);
    ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'old_price')->textInput() ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs(
        '$(".tab-room").on("click", function(){
                $(".tab-block-room").hide();
                $("#tab-"+$(this).data("tab")).show();
        });',
        \yii\web\View::POS_END,
        'tab-room'
);?>

<!-- Modal -->
<?php
Pjax::begin();
?>
<div class="modal fade" id="modal-room-price" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Цена комнаты</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<?php
Pjax::end()
?>
<style>
    .elfinder-list-image div{
        width:150px;
        height:150px;
        margin-left:15px;
        margin-top:15px;
    }
    .elfinder-list-image div img{
        width:150px;
        height:150px;
    }
    .image-block .close {
        position: absolute;
        opacity: 0.3;
    }
    .image-block .close:hover {
        opacity: 1;
    }
    .image-block .close:before, .close:after {
        position: absolute;
        left: 15px;
        content: ' ';
        height: 33px;
        width: 2px;
        background-color: #333;
    }
    .image-block .close:before {
        transform: rotate(45deg);
    }
    .image-block .close:after {
        transform: rotate(-45deg);
    }
</style>