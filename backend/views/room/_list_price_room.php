<?php
use yii\helpers\Html;
use yii\grid\GridView;
use \yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="price-room-index">

    <h1>Цены комнат</h1>

    <p>
        <span class='btn btn-success add-price'>Добавить цену</span>
    </p>
    <?php
    Pjax::begin([
        'id' => 'grid-price-room-pjax',
        'linkSelector'=>'.order'
        ]);
         echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'room_id',
                'count_people',
                'count_child',
                'count_day',
                'date_from',
                'date_to',
                'price',
                ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:260px;'],
                'header'=>'Actions',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    //view button
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open" style="cursor: pointer"></span>&nbsp;', '#', [
                            'title' => Yii::t('app', 'View'),
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return '<span class="glyphicon glyphicon-pencil edit-price-room" style="cursor: pointer" data-id="'.$model->id.'"></span>';
                    },
                    'delete' => function ($url, $model) {
                        return '<span class="glyphicon glyphicon-trash delete-price-room" style="cursor: pointer"  data-id="'.$model->id.'"></span>';
                    },
                ],
                ],

            ],
        ]);
    Pjax::end();
     ?>
</div>

<?php $this->registerJs(
    '$(".add-price").on("click", function(){
            $.post("'.\yii\helpers\Url::to(['/price-room/create']).'", {room: '.$modelRoom->id.'}, function(data){
                $("#modal-room-price .modal-body").html(data);
                $("#modal-room-price").modal({
                    show: true
                });                 
            });
        });
        
        $("body").on("click", ".edit-price-room", function () {
            var id = $(this).data("id");
            $.post("'.\yii\helpers\Url::to(['/price-room/update']).'?id="+id, function(data){
                $("#modal-room-price .modal-body").html(data);
                $("#modal-room-price").modal({
                    show: true
                });
            });
        });
        ',
        \yii\web\View::POS_END,
        'modal-form'
);?>
<script>
    $('body').on('click', '.save-price-room', function (e) {
        e.preventDefault();
        var form = $(this).parents('form:first');
        $.post(form.attr('action'), form.serialize() ,function(data){
            $("#modal-room-price").modal('hide');
            $.pjax.reload({container:'#grid-price-room-pjax'});
        });
    });

    $('body').on('click', '.delete-price-room', function () {
        var id = $(this).data("id");
        $.post("<?=\yii\helpers\Url::to(['/price-room/delete/'])?>?id="+id, function(data){
            $.pjax.reload({container:'#grid-price-room-pjax'});
        });
    });
</script>
