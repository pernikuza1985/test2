<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PriceRoom */

$this->title = 'Create Price Room';
$this->params['breadcrumbs'][] = ['label' => 'Price Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-room-create">

    <?= $this->render('_form', [
        'model' => $model, 'roomId' => $roomId
    ]) ?>

</div>
