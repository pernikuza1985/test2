<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Price Rooms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-room-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Price Room', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'options' => [
            'id' => 'grid-company-pjax',
            'class' => 'grid-view111'
        ],

        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'room_id',
            'count_people',
            'count_child',
            'count_day',
            //'date_from',
            //'date_to',
            //'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
