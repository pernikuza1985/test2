<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PriceRoom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-room-form" >
    <?php $form = ActiveForm::begin(
        ['id' => 'form-price-room']
    ); ?>

    <div id="field-form-price-room">
        <div class="row">
            <input type="hidden" name="PriceRoom[room_id]" value="<?=$roomId?>">
            <div class="col-sm-2 col-md-2">
                <?php $roomType = \common\models\RoomType::find()->orderBy(['title' => SORT_ASC])->asArray()->all();?>
                <?php $roomType = !empty($roomType) ? array_column($roomType, 'title', 'id') : []; ?>
                <?= $form->field($model, 'type_room')->dropDownList($roomType)->label(false) ?>
            </div>
            <div class="col-sm-12 col-md-1">
                <?= $form->field($model, 'count_people')->textInput(['placeholder' => 'Взрослых'])->label(false) ?>
            </div>
            <div class="col-sm-6 col-md-1">
                <?= $form->field($model, 'count_child')->textInput(['placeholder' => 'Детей'])->label(false) ?>
            </div>
            <div class="col-sm-1 col-md-1">
                <?= $form->field($model, 'date_from')->textInput(['type' => 'date'])->label(false) ?>
            </div>

            <div class="col-sm-1 col-md-1">
                <?= $form->field($model, 'date_to')->textInput(['type' => 'date'])->label(false) ?>
            </div>

            <div class="col-sm-1 col-md-5">
                <?php $modelRoom = \common\models\Room::findOne($roomId);?>
                <?php $servicesIdHotel = $modelRoom->hotel->services ? $modelRoom->hotel->services : [];?>

                <?php $servicesHotel = [];?>
                <?php $services = Yii::$app->params['services_additional']?>
                <?php foreach ($servicesIdHotel as $service):?>
                    <?php $servicesHotel[$service] = Yii::$app->params['services_additional'][$service]; ?>
                <?php endforeach; ?>
                <?= $form->field($model, 'services')->dropDownList($servicesHotel, ['class' => 'form-control select2', 'multiple'=> true, 'data-placeholder' =>"Choose Browser"])->label(false); ?>
            </div>

            <div class="col-sm-1 col-md-1" style="width: 30px;max-width: 102px;">
                <?= $form->field($model, 'price')->textInput(['placeholder' => 'цена'])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success save-price-room']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
   $('.select2').select2({
        minimumResultsForSearch: Infinity
   });
</script>
