<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PriceRoom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-room-form" >
    <?php $form = ActiveForm::begin(); ?>
        <div id="clone-form-price-room" style="display: none ">
            <div class="row">
                <input type="hidden" name="PriceRoom[room_id][]" value="<?=$roomId?>">
                <div class="col-sm-2 col-md-2">
                    <?php $roomType = \common\models\RoomType::find()->orderBy(['title' => SORT_ASC])->asArray()->all();?>
                    <?php $roomType = !empty($roomType) ? array_column($roomType, 'title', 'id') : []; ?>
                    <?= $form->field($model, 'type_room[]')->dropDownList($roomType)->label(false) ?>
                </div>
                <div class="col-sm-12 col-md-1">
                    <?= $form->field($model, 'count_people[]')->textInput(['placeholder' => 'Взрослых'])->label(false) ?>
                </div>
                <div class="col-sm-6 col-md-1">
                    <?= $form->field($model, 'count_child[]')->textInput(['placeholder' => 'Детей'])->label(false) ?>
                </div>
                <div class="col-sm-1 col-md-1">
                    <?= $form->field($model, 'date_from[]')->textInput(['type' => 'date'])->label(false) ?>
                </div>

                <div class="col-sm-1 col-md-1">
                    <?= $form->field($model, 'date_to[]')->textInput(['type' => 'date'])->label(false) ?>
                </div>

                <div class="col-sm-1 col-md-5">
                    <?php $modelRoom = \common\models\Room::findOne($roomId);?>
                    <?php $servicesIdHotel = $modelRoom->hotel->services ? $modelRoom->hotel->services : [];?>

                    <?php $servicesHotel = [];?>
                    <?php $services = Yii::$app->params['services_additional']?>
                    <?php foreach ($servicesIdHotel as $service):?>
                        <?php $servicesHotel[$service] = Yii::$app->params['services_additional'][$service]; ?>
                    <?php endforeach; ?>
                    <?= $form->field($model, 'services[b]')->dropDownList($servicesHotel, ['class' => 'form-control', 'multiple'=> true, 'data-placeholder' =>"Choose Browser", 'id' => 'clone_service'])->label(false); ?>
                </div>
                <div class="col-sm-1 col-md-1" style="width: 30px;max-width: 102px;">
                    <?= $form->field($model, 'price[]')->textInput(['placeholder' => 'цена'])->label(false) ?>
                </div>
                <div class="col-sm-1 col-md-1 delete-row-price" style="max-width: 15px;width: 15px;float: left;padding: 0;cursor: pointer">
                    <div style="width: 15px;margin-top: 10px">
                        &#10060;
                    </div>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    <?php $form = ActiveForm::begin(
        ['id' => 'form-price-room']
    ); ?>

    <div id="field-form-price-room">
        <div class="row">
            <input type="hidden" name="PriceRoom[room_id][]" value="<?=$roomId?>">
            <div class="col-sm-2 col-md-2">
                <?php $roomType = \common\models\RoomType::find()->orderBy(['title' => SORT_ASC])->asArray()->all();?>
                <?php $roomType = !empty($roomType) ? array_column($roomType, 'title', 'id') : []; ?>
                <?= $form->field($model, 'type_room[]')->dropDownList($roomType)->label(false) ?>
            </div>
            <div class="col-sm-12 col-md-1">
                <?= $form->field($model, 'count_people[]')->textInput(['placeholder' => 'Взрослых'])->label(false) ?>
            </div>
            <div class="col-sm-6 col-md-1">
                <?= $form->field($model, 'count_child[]')->textInput(['placeholder' => 'Детей'])->label(false) ?>
            </div>
            <div class="col-sm-1 col-md-1">
                <?= $form->field($model, 'date_from[]')->textInput(['type' => 'date'])->label(false) ?>
            </div>

            <div class="col-sm-1 col-md-1">
                <?= $form->field($model, 'date_to[]')->textInput(['type' => 'date'])->label(false) ?>
            </div>

            <div class="col-sm-1 col-md-5">
                <?php $modelRoom = \common\models\Room::findOne($roomId);?>
                <?php $servicesIdHotel = $modelRoom->hotel->services ? $modelRoom->hotel->services : [];?>

                <?php $servicesHotel = [];?>
                <?php $services = Yii::$app->params['services_additional']?>
                <?php foreach ($servicesIdHotel as $service):?>
                    <?php $servicesHotel[$service] = Yii::$app->params['services_additional'][$service]; ?>
                <?php endforeach; ?>
                <?= $form->field($model, 'services[0]')->dropDownList($servicesHotel, ['class' => 'form-control select2', 'multiple'=> true, 'data-placeholder' =>"Choose Browser"])->label(false); ?>
            </div>

            <div class="col-sm-1 col-md-1" style="width: 30px;max-width: 102px;">
                <?= $form->field($model, 'price[]')->textInput(['placeholder' => 'цена'])->label(false) ?>
            </div>
            <div class="col-sm-1 col-md-1 delete-row-price" style="max-width: 15px;width: 15px;float: left;padding: 0;cursor: pointer">
                <div style="width: 15px;margin-top: 10px">
                    &#10060;
                </div>
            </div>
        </div>
    </div>

    <button id="add-price">Добавить цену</button>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success save-price-room']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    var counterClone = 0;
    var cloneForm = $('#clone-form-price-room > div').clone();
    $("#field-form-price-room").on('click', '.delete-row-price' ,function () {
        $(this).parents('.row:first').remove();
    })

    $('#add-price').on('click', function (e) {
        counterClone++;
        e.preventDefault();
        copyCloneForm = cloneForm.clone();
        // copyCloneForm.find("input[type=hidden").attr('name', 'PriceRooms[services]['+counterClone+'][]');;
        copyCloneForm.find('#clone_service').attr('name', 'PriceRoom[services]['+counterClone+'][]');
        copyCloneForm.find('#clone_service').addClass('select2');
        copyCloneForm.find('#clone_service').attr('id', 'clone_service-'+counterClone);
        copyCloneForm.appendTo("#field-form-price-room");
        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });
    });
    $('.select2').select2({
        minimumResultsForSearch: Infinity
    });
</script>
