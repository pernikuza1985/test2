<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PriceRoom */

$this->title = 'Update Price Room: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Price Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="price-room-update">

    <?= $this->render('_update-form', [
        'model' => $model, 'roomId' => $roomId
    ]) ?>

</div>
