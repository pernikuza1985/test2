<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TourTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-table-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'tour_type')->dropDownList(\Yii::$app->params['tour_type']) ?>

    <?= $form->field($model, 'tour_price_type')->dropDownList(\Yii::$app->params['tour_price_type']) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'price_old')->textInput() ?>

    <?php $tourProgram = \common\models\Tour::find()->orderBy('title')->asArray()->all();?>
    <?php $tourProgram = array_column($tourProgram, 'title', 'id') ?>
    <?= $form->field($model, 'tour_id')->dropDownList($tourProgram); ?>

    <?= $form->field($model, 'date')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
