<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TourTable */

$this->title = 'Create Tour Table';
$this->params['breadcrumbs'][] = ['label' => 'Tour Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
