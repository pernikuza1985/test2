<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TourProgram */

$this->title = 'Update Tour Program: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tour Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tour-program-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
