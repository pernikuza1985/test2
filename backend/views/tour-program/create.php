<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TourProgram */

$this->title = 'Create Tour Program';
$this->params['breadcrumbs'][] = ['label' => 'Tour Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-program-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
