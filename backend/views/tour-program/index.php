<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SearchTourProgram */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tour Programs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-program-index">

    <p>
        <?= Html::a('Create Tour Program', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'tour_id',
            'image',
            'description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
