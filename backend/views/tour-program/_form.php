<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model common\models\TourProgram */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-program-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php $tourProgram = \common\models\Tour::find()->orderBy('title')->asArray()->all();?>
    <?php $tourProgram = array_column($tourProgram, 'title', 'id') ?>
    <?= $form->field($model, 'tour_id')->dropDownList($tourProgram); ?>

    <?php
        echo $form->field($model, 'image')->widget(InputFile::className(), [
            'language'      => 'ru',
            'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'path'          => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
            'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
            'buttonName'    => 'Загрузить',
        ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
