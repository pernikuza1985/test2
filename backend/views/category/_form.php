<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <label class="form-label">Логотип</label>
        <?php
        echo \mihaildev\elfinder\InputFile::widget([
            'language'   => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'path'       => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
            'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
            'name'          => 'Category[image]',
            'value'          => $model->image,
            'buttonName'    => 'Загрузить',
            'multiple'      => false
        ]);
        ?>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= \backend\widgets\Seo::widget(['model' => $model, 'form' => $form]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
