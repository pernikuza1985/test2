<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SearchHotelQuestionAnswer */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hotel Question Answers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-question-answer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Hotel Question Answer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'question',
            'answer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
