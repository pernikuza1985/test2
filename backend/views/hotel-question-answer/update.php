<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HotelQuestionAnswer */

$this->title = 'Update Hotel Question Answer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hotel Question Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hotel-question-answer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
