<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HotelQuestionAnswer */

$this->title = 'Create Hotel Question Answer';
$this->params['breadcrumbs'][] = ['label' => 'Hotel Question Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-question-answer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
