<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SearchHotel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hotels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-index">
    <p>
        <?= Html::a('Create Hotel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            'address',
            'phone',
            'site',
            'email:email',
            //'description',
            //'body:ntext',
            //'url:url',
            //'image',
            //'images',
            //'user_id',
            //'type_id',
            //'category_id',
            //'discount',
            //'date_create',
            //'country_id',
            //'region_id',
            //'city_id',
            //'min_age',
            //'max_age',
            //'minimum_quantity',
            //'deposite',
            //'extra',
            //'type_hotel',
            //'identify',
            //'included',
            //'excluded',
            //'payment_methods',
            //'fax',
            //'toll_free',
            //'certification',
            //'video',
            //'h1',
            //'meta_title',
            //'meta_description',
            //'seo_text:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
