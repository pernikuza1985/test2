<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Hotel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hotels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hotel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address',
            'phone',
            'site',
            'email:email',
            'description',
            'body:ntext',
            'url:url',
            'image',
            'images',
            'user_id',
            'type_id',
            'category_id',
            'discount',
            'date_create',
            'country_id',
            'region_id',
            'city_id',
            'min_age',
            'max_age',
            'minimum_quantity',
            'deposite',
            'extra',
            'type_hotel',
            'identify',
            'included',
            'excluded',
            'payment_methods',
            'fax',
            'toll_free',
            'certification',
            'video',
            'h1',
            'meta_title',
            'meta_description',
            'seo_text:ntext',
        ],
    ]) ?>

</div>
