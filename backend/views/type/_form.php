<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\type */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <label class="form-label">Флаг страны</label>
        <div class="input-group">
            <input type="text" class="form-control browse-file" placeholder="Choose" readonly>
            <label class="input-group-btn">
                <span class="btn btn-primary br-tl-0 br-bl-0">
                    Загрузить
                    <?= $form->field($model, 'image')->fileInput(['style' => "display: none;", 'multiple' => true, 'accept' => 'image/*'])->label(false); ?>
                </span>
            </label>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= \backend\widgets\Seo::widget(['model' => $model, 'form' => $form]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
