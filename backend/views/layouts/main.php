<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use \common\components\UrlManager;
use \yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script src="/admin/assets/21a92fbb/jquery.js"></script>
        <script src="/admin/assets/js/jquery-3.5.1.min.js"></script>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="page">
        <div class="page-main h-100">
            <!--Header Section-->
            <div class="app-header1 header-search-icon">
                <div class="header-style1">
                    <!-- Logo -->
                    <a class="header-brand" href="/">
                        <img src="assets/images/brand/logo.png" class="header-brand-img desktop-logo" alt="logo">
                        <img src="assets/images/brand/favicon.png" class="header-brand-img mobile-logo" alt="logo">
                    </a>
                    <!-- Logo -->
                </div>

                <!-- app-sidebar-toggle -->
                <div class="app-sidebar__toggle" data-toggle="sidebar">
                    <a class="open-toggle" href="#"><i class="fe fe-menu"></i></a>
                    <a class="close-toggle" href="#"><i class="fe fe-x"></i></a>
                </div>
                <!-- app-sidebar-toggle -->

                <!-- Country Dropdown -->
                <div class="dropdown d-none d-md-flex country-selector">
                    <a href="#" class="d-flex nav-link leading-none" data-toggle="dropdown">
                        <?php $currentLang = \Yii::$app->language && in_array(\Yii::$app->language, array_keys(\Yii::$app->params['language'])) ? \Yii::$app->language : 'ru'; ?>
                        <img style="background: inherit" src="/admin/assets/images/flags/<?=\Yii::$app->params['language'][$currentLang]['flag']?>" alt="img" class="avatar avatar-xs mr-1 align-self-center">
                        <div class="ml-1"><?=\Yii::$app->params['language'][$currentLang]['title']?></div>
                    </a>
                    <div class="language-width dropdown-menu dropdown-menu-left dropdown-menu-arrow">

                        <?php foreach (\Yii::$app->params['language'] as $lang => $value): ?>
                            <?php if($currentLang == $lang):?>
                                <?php continue; ?>
                            <?php endif; ?>
<!--                            --><?php //die(pr(\Yii::$app->params['language'])); ?>
                            <a href="<?=Url::to([null, 'lang' => $lang]);?>" class="dropdown-item d-flex pb-3">
                                <div class="font-weight-semibold"><?=$value['title']?></div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>

                <!-- Header Right -->
                <div class="d-flex ml-auto header-right-icons">
                    <!-- Full Screen -->
                    <div class="dropdown d-none d-md-flex" >
                        <a class="nav-link icon full-screen-link">
                            <i class="fe fe-maximize-2"  id="fullscreen-button"></i>
                        </a>
                    </div>
                    <!-- /Full Screen -->

                    <!-- Profile Dropdown -->
                    <div class="dropdown">
                        <a href="#" class="nav-link pr-0 leading-none user-img" data-toggle="dropdown">
                            <img src="assets/images/faces/male/24.jpg" alt="profile-img" class="avatar avatar-md brround">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">
                            <a class="dropdown-item" href="main-profile.html">
                                <i class="dropdown-icon fe fe-user text-primary"></i> My Profile
                            </a>
                            <a class="dropdown-item" href="/log-out">
                                <i class="dropdown-icon fe fe-power text-primary"></i> Log out
                            </a>
                        </div>
                    </div>
                    <!-- Profile Dropdown -->
                </div>
                <!-- Header Right -->
            </div>
            <!-- Header Section -->

            <!-- Sidebar menu-->
            <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
            <aside class="app-sidebar doc-sidebar">
                <a class="header-brand sidemenu-header-brand" href="index.html">
                    <img src="assets/images/brand/logo1.png" class="header-brand-img brand-logo-main" alt="Gowell logo">
                    <img src="assets/images/brand/favicon-white.png" class="header-brand-img brand-logo-toggled" alt="Gowell logo">
                </a>

                <!--User Section-->
                <div class="app-sidebar__user">
                    <div class="dropdown user-pro-body text-center">
                        <div class="user-pic">
                            <img src="assets/images/faces/male/24.jpg" alt="user-img" class="rounded-circle mb-1">
                        </div>
                        <div class="user-info">
                            <h5 class="mb-0 font-weight-bold fs-16">Lilly Sparter</h5>
                            <span class="app-sidebar__user-name text-sm text-white-80">App Developer</span>
                        </div>
                    </div>
                </div>
                <!--/User Section-->

                <!-- Sidemenu Navigation Section-->
                <ul class="side-menu">
                    <li class="slide">
                        <a class="side-menu__item <?= \Yii::$app->controller->id == 'user' && \Yii::$app->controller->action->id == 'profile' ? 'active' : '' ?>" href="<?=Url::to(['/']);?>">
                            <i class="side-menu__icon fe fe-settings"></i><span class="side-menu__label">Профиль</span>
                        </a>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= in_array(\Yii::$app->controller->id, ['tour', 'tour-places', 'tour-tables', 'tour-program']) && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-repeat"></i><span class="side-menu__label">Туры</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['tour/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Туры</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ тур</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour-places/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour-places' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Места туров</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour-places/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour-places' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ место тура</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour-table/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour-table' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Таблицы туров</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour-table/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour-table' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ таблицу туров</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour-program/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour-program' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Программы туров</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['tour-program/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'tour-program' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ программу тура</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= in_array(\Yii::$app->controller->id, ['hotel', 'room', 'typeRoom']) && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Санатории</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['hotel/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'hotel' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Санатории</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['hotel/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'hotel' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ санаторий</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['room/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'room' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Комнаты</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['room/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'room' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ комнату</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['room-type/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'typeRoom' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Типы комнаты</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['room-type/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'typeRoom' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ тип комнаты</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['hotel-question-answer/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'hotelQuestionAnswer' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Воппрос - ответ</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['hotel-question-answer/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'hotelQuestionAnswer' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ Воппрос - ответ</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-shopping-cart"></i><span class="side-menu__label">Заказы</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="main-call.html" class="slide-item">Обращения</a>
                            </li>
                            <li>
                                <a href="main-add_call.html" class="slide-item">+ обращение</a>
                            </li>
                            <li>
                                <a href="main-tour.html" class="slide-item">Туры</a>
                            </li>
                            <li>
                                <a href="main-add_call_tour.html" class="slide-item">+ заказ тура</a>
                            </li>
                            <li>
                                <a href="main-hotel.html" class="slide-item">Cанатории</a>
                            </li>
                            <li>
                                <a href="main-add_call_hotel.html" class="slide-item">+ заказ санатория</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= \Yii::$app->controller->id == 'category' && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Категории</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['category/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'category' && in_array(\Yii::$app->controller->action->id, ['index'])  ? 'active' : '' ?>">Все категории</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['category/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'category' && in_array(\Yii::$app->controller->action->id, ['create'])  ? 'active' : '' ?>">Добавить категорию</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= \Yii::$app->controller->id == 'country' && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Страны</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['country/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'country' && in_array(\Yii::$app->controller->action->id, ['index'])  ? 'active' : '' ?>">Все страны</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['country/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'country' && in_array(\Yii::$app->controller->action->id, ['create'])  ? 'active' : '' ?>">Добавить страну</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= \Yii::$app->controller->id == 'region' && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Регионы</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['region/index']);?>" class="slide-item <?= \Yii::$app->controller->id == 'region' && in_array(\Yii::$app->controller->action->id, ['index'])  ? 'active' : '' ?>">Все регионы</a>
                            </li>``
                            <li>
                                <a href="<?=Url::to(['region/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'region' && in_array(\Yii::$app->controller->action->id, ['create'])  ? 'active' : '' ?>">Добавить регион</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= \Yii::$app->controller->id == 'city' && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Города</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['city/index']);?>" class="slide-item" href="<?= \Yii::$app->controller->id == 'city' && in_array(\Yii::$app->controller->action->id, ['create'])  ? 'active' : '' ?>">Все города</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['city/create']);?>" class="slide-item <?= \Yii::$app->controller->id == 'city' && in_array(\Yii::$app->controller->action->id, ['create'])  ? 'active' : '' ?>">Добавить город</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= \Yii::$app->controller->id == 'type' && in_array(\Yii::$app->controller->action->id, ['index', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Типы</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['type/index']);?>" class="slide-item">Все типы</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['type/create']);?>" class="slide-item">Добавить тип</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show <?= \Yii::$app->controller->id == 'user' && in_array(\Yii::$app->controller->action->id, ['list', 'create', 'update', 'view'])  ? 'active' : '' ?>" href="#"><i class="side-menu__icon fe fe-grid"></i><span class="side-menu__label">Пользователи</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="/admin/user/index/" class="slide-item <?= \Yii::$app->controller->id == 'user' && \Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">Все пользователи</a>
                            </li>
                            <li>
                                <a href="/admin/user/create/" class="slide-item <?= \Yii::$app->controller->id == 'user' && \Yii::$app->controller->action->id == 'create' ? 'active' : '' ?>">+ пользователя</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-grid"></i><span class="side-menu__label">Партнеры</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="main-partners.html" class="slide-item">Все партнеры</a>
                            </li>
                            <li>
                                <a href="main-add_partners.html" class="slide-item">+ партнера</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-layers"></i><span class="side-menu__label">Статьи</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="main-articles.html" class="slide-item">Все статьи</a>
                            </li>
                            <li>
                                <a href="main-add_articles.html" class="slide-item">Добавить статью</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-layers"></i><span class="side-menu__label">Новости</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="<?=Url::to(['news/index']);?>" class="slide-item">Все новости</a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['news/create']);?>" class="slide-item">Добавить новость</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-list"></i><span class="side-menu__label">Подписки</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="main-subscribes.html" class="slide-item">Все подписки</a>
                            </li>
                            <li>
                                <a href="main-add_subscribe.html" class="slide-item">+ подписку</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-database"></i><span class="side-menu__label"> Отзывы</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="main-reviews.html" class="slide-item">Все отзывы</a>
                            </li>
                            <li>
                                <a href="main-add_review.html" class="slide-item">Добавить отзыв</a>
                            </li>
                        </ul>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item slide-show" href="#"><i class="side-menu__icon fe fe-database"></i><span class="side-menu__label"> Жалобы</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li>
                                <a href="main-abuses.html" class="slide-item">Все жалобы</a>
                            </li>
                            <li>
                                <a href="main-add_abuses.html" class="slide-item">Добавить жалобу</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidemenu Navigation Section-->
            </aside>
            <!-- Sidebar menu-->
            <div class="app-content">
                <div class="side-app">
                    <div class="page-header">
                        <h4 class="page-title"><?= Html::encode($this->title) ?></h4>
                        <?=
                        Breadcrumbs::widget(
                            [
                                'homeLink' => ['label' => '<i class="fe fe-settings mr-1"></i>Settings', 'url' => ['index'], 'encode' => false],
                                'tag' => 'ol',
                                'itemTemplate' => '<li class="breadcrumb-item" >{link}</li>',
                                'activeItemTemplate' => '<li class="breadcrumb-item active" >{link}</li>',
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]
                        ) ?>
                    </div>
                    <!--app content-->
                    <?= $content ?>
                </div>
            </div>
        </div>
        <div id="global-loader" class="bg-primary">
            <div class="loader-svg-img">
                <img src="assets/images/brand/2.png" class="" alt="">
                <div class="flight-icon"><i class="fa fa-plane"></i></div>
            </div>
        </div>

        <footer class="footer br-bl-7 br-br-7 border-top-0">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                        Copyright © 2021 <a href="/">LimedTravel</a>.
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>