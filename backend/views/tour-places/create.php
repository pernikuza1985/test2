<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TourPlaces */

$this->title = 'Create Tour Places';
$this->params['breadcrumbs'][] = ['label' => 'Tour Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-places-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
