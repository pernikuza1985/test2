<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SearchTour */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tours';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-index">

    <p>
        <?= Html::a('Create Tour', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'address',
            'phone',
            'site',
            'email:email',
            //'description',
            //'body:ntext',
            //'url:url',
            //'image',
            //'user_id',
            //'discount',
            //'date_create',
            //'country_id',
            //'region_id',
            //'city_id',
            //'min_age',
            //'max_age',
            //'minimum_quantity',
            //'required',
            //'trip',
            //'identify',
            //'fax',
            //'toll_free',
            //'certification',
            //'video',
            //'h1',
            //'meta_title',
            //'meta_description',
            //'seo_text:ntext',
            //'title',
            //'included',
            //'excluded',
            //'type_id',
            //'category_id',
            //'established',
            //'services',
            //'payment_methods',
            //'images:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
