<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \common\models\Type;
use \common\models\Category;
use \yii\helpers\Url;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Tour */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row row-cards">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6, 'class' => 'content']) ?>

                <div class="form-group">
                    <label class="form-label">Фото</label>
                    <?php
                    echo InputFile::widget([
                        'language'   => 'ru',
                        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        'path'       => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
                        'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
                        'options'       => ['class' => 'form-control'],
                        'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
                        'name'          => 'Tour[image]',
                        'value'          => $model->image,
                        'buttonName'    => 'Загрузить',
                        'multiple'      => false
                    ]);
                    ?>
                </div>

                <!--                --><?//= $form->field($model, 'images')->fileInput(['class' => 'fancyuploader', 'accept' => '.jpg, .png, image/jpeg, image/png', 'multiple' => true]) ?>

                <?php $user = ['' => 'Выбрать пользователя'] + array_column(User::find()->where(['role' => User::ROLE_HOTELIER])->orderBy(['user_name' => SORT_ASC])->asArray()->all(), 'user_name', 'id')?>
                <?= $form->field($model, 'user_id')->dropDownList($user) ?>
                <input type="hidden" name="Tour[images][]">
                <div class="form-group">
                    <label class="form-label">Картинки</label>
                    <?php
                    //                echo $form->field($model, 'images')->widget(InputFile::className(), [
                    //                    'language'      => 'ru',
                    //                    'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                    //                    'path'          => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
                    //                    'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                    //                    'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
                    //                    'options'       => ['class' => 'form-control'],
                    //                    'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
                    //                    'buttonName'    => 'Загрузить',
                    //                    'multiple'      => true       // возможность выбора нескольких файлов
                    //                ]);

                    echo InputFile::widget([
                        'language'   => 'ru',
                        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        'path'       => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
                        'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
                        'options'       => ['class' => 'form-control'],
                        'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
                        'name'          => 'Tour[images][]',
                        'value'          => is_object($model->images) ? implode(',', $model->images->getValue()) : '',
                        'buttonName'    => 'Загрузить',
                        'multiple'      => true
                    ]);

                    ?>
                </div>


                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <?php $type =  array_column(Type::find()->orderBy(['title' => SORT_ASC])->asArray()->all(), 'title', 'id')?>
                        <?= $form->field($model, 'type_id')->dropDownList($type, ['multiple' => true, 'class' => 'form-control select2']) ?>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <?php $category = array_column(Category::find()->orderBy(['title' => SORT_ASC])->asArray()->all(), 'title', 'id')?>
                        <?= $form->field($model, 'category_id')->dropDownList($category, ['multiple' => true, 'class' => 'form-control select2']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <?= $form->field($model, 'discount')->dropDownList(['' => 'Выбрать скидку'] + Yii::$app->params['discount']) ?>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <?php $country = ['' => 'Выбрать срану'] + array_column(\common\models\Country::find()->orderBy(['title' => SORT_ASC])->asArray()->all(), 'title', 'id')?>
                        <?= $form->field($model, 'country_id')->dropDownList($country) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <?php $regionList = $model->country_id ? array_column(\common\models\Region::find()->orderBy(['title' => SORT_ASC])->where(['country_id' => (int)$model->country_id])->asArray()->all(), 'title', 'id') : [] ?>
                        <?= $form->field($model, 'region_id')->dropDownList($regionList);?>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <?php
                        $cityList = [];
                        if((int)$model->country_id){
                            $modelCity = \common\models\City::find()->orderBy(['title' => SORT_ASC])
                                ->where(['country_id' => (int)$model->country_id]);

                            if((int)$model->region_id){
                                $modelCity = $modelCity->where(['region_id' => (int)$model->region_id]);
                            }
                            $modelCity = $modelCity->asArray()->all();
                            $cityList = array_column($modelCity, 'title', 'id');
                        }
                        ?>
                        <?= $form->field($model, 'city_id')->dropDownList($cityList) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">
                        <?= $form->field($model, 'min_age')->textInput() ?>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <?= $form->field($model, 'max_age')->textInput() ?>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <?= $form->field($model, 'minimum_quantity')->textInput() ?>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <?= $form->field($model, 'trip')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'required')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'identify')->textInput() ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?php

                        if(is_string($model->places_id)){
                            $value = $model->places_id;
                        } elseif($model->places_id && is_array($model->places_id->getValue())){
                            $dataPlaces = [];
                            foreach ($model->places as $place){
                                $dataPlaces[] = $place->title;
                            }
                            $value = implode('| ', $dataPlaces);
                            $value = $value.'| ';
                        }
                        ?>
                        <?= $form->field($model, 'places_id')->textInput(['id' => 'tags', 'class' => 'form-control w-100', 'value' => $value]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <?= $form->field($model, 'included')->dropDownList(Yii::$app->params['included'], ['class' => 'form-control select2', 'multiple' => true]) ?>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <?= $form->field($model, 'excluded')->dropDownList(Yii::$app->params['excluded'], ['class' => 'form-control select2', 'multiple' => true]) ?>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <?= $form->field($model, 'landing')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'established')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'services')->dropDownList(Yii::$app->params['services'], ['class' => 'form-control select2', 'multiple' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'payment_methods')->dropDownList(Yii::$app->params['payment_methods'], ['class' => 'form-control select2', 'multiple' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'toll_free')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?= $form->field($model, 'certification')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>

                <?= \backend\widgets\Seo::widget(['model' => $model, 'form' => $form]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
$csrf_param = Yii::$app->request->csrfParam;
$csrf_token = Yii::$app->request->csrfToken;
$this->registerJs(
    "
    $('#tour-country_id').on('change', function () {
        countryId = $(this).val();
        data = {'country_id': countryId, '$csrf_param' : '$csrf_token'}; 

        $.ajax({
            method: 'Post',
            dataType: 'json',
            data: data,
            url: '".Url::to(['country/regions'])."',
            success: function(result){
                if(result.status == 'ok'){
                    $('#tour-region_id').html(result.content);
                } else {
                    $('#tour-region_id').html('');
                }
            }
        });

        $.ajax({
            method: 'Post',
            dataType: 'json',
            data: data,
            url: '".Url::to(['country/cities'])."',
            success: function(result){
                if(result.status == 'ok'){
                    $('#tour-city_id').html(result.content);
                } else {
                    $('#tour-city_id').html('');
                }
            }
        });
    });
    $('#tour-region_id').on('change', function () {
        regionId = $(this).val();
        data = {'region_id': regionId, '$csrf_param' : '$csrf_token'}; 
        $.ajax({
            method: 'Post',
            dataType: 'json',
            data: data,
            url: '".Url::to(['region/cities'])."',
            success: function(result){
                if(result.status == 'ok'){
                    $('#tour-city_id').html(result.content);
                } else {
                    $('#tour-city_id').html('');
                }
            }
        });
    });
    ",
    \yii\web\View::POS_END,
    'form-city'
)?>
<?php
$tourPlaces = \common\models\TourPlaces::find()->select('title')->orderBy(['title' => SORT_ASC])->asArray()->column();
//die(pr($tourPlaces));
?>

<script>
    $( function() {
        var availableTags = <?= '["' . implode('", "', $tourPlaces) . '"]' ?>;
        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( "#tags" )
            // don't navigate away from the field on tab when selecting an item
            .on( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                minLength: 0,
                source: function( request, response ) {
                    // delegate back to autocomplete, but extract the last term
                    response( $.ui.autocomplete.filter(
                        availableTags, extractLast( request.term ) ) );
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "| " );
                    return false;
                }
            });
    } );
</script>
<style>
    .elfinder-list-image div{
        width:150px;
        height:150px;
        margin-left:15px;
        margin-top:15px;
    }
    .elfinder-list-image div img{
        width:150px;
        height:150px;
    }
    .close {
        position: absolute;
        opacity: 0.3;
    }
    .close:hover {
        opacity: 1;
    }
    .close:before, .close:after {
        position: absolute;
        left: 15px;
        content: ' ';
        height: 33px;
        width: 2px;
        background-color: #333;
    }
    .close:before {
        transform: rotate(45deg);
    }
    .close:after {
        transform: rotate(-45deg);
    }
</style>