<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RoomType */

$this->title = 'Update Room Type: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Room Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="room-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
