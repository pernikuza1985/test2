<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= \backend\widgets\Seo::widget(['model' => $model, 'form' => $form]) ?>

    <div class="form-group">
        <label class="form-label">Фото</label>
        <?php
        echo InputFile::widget([
            'language'   => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'path'       => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
            'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
            'name'          => 'City[image]',
            'value'          => $model->image,
            'buttonName'    => 'Загрузить',
            'multiple'      => false
        ]);
        ?>
    </div>

    <?= $form->field($model, 'country_id')->dropDownList(['0' => 'Выбрать страну'] + array_column(\common\models\Country::find()->orderBy(['title' => SORT_ASC])->asArray()->all(), 'title', 'id')) ?>

    <?php $regionList = $model->country_id ? array_column(\common\models\Region::find()->orderBy(['title' => SORT_ASC])->where(['country_id' => (int)$model->country_id])->asArray()->all(), 'title', 'id') : [] ?>
    <?= $form->field($model, 'region_id')->dropDownList($regionList) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
    $csrf_param = Yii::$app->request->csrfParam;
    $csrf_token = Yii::$app->request->csrfToken;
    $this->registerJs(
        "
            $('#city-country_id').on('change', function () {
                countryId = $(this).val();
                data = {'country_id': countryId, '$csrf_param' : '$csrf_token'}; 

                $.ajax({
                    method: 'Post',
                    dataType: 'json',
                    data: data,
                    url: '".Url::to(['country/regions'])."',
                    success: function(result){
                        if(result.status == 'ok'){
                            $('#city-region_id').html(result.content);
                        } else {
                            $('#city-region_id').html('');
                        }
                    }
                });
            });",
    \yii\web\View::POS_END,
        'form-city'
)?>

