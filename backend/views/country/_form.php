<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model common\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <label class="form-label">Фото</label>
        <?php
        echo InputFile::widget([
            'language'   => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'path'       => 'image', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
            'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="image-block"><div class="input-group">{input}<span class="input-group-btn">{button}</span></div><div class="row elfinder-list-image">{list_image}</div></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-primary br-tl-0 br-bl-0'],
            'name'          => 'Country[image]',
            'value'          => $model->image,
            'buttonName'    => 'Загрузить',
            'multiple'      => false
        ]);
        ?>
    </div>
    <?= \backend\widgets\Seo::widget(['model' => $model, 'form' => $form]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
    .close-image {
        width: 25px;
        height: 25px;
        position: absolute;
        background-color: red;
        margin-top: -145px;
        margin-left: 119px;
        border-radius: 5px;
    }
    .close-image:after{
        position:relative;
        content:"\d7";
        font-size:32px;
        color:white;
        /*font-weight:bold;*/
        top:-13px;
        left:3px
    }
</style>
<script>
    $('.close-image')
</script>