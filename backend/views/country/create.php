<?php

use yii\helpers\Html;
?>
<div class="app-content">
    <div class="side-app">
        <div class="page-header">
            <h4 class="page-title">Добавить страну</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fe   fe-settings mr-1"></i> Settings</a></li>
                <li class="breadcrumb-item active" aria-current="page">Добавить новую страну</li>
            </ol>
        </div>

        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
