<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/css/dashboard/sidemenu.css',
        'assets/plugins/bootstrap/css/bootstrap.min.css',
        'assets/css/style.css',
        'assets/css/dashboard/admin-custom.css',
        'assets/plugins/wysiwyag/richtext.css',
        'assets/plugins/fancyuploder/fancy_fileupload.css',
        'assets/plugins/autocomplete/jquery.autocomplete.css',
        'assets/plugins/perfect-scrollbar/perfect-scrollbar.css',
        'assets/css/icons.css',
        'assets/color-skins/color.css',
    ];
    public $js = [
//        'assets/js/jquery-3.5.1.min.js',
        'assets/plugins/bootstrap/js/popper.min.js',
        'assets/plugins/bootstrap/js/bootstrap.bundle.js',
        'assets/js/selectize.min.js',
        'assets/plugins/toggle-sidebar/sidemenu.js',
        'assets/plugins/select2/select2.full.min.js',
        'assets/js/select2.js',
        'assets/plugins/date-picker/jquery-ui.js',
        'assets/js/date-picker.js',
        'assets/plugins/wysiwyag/jquery.richtext.js',
        'assets/js/formeditor.js',

        'assets/plugins/fancyuploder/jquery.ui.widget.js',
        'assets/plugins/fancyuploder/jquery.fileupload.js',
        'assets/plugins/fancyuploder/jquery.iframe-transport.js',
        'assets/plugins/fancyuploder/jquery.fancy-fileupload.js',
        'assets/plugins/fancyuploder/fancy-uploader.js',


        'assets/plugins/perfect-scrollbar/perfect-scrollbar.js',
        'assets/plugins/perfect-scrollbar/p-scroll.js',
        'assets/plugins/counters/counterup.min.js',
        'assets/plugins/counters/waypoints.min.js',
        'assets/js/admin-custom.js',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
