<?php

namespace backend\controllers;

use common\models\City;
use common\models\Region;
use Yii;
use common\models\Country;
use backend\models\CountrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\BaseInflector as Inflector;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends MyController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        //model
        $searchModel = new CountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Country model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Country model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Country();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Country model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Country model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Country the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Country::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionRegions()
    {       $result['status'] = false;
        if(Yii::$app->request->isAjax && isset($_POST['country_id'])){
            $regions = Region::find()
                ->where(['country_id' => (int) $_POST['country_id']])
                ->orderBy(['title' => SORT_ASC])
                ->with('country')
                ->asArray()
                ->all();

            if($regions){
                $result['status'] = 'ok';
                $regions = array_column($regions,  'title', 'id');
                $result['content'] = '<option value="">Выбрать регион</option>';
                foreach ($regions as $id => $region){
                    $result['content'] .= '<option value="'.$id.'">'.$region.'</option>';
                }

            }
            return \yii\helpers\Json::encode($result);
        }

    }

    public function actionCities()
    {
        $result['status'] = false;
        if(Yii::$app->request->isAjax && isset($_POST['country_id'])){
            $cities = City::find()
                ->where(['country_id' => (int) $_POST['country_id']])
                ->orderBy(['title' => SORT_ASC])
                ->with('country')
                ->asArray()
                ->all();

            if($cities){
                $result['status'] = 'ok';
                $regions = array_column($cities,  'title', 'id');
                $result['content'] = '<option value="">Выбрать город</option>';
                foreach ($regions as $id => $region){
                    $result['content'] .= '<option value="'.$id.'">'.$region.'</option>';
                }

            }
            return \yii\helpers\Json::encode($result);
        }

    }
}
