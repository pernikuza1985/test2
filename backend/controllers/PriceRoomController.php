<?php

namespace backend\controllers;

use common\models\Room;
use common\models\RoomType;
use Yii;
use common\models\PriceRoom;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PriceRoomController implements the CRUD actions for PriceRoom model.
 */
class PriceRoomController extends MyController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PriceRoom models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PriceRoom::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddPriceRoomToSession(){

        if(Yii::$app->request->isAjax && $_POST['roomId']){
            $session = Yii::$app->session;
            $roomId = $_POST['roomId'];
            $priceRoom = ['room_id' => $roomId, 'lang' => Yii::$app->language];
            $session->set('price-room', $priceRoom);
        }

    }

    public function actionAddPriceRoomFromSession(){
        if(Yii::$app->request->isAjax && $_POST['roomId'] && $_POST['hotelId']) {
            $session = Yii::$app->session;
            $priceRoom = $session->get('price-room');
            if ($priceRoom){
                $pricesRoomData = (new Query())->select('*')->from($priceRoom['lang'].'_price_room')->where(['room_id' => $priceRoom['room_id']])->all();
                $roomTypes = (new Query())->select('type_room')->from($priceRoom['lang'].'_price_room')->where(['room_id' => $priceRoom['room_id']])->groupBy('type_room')->column();

                $typesRoom = [];
                $bedTypesRoom = [];

                if(Yii::$app->language != $priceRoom['lang']){
                    foreach ($roomTypes as $roomType){
                        $roomTypeSlug = (new Query())->select('slug')->from($priceRoom['lang'].'_room_type')->where(['id' => $roomType])->one();
                        $roomTypeId = null;

                        if(isset($roomTypeSlug['slug']) && $roomTypeSlug['slug']){
                            $roomTypeId = RoomType::find()->select('id')->where(['slug' => $roomTypeSlug['slug']])->column();
                        }

                        if($roomTypeId){
                            $typesRoom[$roomType] = $roomTypeId[0];
                        } else {
                            $bedTypesRoom[] = $roomType;
                        }
                    }
                }

                if($bedTypesRoom && count($bedTypesRoom) > 0){
                    $roomTypeTitle = (new Query())->select('title')->from($priceRoom['lang'].'_room_type')->where(['id' => $roomType])->column();
                    $result = ['status' => 'error', 'message' => 'Не найдены такие типы комнат - '.implode(', ', $roomTypeTitle)];
                    return \yii\helpers\Json::encode($result);
                }

                foreach ($pricesRoomData as $price){
                    unset($price['id']);
                    $price['room_id'] = $_POST['roomId'];
                    $price['hotel_id'] = $_POST['hotelId'];
                    if(Yii::$app->language != $priceRoom['lang']) {
                        $price['type_room'] = $typesRoom[$price['type_room']];
                    }
                    if(!empty($price['services']) && $price['services'] != '{}'){
                        $servicesId = substr($price['services'],1, -1 );
                        $price['services'] = explode(',', $servicesId);
                    }

                    $model = new PriceRoom();
                    if($model->load(['PriceRoom' => $price])){
                        $model->save();
                    }
                }
                $result = ['status' => 'ok'];
                return \yii\helpers\Json::encode($result);
            } else {
                die(pr(555, $priceRoom));
            }
        }
    }

    public function actionDeletePriceRoomFromSession(){
        $result = ['status' => 'error'];
        if(Yii::$app->request->isAjax && isset($_SESSION['price-room'])) {
            unset($_SESSION['price-room']);
            header("Refresh:0");
            $result['status'] = 'ok';
        }
        return \yii\helpers\Json::encode($result);
    }

    /**
     * Creates a new PriceRoom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $hotelId = null;
        $roomId = null;
        $model = new PriceRoom();
        if($model->load(Yii::$app->request->post())){
            $dataPriceRoom = Yii::$app->request->post();
            $collectionPriceRoom = [];
//            pr($dataPriceRoom['PriceRoom']);
            foreach ($dataPriceRoom['PriceRoom'] as $attribute => $priceRoom){
                foreach ($priceRoom as $key => $value){
                    if(!is_int($key)){
                        continue;
                    }
                    $collectionPriceRoom[$key][$attribute] = $value;
                }
            }
            $error = false;
            foreach ($collectionPriceRoom as $priceRoom){
                $model = new PriceRoom();
                if ($model->load(['PriceRoom' => $priceRoom])) {
                    if(!$hotelId){
                        $hotelId = Room::findOne($model->room_id)->id_hotel;
                    }
                    if(!$roomId){
                        $roomId = $priceRoom['room_id'];
                    }
                    $model->room_id = $roomId;
                    $model->hotel_id = $hotelId;
                    if (!$model->validate()) {
                        die(pr($model->getErrors()));
                    }
                }
            }

            if(!$error){
                foreach ($collectionPriceRoom as $priceRoom){
                    $model = new PriceRoom();
                    if ($model->load(['PriceRoom' => $priceRoom])) {
                        $model->hotel_id = $hotelId;
                        $model->room_id = $roomId;
                        $model->save();
                    }
                }

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => true];
            }
        }


        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model, 'roomId' => (int)$_POST['room']
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PriceRoom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->hotel_id = Room::findOne($model->room_id)->id_hotel;
            $model->save();
            echo 'ok';
            die();

        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model, 'roomId' => $model->room_id
            ]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing PriceRoom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            echo 'ok';
            die();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the PriceRoom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PriceRoom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PriceRoom::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
