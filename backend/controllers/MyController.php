<?php


namespace backend\controllers;

use common\models\User;
use \yii\web\Controller;
use \Yii;

class MyController extends Controller
{
    public $user;
    public function __construct($id, $module, $config = [])
    {
        $user = null;
        if (Yii::$app->user->isGuest) {
            header("Location: /login");
            die();
        }
        $this->user = User::findOne(Yii::$app->user->id);
        if($this->user->role != User::ROLE_ADMIN && $this->user->role != User::ROLE_SUPERADMIN){
            header("Location: /");
            die();
        }
        Yii::$app->language = isset($_GET['lang']) ? $_GET['lang'] : 'ru';
        parent::__construct($id, $module, $config);
//        var_dump(Yii::$app->language, $_GET['lang']);die();
    }
}