<?php
namespace backend\widgets;
class Seo extends \yii\bootstrap\Widget
{
    public $model;
    public $form;

    public function run()
    {
        return $this->render('seo', ['model' => $this->model, 'form' => $this->form]);
    }
}