<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/plugins/bootstrap/css/bootstrap.min.css',
        'assets/css/style.css',
        'assets/css/icons.css',
        'assets/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css',
        'assets/plugins/autocomplete/jquery.autocomplete.css',
        'assets/color-skins/color.css',
    ];
    public $js = [
        'assets/js/jquery-3.5.1.min.js',
        'assets/plugins/bootstrap/js/popper.min.js',
        'assets/plugins/bootstrap/js/bootstrap.bundle.js',
        'assets/js/jquery.sparkline.min.js',
        'assets/js/circle-progress.min.js',
        'assets/js/selectize.min.js',
        'assets/plugins/jquery-bar-rating/jquery.barrating.js',
        'assets/plugins/jquery-bar-rating/js/rating.js',
        'assets/plugins/owl-carousel/owl.carousel.js',
        'assets/js/owl-carousel.js',
        'assets/plugins/horizontal/horizontal-menu/horizontal.js',
        'assets/js/jquery.touchSwipe.min.js',
        'assets/plugins/select2/select2.full.min.js',
        'assets/js/select2.js',
        'assets/plugins/cookie/jquery.ihavecookies.js',
        'assets/plugins/cookie/cookie.js',
        'assets/plugins/autocomplete/jquery.autocomplete.js',
        'assets/plugins/autocomplete/autocomplete.js',
        'assets/js/sticky.js',
        'assets/js/swipe.js',
        'assets/js/scripts.js',
        'assets/js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
