<?php
namespace frontend\controllers;

use backend\controllers\MyController;
use common\models\Category;
use common\models\Hotel;
use common\models\PriceRoom;
use common\models\Region;
use common\models\Room;
use common\models\Tour;
use common\models\TourTable;
use common\models\User;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class MainController extends FrontController
{
    public $layout = 'main-page';
    function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $hotelsTop = Hotel::find()
            ->join('JOIN ', Room::tableName(), Room::tableName().'.id_hotel='.Hotel::tableName().'.id')
            ->join('JOIN ', PriceRoom::tableName(), PriceRoom::tableName().'.room_id='.Room::tableName().'.id')
//            ->groupBy(Hotel::tableName().'.id')
            ->limit(8)->all();


        $topTors = TourTable::find()
            ->where('date > now()')
            ->orderBy('price')->limit(10)
            ->all();

        $topCategories = Category::find()
            ->select(Category::tableName().'.*, count('.Hotel::tableName().'.id) as count_hotel')
            ->join('JOIN', Hotel::tableName(), Category::tableName().'.id = any('.Hotel::tableName().'.category_id)')
            ->join('JOIN', PriceRoom::tableName(), Hotel::tableName().'.id = '.PriceRoom::tableName().'.hotel_id')
            ->groupBy(Category::tableName().'.id')
            ->orderBy('count_hotel')
            ->limit(3)
            ->all();


        $descriptionHotels = Hotel::find()->where("description != ''")->limit(3)->all();

        $regions = Region::find()
            ->select(Region::tableName().'.*, count('.Hotel::tableName().'.id) as count_hotel')
            ->join('JOIN', Hotel::tableName(), '('.Hotel::tableName().'.region_id = '.Region::tableName().'.id)')
            ->groupBy(Region::tableName().'.id')
            ->asArray()
            ->all();

        $regionsTour = Region::find()
            ->join('JOIN', Tour::tableName(), '('.Tour::tableName().'.region_id = '.Region::tableName().'.id)')
            ->all();

        return $this->render('index', [
            'hotelsTop' => $hotelsTop,
            'topTors' => $topTors,
            'topCategories' => $topCategories,
            'descriptionHotels' => $descriptionHotels,
            'regions' => $regions,
            'regionsTour' => $regionsTour
        ]);
    }

}
