<?php
namespace frontend\controllers;

use backend\controllers\MyController;
use common\models\Category;
use common\models\Comments;
use common\models\Hotel;
use common\models\News;
use common\models\PriceRoom;
use common\models\RatingStars;
use common\models\Region;
use common\models\Room;
use common\models\Tour;
use common\models\TourTable;
use common\models\User;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

/**
 * News controller
 */
class NewsController extends FrontController{

    private $limit = 10;

    public function actionList(){

        $query = News::find();

        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        $newsModelList = $query->orderBy(['date_create' => SORT_DESC, 'title' => SORT_ASC])
            ->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('list', [
            'newsList' => $newsModelList,
            'pages' => $pages
        ]);
    }

    public function actionIndex($url){

        $newsModel = News::find()->where(['url' => $url])->one();

        $ratingStars = RatingStars::find()
            ->select('count_stars, count(id) as count_reviews')
            ->where(['news_id' => $newsModel->id])
            ->groupBy('count_stars')
            ->orderBy(['count_stars' => SORT_DESC])
            ->asArray()
            ->all();

        $comments = Comments::find()
            ->where(['news_id' => $newsModel->id, 'parent' => 0, 'approved' => 1])
            ->orderBy(['date' => SORT_ASC])
            ->all();

        $countComments = Comments::find()
            ->where(['news_id' => $newsModel->id, 'approved' => 1])
            ->count();

        return $this->render('index', [
            'ratingStars' => $ratingStars,
            'countComments' => $countComments,
            'comments' => $comments,
            'newsModel' => $newsModel
        ]);
    }
}