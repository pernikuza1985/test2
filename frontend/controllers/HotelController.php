<?php
namespace frontend\controllers;

use backend\controllers\MyController;
use common\components\helper\FilterHelper;
use common\models\Category;
use common\models\Comments;
use common\models\Hotel;
use common\models\PriceRoom;
use common\models\RatingStars;
use common\models\Region;
use common\models\Room;
use common\models\RoomType;
use common\models\Tour;
use common\models\TourTable;
use common\models\User;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\debug\components\search\Filter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class HotelController extends FrontController
{
    public $layout = 'main-page';
    function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($sign)
    {
        $hotel = Hotel::find()
            ->where([Hotel::tableName().'.url' => $sign])
            ->one();

        if(!$hotel){
            throw new NotFoundHttpException();
        }

        $typeRoomsHotel =  RoomType::find()
            ->join('JOIN', Room::tableName(), Room::tableName().'.id_room_type='.RoomType::tableName().'.id')
            ->where([Room::tableName().'.id_hotel' => $hotel->id])
            ->all()
        ;

        $typeRoomsHotelCount =  RoomType::find()
            ->join('JOIN', Room::tableName(), Room::tableName().'.id_room_type='.RoomType::tableName().'.id')
            ->where([Room::tableName().'.id_hotel' => $hotel->id])
            ->count()
        ;

        if($hotel->region_id){
            $relatedHotel = Hotel::find()
                ->join('JOIN ', Room::tableName(), Room::tableName().'.id_hotel='.Hotel::tableName().'.id')
                ->join('JOIN ', PriceRoom::tableName(), PriceRoom::tableName().'.room_id='.Room::tableName().'.id')
                ->where(Hotel::tableName().'.region_id ='.$hotel->region_id. 'AND '.Hotel::tableName().'.id != '.$hotel->id)
                ->all();
        }


        $ratingStars = RatingStars::find()
            ->select('count_stars, count(id) as count_reviews')
            ->where(['hotel_id' => $hotel->id])
            ->groupBy('count_stars')
            ->orderBy(['count_stars' => SORT_DESC])
            ->asArray()
            ->all();

        $comments = Comments::find()
            ->where(['hotel_id' => $hotel->id, 'parent' => 0, 'approved' => 1])
            ->orderBy(['date' => SORT_ASC])
            ->all();

        $countComments = Comments::find()
            ->where(['hotel_id' => $hotel->id, 'approved' => 1])
            ->count();

        return $this->render('index', [
            'hotel' => $hotel,
            'typeRoomsHotel' => $typeRoomsHotel,
            'typeRoomsHotelCount' => $typeRoomsHotelCount,
            'relatedHotels' => $relatedHotel,
            'ratingStars' => $ratingStars,
            'comments' => $comments,
            'countComments' => $countComments
        ]);
    }

    public function actionList($filter = null){
        $limitHotelsOnPage = 10;
        $this->layout = 'layout';
        $filter = FilterHelper::getData($filter);

        $currentPage = isset($filter['page']) ? $filter['page'] : 1;
        $hotels = Hotel::find()
            ->joinWith(['priceRoom']);

        $cloneHotels = clone $hotels;
        $regionsIds = $cloneHotels->select(Hotel::tableName().'.region_id')->groupBy(Hotel::tableName().'.region_id')->column();

        $regions = Region::findAll($regionsIds);

        $filter['price_min'] = $hotels->min(priceRoom::tableName().'.price');
        $filter['price_max'] = $hotels->max(priceRoom::tableName().'.price');

        if(isset($filter['price_from'])){
            $hotels->andWhere(PriceRoom::tableName().'.price >= '. $filter['price_from']);
        }
        if(isset($filter['price_to'])){
            $hotels->andWhere(PriceRoom::tableName().'.price <= '. $filter['price_to']);
        }
        if(isset($filter['person'])){
            $hotels->andWhere(PriceRoom::tableName().'.count_people <= '. $filter['person']);
        }
        if(isset($filter['kids'])){
            $hotels->andWhere(PriceRoom::tableName().'.price <= '. $filter['kids']);
        }

        if(isset($filter['category'])){
            $hotels->andWhere($filter['category'].' = ANY(' .Hotel::tableName().'.category_id)');
        }

        if(isset($filter['country'])){
            if(is_array($filter['country'])){
                $hotels->andWhere(Hotel::tableName().".country_id IN(".implode(",", $filter['country']).")");
            } else {
                $hotels->andWhere(Hotel::tableName().".country_id = ".$filter['country']);
            }
        }

        if(isset($filter['type_room'])){
            $hotels->andWhere(['id_room_type' => $filter['type_room']]);
        }

        $cloneHotels = clone $hotels;
        $hotelsIds = $cloneHotels->select(Hotel::tableName().'.id')->asArray()->all();

        $hotelsIds = array_column($hotelsIds, 'id');

        $filter['type_room'] = RoomType::find()
            ->select(RoomType::tableName().'.*, count('.Room::tableName().'.id_hotel) as count_hotel')
            ->join('JOIN', Room::tableName(), Room::tableName().'.id_room_type='.RoomType::tableName().'.id')
            ->where([Room::tableName().'.id_hotel' => $hotelsIds])
            ->groupBy(RoomType::tableName().'.id')
            ->asArray()
            ->all();

        $hotelsList = $hotels->groupBy(Hotel::tableName().'.id')
            ->orderBy(['date_create' => SORT_ASC])
            ->limit($limitHotelsOnPage)
            ->offset(($currentPage - 1) * $limitHotelsOnPage)
            ->all();

        $countHotels = $hotels->count();

        $countPage = ceil($countHotels / $limitHotelsOnPage);

        $idsHotels = $hotels->select(Hotel::tableName().'.id')->asArray()->all();

        $dataPagination = [
            'countHotels' => $countHotels,
            'limit' => $limitHotelsOnPage,
            'currentPage' => $currentPage,
            'countPage'    => $countPage
        ];

        return $this->render('list',[
            'hotels' => $hotelsList,
            'idsHotels' => $idsHotels,
            'filter'   => $filter,
            'regions'  => $regions,
            'dataPagination' => $dataPagination
        ]);
    }
}
