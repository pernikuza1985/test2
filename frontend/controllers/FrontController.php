<?php


namespace frontend\controllers;

use common\models\User;
use \yii\web\Controller;
use \Yii;

class FrontController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        if(isset($_POST['currency']) && in_array($_POST['currency'], ['usd', 'uah', 'eur'])){
            $session = Yii::$app->session;
            $session->set('currency', $_POST['currency']);
        }
        parent::__construct($id, $module, $config);
    }
}