<?php
namespace frontend\controllers;

use backend\controllers\MyController;
use common\components\helper\FilterHelper;
use common\models\Category;
use common\models\Comments;
use common\models\Hotel;
use common\models\PriceRoom;
use common\models\RatingStars;
use common\models\Region;
use common\models\Room;
use common\models\RoomType;
use common\models\Tour;
use common\models\TourPlaces;
use common\models\TourTable;
use common\models\User;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\debug\components\search\Filter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class TourController extends FrontController
{
    public $layout = 'main-page';
    function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($sign)
    {
        $tour = Tour::find()
            ->where([Tour::tableName().'.url' => $sign])
            ->one();

        if(!$tour){
            throw new NotFoundHttpException();
        }

        if($tour->region_id){
            $relatedTour = Tour::find()
                ->where(Tour::tableName().'.region_id ='.$tour->region_id. 'AND '.Tour::tableName().'.id != '.$tour->id)
                ->all();

        }

        $ratingStars = RatingStars::find()
            ->select('count_stars, count(id) as count_reviews')
            ->where(['tour_id' => $tour->id])
            ->groupBy('count_stars')
            ->orderBy(['count_stars' => SORT_DESC])
            ->asArray()
            ->all();

        $comments = Comments::find()
            ->where(['tour_id' => $tour->id, 'parent' => 0, 'approved' => 1])
            ->orderBy(['date' => SORT_ASC])
            ->all();

        $countComments = Comments::find()
            ->where(['tour_id' => $tour->id, 'approved' => 1])
            ->count();

        $recentTours = Tour::find()
            ->orderBy([Tour::tableName().'.date_create' => SORT_DESC])
            ->limit(5)
            ->all();

        return $this->render('index', [
            'tour' => $tour,
            'recentTours' => $recentTours,
            'relatedHotels' => $relatedTour,
            'ratingStars' => $ratingStars,
            'comments' => $comments,
            'countComments' => $countComments
        ]);
    }

    public function actionList($filter = null){
        $limitHotelsOnPage = 10;
        $this->layout = 'layout';
        $filter = FilterHelper::getData($filter);

        $currentPage = $filter['page'] ? $filter['page'] : 1;
        $tours = Tour::find()
            ->join('JOIN', priceRoom::tableName(), priceRoom::tableName().'.hotel_id='.Hotel::tableName().'.id');

        $cloneHotels = clone $tours;
        $regionsIds = $cloneHotels->select(Hotel::tableName().'.region_id')->groupBy(Hotel::tableName().'.region_id')->asArray()->all();

        $regions = Region::findAll($regionsIds);

        $filter['price_min'] = $tours->min(priceRoom::tableName().'.price');
        $filter['price_max'] = $tours->max(priceRoom::tableName().'.price');

        if(isset($filter['price_from'])){
            $tours->andWhere(PriceRoom::tableName().'.price >= '. $filter['price_from']);
        }
        if(isset($filter['price_to'])){
            $tours->andWhere(PriceRoom::tableName().'.price <= '. $filter['price_to']);
        }
        if(isset($filter['person'])){
            $tours->andWhere(PriceRoom::tableName().'.count_people <= '. $filter['person']);
        }
        if(isset($filter['kids'])){
            $tours->andWhere(PriceRoom::tableName().'.price <= '. $filter['kids']);
        }

        if(isset($filter['category'])){
            $tours->andWhere($filter['category'].' = ANY(' .Hotel::tableName().'.category_id)');
        }

        if(isset($filter['country'])){
            if(is_array($filter['country'])){
                $tours->andWhere(Hotel::tableName().".country_id IN(".implode(",", $filter['country']).")");
            } else {
                $tours->andWhere(Hotel::tableName().".country_id = ".$filter['country']);
            }
        }

        if(isset($filter['type_room'])){
            $tours->andWhere(['id_room_type' => $filter['type_room']]);
        }

        $cloneHotels = clone $tours;
        $toursIds = $cloneHotels->select(Hotel::tableName().'.id')->asArray()->all();

        $toursIds = array_column($toursIds, 'id');

        $filter['type_room'] = RoomType::find()
            ->select(RoomType::tableName().'.*, count('.Room::tableName().'.id_hotel) as count_hotel')
            ->join('JOIN', Room::tableName(), Room::tableName().'.id_room_type='.RoomType::tableName().'.id')
            ->where([Room::tableName().'.id_hotel' => $toursIds])
            ->groupBy(RoomType::tableName().'.id')
            ->asArray()
            ->all();

        $toursList = $tours->groupBy(Hotel::tableName().'.id')
            ->limit($limitHotelsOnPage)
            ->offset(($currentPage - 1) * $limitHotelsOnPage)
            ->all();

        $countHotels = $tours->count();

        $countPage = ceil($countHotels / $limitHotelsOnPage);

        $idsHotels = $tours->select(Hotel::tableName().'.id')->asArray()->all();

        $dataPagination = [
            'countHotels' => $countHotels,
            'limit' => $limitHotelsOnPage,
            'currentPage' => $currentPage,
            'countPage'    => $countPage
        ];

        return $this->render('list',[
            'hotels' => $toursList,
            'idsHotels' => $idsHotels,
            'filter'   => $filter,
            'regions'  => $regions,
            'dataPagination' => $dataPagination
        ]);
    }
}
