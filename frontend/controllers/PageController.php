<?php
namespace frontend\controllers;

use backend\controllers\MyController;
use common\models\Category;
use common\models\Comments;
use common\models\Complaint;
use common\models\Hotel;
use common\models\PriceRoom;
use common\models\Rating;
use common\models\RatingStars;
use common\models\Region;
use common\models\Room;
use common\models\RoomType;
use common\models\Tour;
use common\models\TourTable;
use common\models\User;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class PageController extends FrontController
{
    function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionContact(){
        return $this->render('contact');
    }

    public function actionAbout(){
        return $this->render('about');
    }

    public function actionAddComplaint(){
        if (Yii::$app->request->isAjax) {
            $result = [];
            $model = new Complaint();
            if($model->load(Yii::$app->request->post()) && $model->save()){
                echo 'ok';
                die();
            }
            return $this->renderAjax('_form_complaint', [
                'modelComplaint' => $model
            ]);
        }
    }

    public function actionImage(){

    }

    public function actionAddRatingStars(){
        if (Yii::$app->request->isAjax) {
            $result = ['status' => 'ok'];
            $model = new RatingStars();
            if (Yii::$app->user->isGuest){
                $result = ['status' => 'error'];
                return \yii\helpers\Json::encode($result);
            }
            $model->load(Yii::$app->request->post());
            if($model->hotel_id){
                $rating = RatingStars::find()->where(['user_id' => Yii::$app->user->id, 'hotel_id' => $model->hotel_id])->one();
                if(!$rating){
                    $model->user_id = Yii::$app->user->id;
                    $model->save();
                }

            }
            return \yii\helpers\Json::encode($result);
        }
    }

    public function actionAddComment(){
        if (Yii::$app->request->isAjax) {
            $result = ['status' => 'error'];
            $model = new Comments();
            $model->load(Yii::$app->request->post());
            $hotelId = $model->hotel_id;
            if (!Yii::$app->user->isGuest){
                if($model->hotel_id){
                    $model->user_id = Yii::$app->user->id;
                    $user = User::findOne(Yii::$app->user->id);
                    $model->email = $user->email;
                    $model->name = $user->user_name;
                    if($model->save()){
                        $result = ['status' => 'ok'];
                        $model = new Comments();

                    }
                }
            } else {
                if($model->save()){
                    $result = ['status' => 'ok'];
                    $model = new Comments();

                }
            }
            $model->hotel_id = $hotelId;
            $result['form'] = $this->renderPartial('_form_comment', ['model' => $model]);
            return \yii\helpers\Json::encode($result);
        }
    }
}
