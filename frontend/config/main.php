<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'sourceLanguage' => 'ru',
    'modules' => [
        'languages' => [
            'class' => 'common\modules\languages\Module',
            //Языки используемые в приложении
            'languages' => [
                'English' => 'en',
                'Русский' => 'ru',
                'Українська' => 'ua',
            ],
            'default_language' => 'ru', //основной язык (по-умолчанию)
            'show_default' => false, //true - показывать в URL основной язык, false - нет
        ],
    ],
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'languages'
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'forceTranslation' => true,
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'request' => [
            'baseUrl' => '',
//            'class' => 'common\components\Request',
            'csrfParam' => '_csrf-site',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' =>
            [
//            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'class' => 'common\components\FrontUrlManager',
            'rules' => [
                'languages' => 'languages/default/index', //для модуля мультиязычности
                //далее создаем обычные правила
                '<lang:(|en|ua)>/' => 'main/index',
                '<lang:(|en|ua)>/tour/<sign:[\w\-]+>' => 'tour/index',
                '<lang:(|en|ua)>/tours/<filter:[\w\-=:\/_,]*>' => 'tour/list/',
                '<lang:(|en|ua)>/news/' => 'news/list/',
                'news/<url:[\w\-]+>' => 'news/index',
                'hotel/<sign:[\w\-]+>' => 'hotel/index',
                'hotels' => 'hotel/list/',
                'hotels/<filter:[\w\-=:\/_,]*>' => 'hotel/list/',
                '<action:(contact|about)>' => 'page/<action>',
                '<action:(login|log-out|language|register)>' => 'site/<action>',
                '<controller:\w+>/<action:[\w\-]+>/' => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
