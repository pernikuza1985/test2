<?php
$this->title = 'Add Post';
$this->params['breadcrumbs'][] = $this->title;
?>

<!--Add posts-section-->
<section class="sptb">
    <div class="container">
        <div class="row ">
            <div class="col-lg-8 col-md-12">
                <h5 class="font-weight-bold">Select Category</h5>
                <div class="row gutters-sm mb-4">
                    <div class="col-6 col-sm-2 text-center">
                        <div class="card">
                            <label class="imagecheck mb-0 relative ieh-100">
                                <input name="imagecheck" type="radio" value="1" id="tour" class="imagecheck-input" checked="">
                                <span class="imagecheck-figure">
									<img src="../../assets/images/categories/category/6.png" alt="}" class="imagecheck-image cover-image br-bl-0 br-br-0 ieh-100">
								</span>
                            </label>
                            <div class="font-weight-bold py-1 leading-normal">Tour</div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-2 text-center">
                        <div class="card">
                            <label class="imagecheck mb-0 ieh-100">
                                <input name="imagecheck" type="radio" value="2" id="hotel"  class="imagecheck-input">
                                <span class="imagecheck-figure">
									<img src="../../assets/images/products/products/h3.jpg" alt="}" class="imagecheck-image cover-image br-bl-0 br-br-0 ieh-100">
								</span>
                            </label>
                            <div class="font-weight-bold py-1 leading-normal">Hotel</div>
                        </div>
                    </div>
                </div>
                <div class="post-content card category-tour">
                    <div class="card-header">
                        <div class="card-title">Add Tour Details</div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label text-dark">Tour Title</label>
                            <input type="text" class="form-control" placeholder="Add Tour Title">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Tour From</label>
                            <input type="text" class="form-control" placeholder="Tour From Address">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Tour To</label>
                            <select class="form-control custom-select select2-show-search">
                                <option value="UM">United States of America</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="AD">Andorra</option>
                                <option value="AG">Antigua and Barbuda</option>
                                <option value="AU">Australia</option>
                                <option value="AM">Armenia</option>
                                <option value="AO">Angola</option>
                                <option value="AR">Argentina</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BB">Barbados</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BE">Belgium</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BH">Bahrain</option>
                                <option value="BJ">Benin</option>
                                <option value="BN">Brunei</option>
                                <option value="BO">Bolivia</option>
                                <option value="BT">Bhutan</option>
                                <option value="BY">Belarus</option>
                                <option value="CD">Congo</option>
                                <option value="CA">Canada</option>
                                <option value="CF">Central African Republic</option>
                                <option value="CI">Cote d'Ivoire</option>
                                <option value="CL">Chile</option>
                                <option value="CM">Cameroon</option>
                                <option value="CN">China</option>
                                <option value="CO">Colombia</option>
                                <option value="CU">Cuba</option>
                                <option value="CV">Cabo Verde</option>
                                <option value="CY">Cyprus</option>
                                <option value="DJ">Djibouti</option>
                                <option value="DK">Denmark</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="EC">Ecuador</option>
                                <option value="EE">Estonia</option>
                                <option value="ER">Eritrea</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FI">Finland</option>
                                <option value="FJ">Fiji</option>
                                <option value="FR">France</option>
                                <option value="GA">Gabon</option>
                                <option value="GD">Grenada</option>
                                <option value="GE">Georgia</option>
                                <option value="GH">Ghana</option>
                                <option value="GH">Ghana</option>
                                <option value="HN">Honduras</option>
                                <option value="HT">Haiti</option>
                                <option value="HU">Hungary</option>
                                <option value="ID">Indonesia</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IN">India</option>
                                <option value="IQ">Iraq</option>
                                <option value="IR">Iran</option>
                                <option value="IS">Iceland</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JO">Jordan</option>
                                <option value="JP">Japan</option>
                                <option value="KE">Kenya</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="KI">Kiribati</option>
                                <option value="KW">Kuwait</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="LA">Laos</option>
                                <option value="LB">Lebanons</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LR">Liberia</option>
                                <option value="LS">Lesotho</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="LV">Latvia</option>
                                <option value="LY">Libya</option>
                                <option value="MA">Morocco</option>
                                <option value="MC">Monaco</option>
                                <option value="MD">Moldova</option>
                                <option value="ME">Montenegro</option>
                                <option value="MG">Madagascar</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MK">Macedonia (FYROM)</option>
                                <option value="ML">Mali</option>
                                <option value="MM">Myanmar (formerly Burma)</option>
                                <option value="MN">Mongolia</option>
                                <option value="MR">Mauritania</option>
                                <option value="MT">Malta</option>
                                <option value="MV">Maldives</option>
                                <option value="MW">Malawi</option>
                                <option value="MX">Mexico</option>
                                <option value="MZ">Mozambique</option>
                                <option value="NA">Namibia</option>
                                <option value="NG">Nigeria</option>
                                <option value="NO">Norway</option>
                                <option value="NP">Nepal</option>
                                <option value="NR">Nauru</option>
                                <option value="NZ">New Zealand</option>
                                <option value="OM">Oman</option>
                                <option value="PA">Panama</option>
                                <option value="PF">Paraguay</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PH">Philippines</option>
                                <option value="PK">Pakistan</option>
                                <option value="PL">Poland</option>
                                <option value="QA">Qatar</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russia</option>
                                <option value="RW">Rwanda</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SC">Seychelles</option>
                                <option value="SD">Sudan</option>
                                <option value="SE">Sweden</option>
                                <option value="SG">Singapore</option>
                                <option value="TG">Togo</option>
                                <option value="TH">Thailand</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TL">Timor-Leste</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TN">Tunisia</option>
                                <option value="TO">Tonga</option>
                                <option value="TR">Turkey</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TW">Taiwan</option>
                                <option value="UA">Ukraine</option>
                                <option value="UG">Uganda</option>
                                <option value="UY">Uruguay</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VA">Vatican City (Holy See)</option>
                                <option value="VE">Venezuela</option>
                                <option value="VN">Vietnam</option>
                                <option value="VU">Vanuatu</option>
                                <option value="YE">Yemen</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Tour Visting Places</label>
                            <select class="form-control custom-select select2-show-search" Multiple>
                                <option value="0">Tour Place 01</option>
                                <option value="2">Tour Place 02</option>
                                <option value="3">Tour Place 03</option>
                                <option value="4">Tour Place 04</option>
                                <option value="5">Tour Place 05</option>
                                <option value="6">Tour Place 06</option>
                                <option value="7">Tour Place 07</option>
                                <option value="8">Tour Place 08</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Phone Number</label>
                            <input type="text" class="form-control" placeholder="Add Phone Number">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label text-dark">Tour Start Date</label>
                                    <input class="form-control fc-datepicker" placeholder="Tour Start Date" type="text">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label text-dark">Tour End Date</label>
                                    <input class="form-control fc-datepicker" placeholder="Tour End Date" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Description</label>
                            <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="text here.."></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Image Upload</label>
                            <input class="fancyuploader" type="file" name="files" accept=".jpg, .png, image/jpeg, image/png" multiple>
                        </div>
                        <div class="control-group form-group mt-5">
                            <label class="form-label text-dark">Tour Package</label>
                            <div class="d-md-flex ad-post-details">
                                <label class="custom-control custom-radio mb-2 mr-4">
                                    <input type="checkbox" class="custom-control-input" name="radios" value="option1" checked="">
                                    <span class="custom-control-label"><a class="text-muted"><b>$20 </b>/ Single Person</a></span>
                                </label>
                                <label class="custom-control custom-radio  mb-2 mr-4">
                                    <input type="checkbox" class="custom-control-input" name="radios" value="option2" >
                                    <span class="custom-control-label"><a class="text-muted"><b>$30</b> / Honeymoon</a></span>
                                </label>
                                <label class="custom-control custom-radio  mb-2 mr-4">
                                    <input type="checkbox" class="custom-control-input" name="radios" value="option3" >
                                    <span class="custom-control-label"><a class="text-muted"><b>$50</b> / Family</a></span>
                                </label>
                                <label class="custom-control custom-radio  mb-2">
                                    <input type="checkbox" class="custom-control-input" name="radios" value="option4" >
                                    <span class="custom-control-label"><a class="text-muted"><b>$100</b> / All Inclusive</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Offer Discount</label>
                            <select class="form-control custom-select select2">
                                <option value="0">10% Discount</option>
                                <option value="2">15% Discount</option>
                                <option value="3">20% Discount</option>
                                <option value="4">25% Discount</option>
                                <option value="5">30% Discount</option>
                                <option value="6">35% Discount</option>
                                <option value="7">40% Discount</option>
                                <option value="8">45% Discount</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <a href="#" class="btn btn-success">Submit Now</a>
                    </div>
                </div>
                <div class="post-content card category-hotel">
                    <div class="card-header">
                        <div class="card-title">Add Hotel Details</div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label text-dark">Hotel Title</label>
                            <input type="text" class="form-control" placeholder="Hotel Title">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Hotel Address</label>
                            <input type="text" class="form-control" placeholder="Hotel Address">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Phone Number</label>
                            <input type="text" class="form-control" placeholder="Add Phone Number">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Email Address</label>
                            <input type="text" class="form-control" placeholder="Add Email Address">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Description</label>
                            <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="text here.."></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Image Upload</label>
                            <input class="fancyuploader" type="file" name="files" accept=".jpg, .png, image/jpeg, image/png" multiple>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Offer Discount</label>
                            <select class="form-control custom-select select2">
                                <option value="0">10% Discount</option>
                                <option value="2">15% Discount</option>
                                <option value="3">20% Discount</option>
                                <option value="4">25% Discount</option>
                                <option value="5">30% Discount</option>
                                <option value="6">35% Discount</option>
                                <option value="7">40% Discount</option>
                                <option value="8">45% Discount</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <a href="#" class="btn btn-success">Submit Now</a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Personal Information</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group mb-0">
                                    <label class="form-label">Phone Number</label>
                                    <input type="number" class="form-control" placeholder="Number">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group mb-0">
                                    <label class="form-label">Address</label>
                                    <input type="text" class="form-control" placeholder="Address">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Social Media Information</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text w-7">
                                    <i class="fa fa-facebook tx-16 lh-0 op-6 text-center mx-auto"></i>
                                </div>
                            </div><!-- input-group-prepend -->
                            <input class="form-control" placeholder="Facebook URL" type="text">
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <div class="input-group-text w-7">
                                    <i class="fa fa-map-marker tx-16 lh-0 op-6 text-center mx-auto"></i>
                                </div>
                            </div>
                            <input class="form-control" placeholder="GoogleMap Location URL" type="text">
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Business Info</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">Company Established year</label>
                            <select name="user[year]" class="form-control select2">
                                <option value="">Year</option>
                                <option value="2020">2020</option>
                                <option value="2019">2019</option>
                                <option value="2018">2018</option>
                                <option value="2017">2017</option>
                                <option value="2016">2016</option>
                                <option value="2015">2015</option>
                                <option value="2014">2014</option>
                                <option value="2013">2013</option>
                                <option value="2012">2012</option>
                                <option value="2011">2011</option>
                                <option value="2010">2010</option>
                                <option value="2009">2009</option>
                                <option value="2008">2008</option>
                                <option value="2007">2007</option>
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option selected="selected" value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>
                                <option value="1974">1974</option>
                                <option value="1973">1973</option>
                                <option value="1972">1972</option>
                                <option value="1971">1971</option>
                                <option value="1970">1970</option>
                                <option value="1969">1969</option>
                                <option value="1968">1968</option>
                                <option value="1967">1967</option>
                                <option value="1966">1966</option>
                                <option value="1965">1965</option>
                                <option value="1964">1964</option>
                                <option value="1963">1963</option>
                                <option value="1962">1962</option>
                                <option value="1961">1961</option>
                                <option value="1960">1960</option>
                                <option value="1959">1959</option>
                                <option value="1958">1958</option>
                                <option value="1957">1957</option>
                                <option value="1956">1956</option>
                                <option value="1955">1955</option>
                                <option value="1954">1954</option>
                                <option value="1953">1953</option>
                                <option value="1952">1952</option>
                                <option value="1951">1951</option>
                                <option value="1950">1950</option>
                                <option value="1949">1949</option>
                                <option value="1948">1948</option>
                                <option value="1947">1947</option>
                                <option value="1946">1946</option>
                                <option value="1945">1945</option>
                                <option value="1944">1944</option>
                                <option value="1943">1943</option>
                                <option value="1942">1942</option>
                                <option value="1941">1941</option>
                                <option value="1940">1940</option>
                                <option value="1939">1939</option>
                                <option value="1938">1938</option>
                                <option value="1937">1937</option>
                                <option value="1936">1936</option>
                                <option value="1935">1935</option>
                                <option value="1934">1934</option>
                                <option value="1933">1933</option>
                                <option value="1932">1932</option>
                                <option value="1931">1931</option>
                                <option value="1930">1930</option>
                                <option value="1929">1929</option>
                                <option value="1928">1928</option>
                                <option value="1927">1927</option>
                                <option value="1926">1926</option>
                                <option value="1925">1925</option>
                                <option value="1924">1924</option>
                                <option value="1923">1923</option>
                                <option value="1922">1922</option>
                                <option value="1921">1921</option>
                                <option value="1920">1920</option>
                                <option value="1919">1919</option>
                                <option value="1918">1918</option>
                                <option value="1917">1917</option>
                                <option value="1916">1916</option>
                                <option value="1915">1915</option>
                                <option value="1914">1914</option>
                                <option value="1913">1913</option>
                                <option value="1912">1912</option>
                                <option value="1911">1911</option>
                                <option value="1910">1910</option>
                                <option value="1909">1909</option>
                                <option value="1908">1908</option>
                                <option value="1907">1907</option>
                                <option value="1906">1906</option>
                                <option value="1905">1905</option>
                                <option value="1904">1904</option>
                                <option value="1903">1903</option>
                                <option value="1902">1902</option>
                                <option value="1901">1901</option>
                                <option value="1900">1900</option>
                                <option value="1899">1899</option>
                                <option value="1898">1898</option>
                                <option value="1897">1897</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Services</label>
                            <select class="form-control select2" data-placeholder="Choose Services" multiple>
                                <option>Select Category</option>
                                <option value="0">Hotels</option>
                                <option value="7" selected>Tours & Travels</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Payment Methods</label>
                            <select class="form-control select2" data-placeholder="Choose Payment" multiple>
                                <option value="Cash" Selected>Cash</option>
                                <option value="Net Banking">Net Banking</option>
                                <option value="Visa">Visa</option>
                                <option value="Master Card">Master Card</option>
                                <option value="Discover">Discover</option>
                                <option value="Americal Express">Americal Express</option>
                                <option value="Online Transaction">Online Transaction</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Fax</label>
                            <input type="number" class="form-control" placeholder="Fax Number">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Toll Free</label>
                            <input type="number" class="form-control" placeholder="Toll Free Number">
                        </div>
                        <div class="form-group">
                            <label class="form-label text-dark">Certification</label>
                            <select class="form-control select2" data-placeholder="Choose Certification" multiple>
                                <option value="Iso Certified" Selected>Iso Certified</option>
                                <option value="Government">Government</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="float-right mb-5 mb-lg-0">
                    <a href="#" class="btn btn-lg btn-success">Submit Now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Terms And Conditions</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-unstyled widget-spec mb-0">
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Money Not Refundable
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>You can renew your Premium ad after experted.
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings are active for depend on package.
                            </li>
                            <li class="ml-5 mb-0">
                                <a href="tips.html"> View more..</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Benefits Of Premium Ad</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-unstyled widget-spec mb-0">
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings Active
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings are displayed on top
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings will be Show in Google results
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-0 overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Safety Tips</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i> Meet Seller at public Place
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i> Check item before you buy
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i> Pay only after collecting item
                            </li>
                            <li class="ml-5 mb-0">
                                <a href="tips.html"> View more..</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Add posts-section-->


