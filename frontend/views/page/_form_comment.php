<?php
$form = \yii\widgets\ActiveForm::begin([
    'class' => 'form_comment'
]);
?>
    <?=$form->field($model, 'hotel_id')->hiddenInput()->label(false); ?>

    <?=$form->field($model, 'news_id')->hiddenInput()->label(false); ?>

    <?=$form->field($model, 'tour_id')->hiddenInput()->label(false); ?>

    <?=$form->field($model, 'parent')->hiddenInput(['value' => 0, 'class' => 'reply_parent'])->label(false); ?>

    <?php if(Yii::$app->user->isGuest): ?>
        <?=$form->field($model, 'name')->textInput(['placeholder' => Yii::t('app','Ваше имя'), 'class' => 'form-control'])->label(false); ?>

        <?=$form->field($model, 'email')->textInput(['placeholder' => Yii::t('app','Email'), 'class' => 'form-control', 'type' => 'email'])->label(false); ?>
    <?php endif; ?>

    <?=$form->field($model, 'message')->textarea(['placeholder' => Yii::t('app','Написать отзыв'), 'class' => 'form-control', 'rows' => 6])->label(false); ?>

<?php \yii\widgets\ActiveForm::end(); ?>