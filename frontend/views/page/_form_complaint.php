<?php
use \yii\widgets\ActiveForm;

?>
<?php
$form = ActiveForm::begin([
        'id' => 'form-complaint'
    ])?>
    <div class="modal-body">
        <?=$form->field($modelComplaint, 'url')->textInput(['id' => 'report-name', 'class' => 'form-control', 'placeholder' => 'Enter url'])->label(false);?>

        <?=$form->field($modelComplaint, 'type')->dropDownList(Yii::$app->params['type-complaint'], ['class' => 'form-control custom-select select2-no-serach', 'id' => 'select-countries2'])->label(false);?>

        <?=$form->field($modelComplaint, 'email')->textInput(['id' => 'report-email', 'class' => 'form-control', 'placeholder' => 'Email Address'])->label(false);?>

        <?=$form->field($modelComplaint, 'message')->textarea(['rows' => 6, 'class' => 'form-control', 'placeholder' => 'Message'])->label(false);?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success" id="add-complaint">Submit</button>
    </div>
<?php ActiveForm::end(); ?>
