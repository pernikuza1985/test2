<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\Url;

$this->title = Yii::t('app', 'Зайти');
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="site-login">-->
<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
<!---->
<!--    <p>Please fill out the following fields to login:</p>-->
<!---->
<!--    <div class="row">-->
<!--        <div class="col-lg-5">-->
<!--            --><?php //$form = ActiveForm::begin(['id' => 'login-form']); ?>
<!---->
<!--                --><?//= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($model, 'password')->passwordInput() ?>
<!---->
<!--                --><?//= $form->field($model, 'rememberMe')->checkbox() ?>
<!---->
<!--                <div style="color:#999;margin:1em 0">-->
<!--                    If you forgot your password you can --><?//= Html::a('reset it', ['site/request-password-reset']) ?><!--.-->
<!--                    <br>-->
<!--                    Need new verification email? --><?//= Html::a('Resend', ['site/resend-verification-email']) ?>
<!--                </div>-->
<!---->
<!--                <div class="form-group">-->
<!--                    --><?//= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
<!--                </div>-->
<!---->
<!--            --><?php //ActiveForm::end(); ?>
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<section class="sptb">
    <div class="container customerpage">
        <div class="row">
            <div class="single-page" >
                <div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
                    <div class="wrapper wrapper2 text-left">
                        <?php $form = ActiveForm::begin([
                            'id' => 'login',
                            'options' => [
                                'class' => 'card-body',
                                'tabindex' => 500
                            ]
                        ]);?>
                            <h4 class="font-weight-semibold2 pb-4"><?= Yii::t('app', 'Зайти в аккаунт')?></h4>
                            <div class="mail">
                                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>
                                <label>Mail or Username</label>
                            </div>
                            <div class="passwd">
                                <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
                                <label><?= Yii::t('app', 'Пароль')?></label>
                            </div>
                            <div class="submit">
                                <input type="submit" class="btn btn-primary btn-block" value="<?= Yii::t('app', 'Зайти')?>">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="text-dark mb-0 fs-12"><?= Yii::t('app', 'Нет аккаунта?')?><a href="<?=Url::to(['/register']);?>" class="text-primary ml-1"><?= Yii::t('app', 'Регистрация')?></a></p>
                                </div>
                                <div class="col col-auto">
                                    <p class="mb-0 mt-0 fs-12"><a href="<?=Url::to(['/forgot']);?>" ><?= Yii::t('app', 'Забыл пароль')?></a></p>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
<!--                        </form>-->
                        <hr class="divider">
                        <div class="card-body text-center">
                            <ul class="mb-0 login-social-icons">
                                <li class="btn-facebook">
                                    <a class="social-icon" href=""><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="btn-google">
                                    <a class="social-icon" href=""><i class="fa fa-google-plus"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
