<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Error';
$this->params['breadcrumbs'] = [];
?>
<!-- Page -->
<div class="page page-h" style="padding: 50px">
    <div class="page-content zindex-10" style="min-height: inherit">
        <div class="container text-center">
            <div class="display-1 mb-5 font-weight-semibold2">404</div>
            <h1 class="h1 mb-3 font-weight-semibold2">Page Not Found</h1>
            <p class="h4 font-weight-normal mb-7 leading-normal">Oops!!!! you tried to access a page which is not available. go back to Home</p>
            <a class="btn btn-secondary px-6" href="/">
                <i class="fe fe-arrow-left mr-2"></i> Back To Home
            </a>
        </div>
    </div>
</div>
<!-- End Page -->

<!--Back to top -->
<a href="#top" id="back-to-top" ><i class="fe fe-arrow-up"></i></a>
