<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="site-signup">-->
<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
<!---->
<!--    <p>Please fill out the following fields to signup:</p>-->
<!---->
<!--    <div class="row">-->
<!--        <div class="col-lg-5">-->
<!--            --><?php //$form = ActiveForm::begin(['id' => 'form-signup']); ?>
<!---->
<!--                --><?//= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
<!---->
<!--                --><?//= $form->field($model, 'email') ?>
<!---->
<!--                --><?//= $form->field($model, 'password')->passwordInput() ?>
<!---->
<!--                <div class="form-group">-->
<!--                    --><?//= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
<!--                </div>-->
<!---->
<!--            --><?php //ActiveForm::end(); ?>
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


<section class="sptb">
    <div class="container customerpage">
        <div class="row">
            <div class="single-page" >
                <div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
                    <div class="wrapper wrapper2 text-left">
                        <?php $form = ActiveForm::begin([
                            'id' => 'login',
                            'options' => [
                                'class' => 'card-body',
                                'tabindex' => 500
                            ]
                        ]);?>
                            <h4 class="font-weight-semibold2 pb-4"><?= Yii::t('app', 'Зарегистрироваться');?></h4>
                            <div class="name">
                                <input type="text" name="name">
                                <label><?= Yii::t('app', 'Имя');?></label>
                            </div>
                            <div class="mail">
                                <input type="email" name="mail">
                                <label>Mail or Username</label>
                            </div>
                            <div class="passwd">
                                <input type="password" name="password">
                                <label><?= Yii::t('app', 'Пароль');?></label>
                            </div>
                            <div class="submit">
                                <a class="btn btn-primary btn-block" href="index.html"><?= Yii::t('app', 'Зарегистрироваться');?></a>
                            </div>
                            <p class="text-dark mb-0 fs-13"><?= Yii::t('app', 'Уже есть аккаунт?');?><a href="<?=\yii\helpers\Url::to('/login')?>" class="text-primary ml-1"><?= Yii::t('app', 'Войти');?></a></p>
                        <?php  ActiveForm::end()?>
                        <hr class="divider">
                        <div class="card-body text-center">
                            <ul class="mb-0 login-social-icons">
                                <li class="btn-facebook">
                                    <a class="social-icon" href=""><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="btn-google">
                                    <a class="social-icon" href=""><i class="fa fa-google-plus"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>