<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \common\models\Category;
use \common\models\Hotel;
use \common\models\Tour;
use \yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <script src="/assets/js/jquery-3.5.1.min.js"></script>
</head>
<body>
<?php $this->beginBody() ?>
<!--Loader-->
<div id="global-loader" class="bg-primary">
    <div class="loader-svg-img">
        <img src="/assets/images/brand/2.png" class="" alt="">
        <div class="flight-icon"><i class="fa fa-plane"></i></div>
    </div>
</div>
<!--Horizontal Section-->
<div class="header-main header-style03">
    <div class="top-bar app-header-background app-header-background2">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-sm-4 col-7">
                    <div class="top-bar-left d-flex">
                        <div class="clearfix">
                            <ul class="socials">
                                <li>
                                    <a class="social-icon text-white" href="#"><i class="fa fa-facebook text-white"></i></a>
                                </li>
                                <li>
                                    <a class="social-icon text-white" href="#"><i class="fa fa-linkedin text-white"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix">
                            <ul class="contact border-right">
                                <li class="mr-5 d-lg-none">
                                    <a href="tel:+4253458765" class="callnumber text-white"><span><i class="fa fa-phone mr-1"></i>: +425 345 8765</span></a>
                                </li>
<!--                                <li class="dropdown mr-5">-->
<!--                                    <a href="#" class="text-white" data-toggle="dropdown"><span> Language <i class="fa fa-caret-down text-white"></i></span> </a>-->
<!--                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">-->
<!--                                        <a href="#" class="dropdown-item" >-->
<!--                                            English-->
<!--                                        </a>-->
<!--                                        <a href="#" class="dropdown-item" >-->
<!--                                            Russian-->
<!--                                        </a>-->
<!--                                        <a href="#" class="dropdown-item" >-->
<!--                                            Ukrainian-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-8 col-5">
                    <div class="top-bar-right">
                        <ul class="custom">
                            <li>
                                <a href="register.html" class="text-white"><i class="fa fa-user mr-1 text-white"></i> <span><?=Yii::t('app', 'Регистрация');?></span></a>
                            </li>
                            <li>
                                <a href="/login" class="text-white"><i class="fa fa-sign-in mr-1 text-white"></i> <span><?=Yii::t('app', 'Войти');?></span></a>
                            </li>
                            <?php if(!Yii::$app->user->isGuest):?>
                                <li class="dropdown">
                                    <a href="#" class="text-white" data-toggle="dropdown"><i class="fa fa-home mr-1 text-white"></i><span>Панель</span><i class="fa fa-angle-down ml-1 header-dropdown-icon"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a href="mydash.html" class="dropdown-item">
                                            <i class="dropdown-icon icon icon-user"></i> Настройки
                                        </a>
    <!--                                    <a class="dropdown-item" href="#">-->
    <!--                                        <i class="dropdown-icon icon icon-speech"></i> Inbox-->
    <!--                                    </a>-->
                                        <a class="dropdown-item" href="#">
                                            <i class="dropdown-icon icon icon-power"></i> Выйти
                                        </a>
                                    </div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mobile Header -->
    <div class="sticky">
        <div class="horizontal-header clearfix ">
            <div class="container">
                <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
                <span class="smllogo"><a href="index.html"><img src="../../assets/images/brand/logo.png" alt=""></a></span>
                <a href="#" class="callusbtn"><i class="fe fe-plus-circle" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <!-- /Mobile Header -->

    <div class="horizontal-main  bg-dark-transparent clearfix py-lg-3">
        <div class="horizontal-mainwrapper container clearfix">
            <div class="desktoplogo">
                <a href="/"><img src="../../assets/images/brand/logo.png" alt=""></a>
            </div>
            <div class="desktoplogo-1">
                <a href="/"><img src="../../assets/images/brand/logo.png" alt=""></a>
            </div>
            <?php echo $this->render('_menu'); ?>
        </div>
        <div class="body-progress-container">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" id="myBar"></div>
        </div>
    </div>
</div>
<!--/Horizontal Section-->
<!--content-Section-->
<?= $content ?>
<!--/content-Section-->
<?php echo $this->render('footer');?>

<!-- Back to top -->
<a href="#top" id="back-to-top" ><i class="fe fe-arrow-up"></i></a>

<!-- JQuery js-->
<?php $this->endBody() ?>

<!-- Internal :::   Datepicker js -->
<script src="/assets/plugins/date-picker/jquery-ui.js"></script>

<!-- Internal :::   Timepicker js -->
<script src="/assets/plugins/jquery-timepicker/jquery.timepicker.js"></script>

<!-- Internal :::    wickedpicker js-->
<script src="/assets/plugins/wildtime/wickedpicker.min.js"></script>
<script src="/assets/js/timepicker.js"></script>

<!-- Internal :::   Datepicker js -->
<script src="/assets/js/date-picker.js"></script>

</body>
</html>
<?php $this->endPage() ?>