<?php
use \yii\widgets\ActiveForm;
?>
<?php ActiveForm::begin(['id' => 'form-currency'])?>
    <input type="hidden" name="currency" id="currency" >
<?php ActiveForm::end();?>
<a href="#" class="text-white" data-toggle="dropdown"><span>Currency <i class="fa fa-caret-down text-white"></i></span></a>
<?php $currentCurrency = isset(Yii::$app->session['currency']) ? Yii::$app->session['currency'] : 'uah'?>
<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
    <a class="dropdown-item <?=$currentCurrency != 'eur' ? 'change-currency': '' ?>" data-currency="eur" href="#">
        EUR
    </a>
    <a href="#" class="dropdown-item <?=$currentCurrency != 'usd' ? 'change-currency': '' ?>" data-currency="usd" >
        USD
    </a>
    <a href="#" class="dropdown-item <?=$currentCurrency != 'uah' ? 'change-currency': '' ?>" data-currency="uah">
        UAH
    </a>
</div>
<?php
$this->registerJs(
    "$('.change-currency').on('click', function () {        
        $('#currency').val($(this).data('currency'));
        $('#form-currency').submit();
    })",
    \yii\web\View::POS_END,
    'form-currency'
);
?>
<script>


</script>