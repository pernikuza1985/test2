<!--Section-->
<section class="sptb cover-image patter-image patter-image bg-background-pattern" data-image-src="/assets/images/pattern/9.png">
    <div class="content-text mb-0">
        <div class="container">
            <div class="text-center">
                <span class="heading-style text-secondary">Subscribe</span>
                <h2 class="mb-2 font-weight-bold"><?=Yii::t('app', 'Наша рассылка')?></h2>
                <p class="fs-16 mb-0"><?=Yii::t('app', 'Подписывайтесь и оставайтесь всегда в курсе горячих туров')?></p>
                <div class="row">
                    <div class="col-lg-8 mx-auto d-block">
                        <div class="mt-5">
                            <div class="input-group sub-input mt-1">
                                <input type="text" class="form-control input-lg " placeholder="<?=Yii::t('app', 'Ваш Email')?>">
                                <div class="input-group-append ">
                                    <button type="button" class="btn btn-secondary btn-lg br-tr-5 br-br-5 pl-5 pr-5">
                                        <?=Yii::t('app', 'Подписаться')?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Section-->

<!--Footer Section-->
<section>
    <footer class="bg-dark-purple text-white">
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-12">
                        <h6>О проекте</h6>
                        <p class="text-white-50">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium , totam rem aperiam, eaque ipsa quae ab inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo </p>
                    </div>
                    <div class="col-xl-2 col-lg-4 col-md-12">
                        <h6>Полезная информация</h6>
                        <ul class="list-unstyled mb-0">
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Статьи</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Оздоровительные туры</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Санатории</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Как пользоваться?</a></li>
                        </ul>
                    </div>
                    <div class="col-xl-2 col-lg-4 col-md-12">
                        <h6>Наш сервис</h6>
                        <ul class="list-unstyled mb-0">
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> О нашей команде</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Контакты</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Партнерам</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Соглашение</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right mr-2 text-secondary"></i> Конфиденциальность</a></li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-12">
                        <h6 class="mt-6 mt-xl-0">Популярное</h6>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a href="#" class="btn footer-btn-outline btn-sm btn-pill mb-1">Кардиология</a>
                                <a href="#" class="btn footer-btn-outline btn-sm btn-pill mb-1">Болезни почек</a>
                                <a href="#" class="btn footer-btn-outline btn-sm btn-pill mb-1">Карпаты</a>
                                <a href="#" class="btn footer-btn-outline btn-sm btn-pill mb-1">Для детей</a>
                                <a href="#" class="btn footer-btn-outline btn-sm btn-pill mb-1">Миниральные воды</a>
                                <a href="#" class="btn footer-btn-outline btn-sm btn-pill mb-1">Опорно-двигательный аппарат</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-2 col-lg-4 col-md-12">
                        <h6 class="mt-6 mt-xl-0">Адрес</h6>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a href="#"><i class="fa fa-home mr-3 text-secondary"></i>Киев, 02068</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-home mr-3 text-secondary"></i>ул. О.Пчилки, 5, 270</a>
                            </li>
                            <li>
                                <a href="mailto:partner@info.com"><i class="fa fa-envelope mr-3 text-secondary fs-12"></i>partner@info.com</a></li>
                            <li>
                                <a href="tel:+0123456788"><i class="fa fa-phone mr-3 text-secondary"></i> + 01 234 567 88</a>
                            </li>
                            <li>
                                <a href="tel:+0123456789"><i class="fa fa-print mr-3 text-secondary"></i> + 01 234 567 89</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-dark-purple text-white-50 p-0">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-lg-12 col-sm-12  mt-3 mb-3 text-center ">
                        Copyright © 2021 <a href="#" class="text-secondary">LimedTravel</a>. All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>
<!--Footer Section-->
