<?php
use \common\models\Category;
use \common\models\Hotel;
use \common\models\Tour;
use \yii\helpers\Url;
?>

<!--Nav-->
<nav class="horizontalMenu clearfix d-md-flex">
    <ul class="horizontalMenu-list">
        <li><a href="<?=Url::to(['/'])?>"><?=Yii::t('app', 'Главная')?></a></li>
        <?php
        $categoriesTour = Category::find()
            ->join('JOIN', Tour::tableName(), Category::tableName().'.id = ANY('.Tour::tableName().'.category_id)')
            ->orderBy(Category::tableName().'.title')
            ->all();
        ?>
        <?php if ($categoriesTour):?>
            <li><a href="<?=Url::to(['/tours'])?>"><?=Yii::t('app', 'Туры')?> <span class="fa fa-caret-down m-0"></span></a>
                <ul class="sub-menu">
                    <li>
                        <?php foreach ($categoriesTour as $category): ?>
                            <li><a href="<?=Url::to(['tours/'.$category->url])?>"><?=$category->title; ?> <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                                <?php $toursCategory = $category->tours;?>
                                <?php if($toursCategory):?>
                                    <ul class="sub-menu">
                                        <?php foreach ($category->regionsTour as $regionTour):?>
                                            <li><a href="<?=Url::to(['tours/'.$category->url.'/'.$regionTour->url])?>"><?=$regionTour->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </li>
                </ul>
            </li>
        <?php endif; ?>
        <li><a href="<?=Url::to(['/hotels'])?>"><?=Yii::t('app', 'Санатории')?> <span class="fa fa-caret-down m-0"></span></a>
            <ul class="sub-menu">
                <?php
                $categories = Category::find()
                    ->join('JOIN', Hotel::tableName(), Category::tableName().'.id = ANY('.Hotel::tableName().'.category_id)')
                    ->orderBy(Category::tableName().'.title')
                    ->all();
                ?>
                <li>
                    <?php foreach ($categories as $category): ?>
                        <li><a href="<?=Url::to(['hotels/'.$category->url])?>"><?=$category->title; ?> <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                            <?php if($category->regionsHotel):?>
                                <ul class="sub-menu">
                                    <?php foreach ($category->regionsHotel as $regionsHotel):?>
                                        <li><a href="<?=Url::to(['hotels/'.$category->url.'/'.$regionsHotel->url])?>"><?=$regionsHotel->title; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </li>
            </ul>
        </li>
        <li><a href="<?=Url::to(['about/'])?>"><?=Yii::t('app', 'О нас');?> </a></li>
        <li><a href="<?=Url::to(['news/'])?>"><?=Yii::t('app', 'Новости');?> </a></li>
        <li><a href="<?=Url::to(['contact/'])?>"> <?=Yii::t('app', 'Контакты');?></a></li>
        <li>
            <span><a class="btn btn-secondary" href="add-post.html"><i class="fe fe-plus"></i> Partnership</a></span>
        </li>
    </ul>
</nav>
<!--Nav-->