<?php
use common\components\helper\PriceHelper;
use common\models\PriceRoom;
use \common\models\Hotel;
use \common\models\Category;
use \common\models\Region;
?>
<section>
    <div class="banner-1 cover-image sptb-22 sptb-tab bg-background2" data-image-src="../../assets/images/banners/banner1.jpg">
        <div id="particles-js" ></div>
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white mb-7 mt-7">
                    <h1 class="mb-1"><?=Yii::t('app', 'Спланируйте поездку с нами')?></h1>
                    <p><?=Yii::t('app', 'Огромный выбор санаториев и оздоровительных центров в Украине')?></p>
                </div>
            </div>
        </div><!-- /header-text -->
    </div>
</section>
<!--Section-->

<!--Section-->
<div class="categories2">
    <div class="container">
        <div class="card mb-0">
            <div class="card-body">
                <div class="bg-white form-list">
                    <div class="">
                        <div class="form row no-gutters">
                            <div class="form-group  col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg"  style="min-width: 275px;margin-left: 22px;border-left:1px solid #e6e5f0">
                                <?php $categories = Region::find()
                                    ->join('JOIN', Hotel::tableName(), Region::tableName().'.id = '.Hotel::tableName().'.region_id')
                                    ->select(Region::tableName().".url, ".Region::tableName().".title")
                                    ->orderBy('title')
                                    ->groupBy(Region::tableName().'.id')
                                    ->all();
                                ?>
                                <select class="form-control select2-show-search border-bottom-0 w-100" data-placeholder="Регионы" id="filter-region">
                                    <optgroup>
                                        <option value="">Регионы</option>
                                        <?php foreach ($categories as $category):?>
                                            <option value="<?=$category->url?>"><?=$category->title?></option>
                                        <?php endforeach;?>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group  col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg" style="min-width: 275px;">
                                <?php $categories = Category::find()
                                    ->join('JOIN', Hotel::tableName(), Category::tableName().'.id = ANY('.Hotel::tableName().'.category_id)')
                                    ->select(Category::tableName().'.url, '.Category::tableName().'.title')->orderBy('title')
                                    ->all();
                                ?>
                                <select class="form-control select2-show-search border-bottom-0 w-100" data-placeholder="Категории" id="filter-category">
                                    <optgroup>
                                        <option value="">Категории</option>
                                        <?php foreach ($categories as $category):?>
                                            <option value="<?=$category->url?>"><?=$category->title?></option>
                                        <?php endforeach;?>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group col-xl-4 col-lg-4  col-md-4 mb-0 location">
                                <div class="row no-gutters">
                                    <div>
                                        <input class="form-control fc-datepicker br-0 input-lg" placeholder="<?=Yii::t('app', 'Выбрать дату');?>" type="text">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-12 mb-0">
                                        <a class="btn btn-block btn-secondary fs-14 br-tl-0 br-bl-0 shadow-none btn-lg" id="search-filter"  href="#"><?=Yii::t('app', 'Поиск');?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Section популярные санатории-->
<section class="sptb">
    <div class="container">
        <div class="section-title center-block text-center">
            <span class="heading-style text-secondary"><?=Yii::t('app', 'Best of the best')?></span>
            <h1><?= Yii::t('app', 'Популярные санатории')?></h1>
            <p><?= Yii::t('app', 'Отдохни с пользой для своего здоровья')?></p>
        </div>
        <?php
        ?>
        <div class="owl-carousel owl-carousel-icons">
            <?php foreach ($hotelsTop as $hotel):?>
                <?php $priceRoom = $hotel->priceRoom[0]; ?>
                <div class="item">
                    <div class="card mb-0 item-card2-card">
                        <a href="tours.html" class="absolute-link2"></a>
                        <div class="item-card2-img">
                            <?php if(!empty($hotel->images->getValue())): ?>
                                <div id="image-slider1" class="carousel" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php foreach ($hotel->images->getValue() as $image):?>
                                            <div class="carousel-item active">
                                                <img src="<?=Yii::$app->params['uploadDir'].$image?>" alt="img" class="cover-image">
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                    <a class="carousel-control-prev left-0" href="#image-slider1" role="button" data-slide="prev">
                                        <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next right-0" href="#image-slider1" role="button" data-slide="next">
                                        <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                    <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                        <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 2</span>
                                    </div>
                                </div>
                            <?php elseif($hotel->image): ?>
                                <img src="<?=Yii::$app->params['uploadDir'].$hotel->image?>" alt="img" class="cover-image">
                            <?php endif; ?>
                        </div>
                        <div class="card-body">
                            <div class="item-card2">
                                <div class="item-card2-desc">
                                    <div class="item-card2-text">
                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-semibold2 mb-1"><?=$hotel->title?></h4></a>
                                        <div class="item-card2-rating mb-0 mt-1">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mt-4">
                                    <div class="">
                                        <?php if($priceRoom->count_day): ?>
                                            <p class="fs-14 mb-2 item-card2-desc"><i class="fe fe-calendar d-inline-block"></i> <?=$priceRoom->count_day?> Days,<?=$priceRoom->count_day - 1 ?> Nights</p>
                                        <?php endif; ?>
                                        <p class="mb-1 item-card2-desc"><i class="fe fe-map-pin d-inline-block"></i> <?=$hotel->country ? $hotel->country->title : '';?>
                                            <?=$hotel->city ? ' - '.$hotel->city->title : ($hotel->region ? ' - '.$hotel->region->title : '');?></p>
                                    </div>
                                    <div class="ml-auto mt-0 border-left pl-5 text-center">
                                        <h4 class="mb-2">Price</h4>
                                        <h2 class="mb-0 font-weight-bold"><?=PriceHelper::Currency($priceRoom->price); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="card-footer text-center" href="tours.html">
                            <span class="d-block font-weight-semibold2">Book Now</span>
                        </a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<!--Section-->

<!--Section-->
<?php if ($topTors): ?>
    <section class="cover-image bg-background-color" data-image-src="../../assets/images/banners/banner.png">
    <div class="container">
        <div class="row content-text">
            <div class="col-xl-3 m-0">
                <div class="pt-xl-9 pt-7 pr-0 pl-4 pl-xl-0">
                    <span class="heading-style text-white-80">Offers</span>
                    <div class="Special-title fs-40 text-white font-weight-bold leading-tight">Лучшие</div>
                    <a class="btn btn-lg btn-secondary px-7 mt-5" href="#" id="dyanamic">Смотреть еще</a>
                </div>
            </div>
            <div class="col-xl-9">
                <div class="py-6 px-0">
                    <div id="myCarousel12" class="owl-carousel owl-carousel-icons6">
                        <?php foreach ($topTors as $tourPrice): ?>
                            <div class="item">
                                <div class="card border-0">
                                    <a href="tours.html" class="text-center mb-xl-0 p-0 overflow-hidden border-0">
                                        <div class="bg-pink-transparent br-tl-5 br-tr-5"><img src="<?=$tourPrice->tour->image?>" alt="img" class=""></div>
                                    </a>
                                    <div class="servic-data card-body text-center">
                                        <h4 class="font-weight-bold text-default pb-4 mb-0"><?=$tourPrice->tour->title?></h4>
                                        <div>
                                            <a class="mb-1 mt-2" href="#"><i class="fe fe-calendar"></i> <?=common\components\helper\aDate::getFormatter(strtotime($tourPrice->date), 'short')?></a>
                                            <a class="mb-1 mt-2 border-left ml-2 pl-2" href="#"><i class="fe fe-clock"></i><?=$tourPrice->tour->trip?></a>
                                            <a class="mb-1 mt-2 border-left ml-2 pl-2 text-yellow" href="#"><i class="fe fe-star text-yellow"></i> <?=rand(40, 50) / 10;?></a>
                                        </div>
                                    </div>
                                    <div class="font-weight-semibold2 text-muted mb-0 price-badge"><span class="text-white"><?=PriceHelper::Currency($tourPrice->price)?></span><del class="text-white-80"><?=PriceHelper::Currency($tourPrice->price_old)?></del></div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!--Section-->

<!--Section category-->
<?php if($topCategories):?>
    <section class="sptb bg-white">
        <div class="container">
            <div class="section-title center-block text-center">
                <span class="heading-style text-secondary">Top</span>
                <h2>Оздоровление по направлениям</h2>
            </div>
            <div class="row">
                <?php foreach ($topCategories as $topCategory):?>
                    <div class="col-xl-4 col-lg-4">
                        <div class="card overflow-hidden">
                            <div class="item-card8-img  br-tr-4 br-tl-4 relative">
                                <img src="<?=\common\components\helper\ImageHelper::path($topCategory->image)?>" alt="img" class="cover-image">
                                <div class="details-absolute p-3 bg-dark-transparent6">
                                    <h4 class="mb-0 text-white font-weight-bold"><?=$topCategory->title?></h4>
                                </div>
                            </div>
                            <?php $hotelsCategory = Hotel::find()->where($topCategory->id.' = ANY('. Hotel::tableName().'.category_id)')->limit(3)->all(); ?>
                            <?php foreach ($hotelsCategory as $hotel):?>
                                <div class="card-body">
                                    <div class="item-det">
                                        <a href="tours.html" class="text-dark d-flex">
                                            <h4 class="mb-1 fs-16 font-weight-bold"><?=$hotel->title?></h4>
                                            <?php $priceRoom = PriceRoom::findOne(['hotel_id' => $hotel->id])?>
                                            <div class="ml-auto"><div class="fs-20 font-weight-bold"><?=PriceHelper::Currency($priceRoom->price)?> </div></div>
                                        </a>
                                        <p class="fs-12"><?=$hotel->description ?> <sup class="text-danger fs-14">*</sup></p>
                                        <small class="badge bg-light"><i class="fe fe-clock d-inline-block"></i> <?=$priceRoom->count_day ? $priceRoom->count_day.' '.Yii::t('app', 'дней').' '.($priceRoom->count_day - 1).' '.Yii::t('app', 'ночей') : ''; ?></small> <?php if($hotel->location): ?><small class="badge bg-light"><i class="fe fe-map-pin d-inline-block"></i> <?=$hotel->location?></small><?php endif;?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>
<?php endif; ?>

<!--/Section-->

<!--Section description hotels-->
<?php if($descriptionHotels): ?>
    <section class="sptb position-relative">
        <div class="container">
            <div class="section-title center-block text-center pb-5">
                <span class="heading-style text-secondary">Our</span>
                <h1 class="position-relative"><?=Yii::t('app', 'Клиенты говорят')?></h1>
                <p><?=Yii::t('app', 'Отзывы отдыхающих, рекомендации и пожелания');?></p>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel-2" class="owl-carousel testimonial-owl-carousel">
                        <?php foreach ($descriptionHotels as $descriptionHotel):?>
                            <div class="item text-center pt-0">
                                <div class="row">
                                    <div class="col-xl-8 col-md-12 d-block mx-auto">
                                        <div class="">
                                            <div class="owl-controls clickable mt-0 mb-4">
                                                <div class="owl-pagination">
                                                    <?php if(!empty($descriptionHotel->images->getValue())):?>
                                                        <?php foreach ($descriptionHotel->images->getValue() as $image):?>
                                                            <div class="owl-page active" style="background-image: url(<?=$image?>)">
                                                                <span class=""></span>
                                                            </div>
                                                        <?php endforeach;?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <p class="">
                                                <i class="fa fa-quote-left"></i>
                                                <?=$descriptionHotel->description; ?>
                                            </p>
                                            <h3 class="title mb-1 fs-18 font-weight-semibold2"><?=$descriptionHotel->title?></h3>
                                            <div class="star-ratings start-ratings-main clearfix">

                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<!--Section-->

<!--Section regions-->
<?php if($regions):?>
    <section class="sptb bg-white">
    <div class="content-text mb-0">
        <div class="container">
            <div class="section-title center-block text-center">
                <span class="heading-style text-secondary">Popular</span>
                <h1 class="position-relative"><?=Yii::t('app', 'Направление для отдыха')?></h1>
                <p><?=Yii::t('app', 'Популярные санатории и оздоровительные центры')?></p>
            </div>

            <div class="row">
                <div class="col-md-12  item-all-cat">
                    <div class="owl-carousel owl-carousel-icons4 category-type">
                        <?php foreach ($regions as $region):?>
                            <div class="item">
                                <div class="">
                                    <div class="">
                                        <div class="cat-item text-center">
                                            <a href="tour-list.html"></a>
                                            <div class="">
                                                <img src="<?=\common\components\helper\ImageHelper::path($region['image'])?>" class="rounded-circle  mx-auto border" alt="" style="width: 100px; height: 100px">
                                            </div>
                                            <div class="item-all-text pt-4">
                                                <h5 class="mb-1 text-body font-weight-semibold2"><?=$region['title']?></h5>
                                                <p class="mb-0 text-muted fs-12"><b><?=$region['count_hotel']?></b> <?=\common\components\helper\StringHelper::decliningString($region['count_hotel'])?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!--Section-->
<!--Section-->
<?php if($regionsTour):?>
    <section class="sptb">
    <div class="container">
        <div class="section-title center-block text-center">
            <span class="heading-style text-secondary">Famous</span>
            <h2><?=Yii::t('app', 'Известные направления')?></h2>
            <p><?=Yii::t('app', 'Самые известные направления для санаторно-курортного отдыха и для проведения оздоровительных туров')?></p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-carousel-icons3">
                    <?php foreach ($regionsTour as $regionTour): ?>
                        <div class="item">
                            <div class="card cover-image bg-background-color place-tour-card" data-image-src="../../assets/images/categories/09.png">
                                <div class="card-body p-lg-8 pb-9 pt-8 text-white content-text place-tour">
                                    <h2 class="mt-2"><?=$regionTour->title?></h2>
                                    <p class="fs-14 mt-4 leading-loose"><?=$regionTour->description?></p>
                                    <a class="btn btn-lg btn-secondary px-6" href="tours.html"><?=Yii::t('app', 'Подробней')?></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!--/Section-->

<!--Section-->
<section class="sptb cover-image bg-background-color bg-background-color-dark py-9" data-image-src="../../assets/images/banners/banner2.jpg">
    <div class="container">
        <div class="section-title center-block text-center content-text m-0 p-0">
            <span class="heading-style text-white-80">Where Would Like to Go</span>
            <h2 class="text-white"><?=Yii::t('app', 'Хотите что-то особенное? Пишите нам, и мы организуем Ваш оздоровительный отпуск')?></h2>
            <div class="mt-7 row">
                <div class="col-lg-7 d-block mx-auto">
                    <div class="input-group sub-input mt-1">
                        <input type="text" class="form-control input-lg " placeholder="<?=Yii::t('app', 'Укажите направление или свои пожелания')?>">
                        <div class="input-group-append ">
                            <button type="button" class="btn btn-secondary btn-lg br-tr-5 br-br-5 pl-5 pr-5">
                                <?=Yii::t('app', 'Отправить запрос')?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Section-->

<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="section-title center-block text-center">
            <span class="heading-style text-secondary">Features</span>
            <h2><?=Yii::t('app', 'Преимущества')?></h2>
            <p><?=Yii::t('app', 'Почему мы? Именно мы поможем Вам организовать незабываемый оздоровительный отдых')?></p>
        </div>
        <div class="item-all-cat center-block text-center">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="status-border mb-4 mb-lg-0 p-5 pt-5 card">
                        <div>
                            <div class="status-info text-center">
                                <div class="cat-img category-svg d-block bg-primary-transparent">
                                    <i class="fa fa-thumbs-o-up fs-30 text-primary icon-lineheight"></i>
                                </div>
                                <h4 class="mt-5 mb-2 fs-20 font-weight-semibold2"><?=Yii::t('app', 'Качественный сервис')?></h4>
                                <p class="text-muted mb-0"><?=Yii::t('app', 'Быстрая онлайн-бронь туров на сайте, прозрачная ценовая политика')?>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="status-border mb-4 mb-lg-0 p-5 pt-5 card">
                        <div>
                            <div class="status-info text-center">
                                <div class="cat-img category-svg d-block bg-success-transparent">
                                    <i class="fa fa-shield fs-30 text-success icon-lineheight"></i>
                                </div>
                                <h4 class="mt-5 mb-2 fs-20 font-weight-semibold2"><?=Yii::t('app', 'Индивидуальный подход')?></h4>
                                <p class="text-muted mb-0"><?=Yii::t('app', 'Актуальная информация проверенная информация о санаториях и оздоровительных центрах')?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="status-border p-5 pt-5 card">
                        <div>
                            <div class="status-info text-center">
                                <div class="cat-img category-svg d-block  bg-warning-transparent">
                                    <i class="fa fa-headphones fs-30 text-warning icon-lineheight"></i>
                                </div>
                                <h4 class="mt-5 mb-2 fs-20 font-weight-semibold2">24/7 <?=Yii::t('app', 'Поддержка')?></h4>
                                <p class="text-muted mb-0"><?=Yii::t('app', 'Поможем забронировать отель, расскажем о преимуществах оздоровительного медучреждения')?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Section-->
<?php
$this->registerJs('        
        $("#search-filter").on("click", function () {
            url = getUrlMainSearch();
            location = "'.\yii\helpers\Url::to('/hotels').'/"+url;
        });
        function getUrlMainSearch(){
            var url = "";
            var category = $("#filter-category").val();
            var region = $("#filter-region").val();
            var dateFrom = $("#filter-date-from").val();
            if(category){
                url += category+"/";
            }   
            if(region){
                url += region+"/";
            }   
            if(dateFrom){
                date = dateFrom.replace(/\s/g, "");
                date = dateFrom.replace(/\//g, ":");
                url += "date_from="+date+"/";
            }   
            
            return url;
        }        
        '
);
?>