<link href="/assets/plugins/jquery-uislider/jquery-ui.css" rel="stylesheet">
<div class="cover-image sptb  py-9 bg-background" data-image-src="/assets/images/banners/banner16.jpg">
    <div class="header-text1 mb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
                    <div class="text-center text-white ">
                        <h2 class="fs-40">
<!--                            <span class="font-weight-bold">96,758</span>-->
                            Hotels available
                        </h2>
                    </div>
                    <div class="search-background">
                        <form id="main-search">
                            <div class="form row no-gutters">
                                <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0">
                                    <input type="text" class="form-control input-lg location-input border-right br-tr-0 br-br-0" placeholder="Current Location">
                                    <span><img src="/assets/images/svgs/gps.svg" class="location-gps" alt="img"></span>
                                </div>
                                <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg">
                                    <select class="form-control select2-show-search border-bottom-0 w-100">
                                        <option value="">Destination Location</option>
                                        <?php foreach ($regions as $region):?>
                                            <option value="<?=$region->url?>"><?=$region->title?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-8 col-lg-8 col-md-12 mb-0 location">
                                    <div class="row no-gutters">
                                        <div class="form-group col-xl-3 col-lg-3 col-md-12 mb-0">
                                            <input class="form-control input-lg fc-datepicker br-0 border-right border-left" placeholder="Travel Date" type="text" id="filter-date-from"
                                                   value="<?=isset($filter['date_from']) ? $filter['date_from'] : '' ?>">
                                        </div>
                                        <div class="form-group col-xl-3 col-lg-3 col-md-12 mb-0">
                                            <input class="form-control input-lg fc-datepicker br-0 border-right border-left-0" placeholder="Return Date" type="text" id="filter-date-to"
                                                   value="<?=isset($filter['date_to']) ? $filter['date_to'] : '' ?>">
                                        </div>
                                        <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg border-right">
                                            <select class="form-control select2-show-search border-bottom-0 w-100" id="filter-person">
                                                <option>Persons</option>
                                                <option value="1" <?=isset($filter['person']) && $filter['person'] == 1 ? 'selected' : ''; ?>>1</option>
                                                <option value="2" <?=isset($filter['person']) && $filter['person'] == 2 ? 'selected' : ''; ?>>2</option>
                                                <option value="3" <?=isset($filter['person']) && $filter['person'] == 3 ? 'selected' : ''; ?>>3</option>
                                                <option value="4" <?=isset($filter['person']) && $filter['person'] == 4 ? 'selected' : ''; ?>>4</option>
                                                <option value="5" <?=isset($filter['person']) && $filter['person'] == 5 ? 'selected' : ''; ?>>5</option>
                                                <option value="6" <?=isset($filter['person']) && $filter['person'] == 6 ? 'selected' : ''; ?>>6</option>
                                                <option value="7" <?=isset($filter['person']) && $filter['person'] == 7 ? 'selected' : ''; ?>>7</option>
                                                <option value="8" <?=isset($filter['person']) && $filter['person'] == 8 ? 'selected' : ''; ?>>8</option>
                                                <option value="9" <?=isset($filter['person']) && $filter['person'] == 9 ? 'selected' : ''; ?>>9</option>
                                                <option value="10" <?=isset($filter['person']) && $filter['person'] == 10 ? 'selected' : ''; ?>>10</option>
                                                <option value="11" <?=isset($filter['person']) && $filter['person'] == 11 ? 'selected' : ''; ?>>11</option>
                                                <option value="12" <?=isset($filter['person']) && $filter['person'] == 12 ? 'selected' : ''; ?>>12</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg">
                                            <?php $filter['kids'] = isset($filter['kids']) ? $filter['kids'] : 0; ?>
                                            <select class="form-control select2-show-search border-bottom-0 w-100" id="filter-kids">
                                                <option>Kids</option>
                                                <option value="1" <?=isset($filter['kids']) && $filter['kids'] == 1 ? 'selected' : ''; ?>>1</option>
                                                <option value="2" <?=isset($filter['kids']) && $filter['kids'] == 2 ? 'selected' : ''; ?>>2</option>
                                                <option value="3" <?=isset($filter['kids']) && $filter['kids'] == 3 ? 'selected' : ''; ?>>3</option>
                                                <option value="4" <?=isset($filter['kids']) && $filter['kids'] == 4 ? 'selected' : ''; ?>>4</option>
                                                <option value="5" <?=isset($filter['kids']) && $filter['kids'] == 5 ? 'selected' : ''; ?>>5</option>
                                                <option value="6" <?=isset($filter['kids']) && $filter['kids'] == 6 ? 'selected' : ''; ?>>6</option>
                                                <option value="7" <?=isset($filter['kids']) && $filter['kids'] == 7 ? 'selected' : ''; ?>>7</option>
                                                <option value="8" <?=isset($filter['kids']) && $filter['kids'] == 8 ? 'selected' : ''; ?>>8</option>
                                                <option value="9" <?=isset($filter['kids']) && $filter['kids'] == 9 ? 'selected' : ''; ?>>9</option>
                                                <option value="10" <?=isset($filter['kids']) && $filter['kids'] == 10 ? 'selected' : ''; ?>>10</option>
                                                <option value="11" <?=isset($filter['kids']) && $filter['kids'] == 11 ? 'selected' : ''; ?>>11</option>
                                                <option value="12" <?=isset($filter['kids']) && $filter['kids'] == 12 ? 'selected' : ''; ?>>12</option>
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-2 col-md-12 mb-0">
                                            <a class="btn btn-block btn-secondary btn-lg fs-14 br-tl-0 br-bl-0 shadow-none" id="starch-filter" href="#">Search</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /header-text -->
</div>
<!--/Sliders Section-->

<!--Breadcrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Hotel List 02</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Hotel List 02</li>
            </ol>
        </div>
    </div>
</div>
<!--/Breadcrumb-->

<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!--Add lists-->
                <?php
                $dataList = $dataPagination;
                $dataList['hotels'] = $hotels;
                ?>
                <?php echo $this->render('_list', $dataList);?>
                <!--/Add lists-->
            </div>
            <?php echo $this->render('_filter', ['filters' => $filter]);?>
        </div>
    </div>
</section>
<!--/Add Listings-->
<?php
    $this->registerJs('        
        $("#starch-filter").on("click", function () {
            url = getUrlMainSearch();
            location = "'.\yii\helpers\Url::to('/hotels').'/"+url;
        });
        function getUrlMainSearch(){
            var url = "";
            var adults = $("#filter-person").val();
            var child = $("#filter-kids").val();
            var dateFrom = $("#filter-date-from").val();
            var dateTo = $("#filter-date-to").val();
            var region = $("#filter-region").val();
            if(adults){
                url += "person="+adults+"/";
            }   
            if(child){
                url += "kids="+child+"/";
            }   
            if(dateFrom){
                date = dateFrom.replace(/\s/g, "");
                date = dateFrom.replace(/\//g, ":");
                url += "date_from="+date+"/";
            }   
            if(dateTo){
                date = dateTo.replace(/\s/g, "");
                date = dateTo.replace(/\//g, ":");
                url += "date_to="+date+"/";
            }
            if(region){
                url += "region="+region+"/";
            }
            
            return url;
        }
        
        '

    );
?>

<a href="#top" id="back-to-top" ><i class="fe fe-arrow-up"></i></a>

<!--<script src="/assets/plugins/jquery-uislider/jquery-ui.js"></script>-->
<!---->
<!--<script src="/assets/js/price-range.js"></script>-->