<?php

use \yii\helpers\Url;
use \yii\widgets\ActiveForm;
use \common\models\Rating;
use \common\components\helper\ImageHelper;

?>
<!--Add listing-->
<div class="relative pt-10 pb-10 pattern2 bg-background-color bg-background cover-image pb-9" data-image-src="../../assets/images/banners/banner16.jpg">
    <div class="header-text1 mb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 d-block mx-auto">
                    <div class="text-center text-white ">
                        <h1 class="mb-2"><span class="font-weight-semibold"><?=$tour->title?></span></h1>
                        <div class="star-ratings start-ratings-main clearfix mx-auto mb-2 mt-3 d-flex banner-ratings">
                            <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                <select class="example-fontawesome" name="rating" autocomplete="off">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4" selected>4</option>
                                    <option value="5">5</option>
                                </select>
                            </div> <a class="text-white" href="#"><?=$countComments; ?> reviews</a>
                        </div>
                        <a class="btn btn-info mb-1 mt-1" href="#"><i class="fa fa-heart-o"></i> Add Wishlist</a>
                        <a class="btn btn-success mb-1 mt-1 reply-comment" href="#" data-id="0"><i class="fa fa-star"></i> Write Review</a>
                        <a href="#" class="btn btn-danger icons mb-1 mt-1" data-toggle="modal" data-target="#report"><i class="icon icon-exclamation mr-1"></i> Report Abuse</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /header-text -->
    <div class="details-absolute">
        <div class="d-sm-flex container">
            <div><a href="#" class="text-white d-inline-block mr-4 "><i class="fe fe-map-pin text-white mr-2"></i>Mp-214, New York, NY 10012, US-52014</a></div>
            <div class="ml-auto"><a href="#" class="text-white d-inline-block mt-2 mt-sm-0"><i class="fe fe-phone text-white mr-2 fs-14"></i>468 865 9658</a></div>
        </div>
    </div>
</div>
<!--/Sliders Section-->

<!--BreadCrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Tours Details</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Categories</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tours Details</li>
            </ol>
        </div>
    </div>
</div>
<!--/BreadCrumb-->

<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">

                <!--Tours And Travels Overview-->
                <div class="card overflow-hidden">
                    <div class="ribbon ribbon-top-right text-danger"><span class="bg-danger fs-16 font-weight-bold">$456 <small class="fs-12"> /person</small></span></div>
                    <div class="card-body">
                        <div class="product-slider">
                            <div id="carousel2" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php $counter = 1;?>
                                    <?php foreach ($tour->images->getValue() as  $image):?>
                                        <div class="carousel-item <?=$counter==1 ? 'active' : '' ?>"><img src="<?=ImageHelper::path($image)?>" alt="img"> </div>
                                        <?php $counter++; ?>
                                    <?php endforeach; ?>
                                </div>
                                <a class="carousel-control-prev" href="#carousel2" role="button" data-slide="prev">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                </a>
                                <a class="carousel-control-next" href="#carousel2" role="button" data-slide="next">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="clearfix">
                                <div id="thumbcarousel2" class="carousel slide product-slide-thumb" data-interval="false">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <?php $counter = 0;?>
                                            <?php foreach ($tour->images->getValue() as  $image):?>
                                                <div data-target="#carousel2" data-slide-to="<?=$counter++?>" class="thumb"><img src="<?=ImageHelper::path($image)?>" alt="img"></div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#thumbcarousel2" role="button" data-slide="prev">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    </a>
                                    <a class="carousel-control-next" href="#thumbcarousel2" role="button" data-slide="next">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card details-accordion">
                    <div class="card-body">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 card-header bg-transparent" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Overview
                                </a>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="mb-0">
                                            <p class="leading-loose"><?=$tour->description?></p>
                                        </div>
                                        <?php if($tour->places):?>
                                            <?php $dataPlaces = []; ?>
                                            <?php foreach ($tour->places as $places): ?>
                                                <?php $dataPlaces[] = $places->title; ?>
                                            <?php endforeach; ?>
                                            <h4 class="card-title mt-6 mb-3 font-weight-semibold2">Travel Places</h4>
                                            <div class="item-user mt-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h6 class="font-weight-normal"><span>
                                                        <i class="fe fe-map-pin mr-3 mb-2 d-inline-block"></i></span><span class="text-body"> <?=explode(', ', $dataPlaces)?></span></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <h4 class="card-title mt-6 mb-3 font-weight-semibold2">Contact Info</h4>
                                        <div class="item-user mt-3">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h6 class="font-weight-normal"><span><i class="fe fe-mail mr-3 mb-2 d-inline-block"></i></span><a href="#" class="text-body"> robert123@gmail.com</a></h6>
                                                </div>
                                                <div class="col-md-4">
                                                    <h6 class="font-weight-normal"><span><i class="fe fe-phone mr-3 mb-2 d-inline-block"></i></span>0-235-657-24587</h6>
                                                </div>
                                                <div class="col-md-4">
                                                    <h6 class="font-weight-normal"><span><i class="fe fe-link mr-3 d-inline-block"></i></span><a href="#" class="text-secondary">https://spruko.com/</a></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    Specifications
                                </a>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="table-responsive">
                                            <table class="table row table-borderless w-100 m-0 text-nowrap">
                                                <tbody class="col-lg-12 col-xl-6 p-0">
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Локация')?> :</span> <?=$tour->country->title?><?=$tour->city ? ' - '.$tour->city->title : ''; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'мин. возраст')?> Min Age :</span> <?=$tour->min_age?></td>
                                                </tr>
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'макс. возраст')?> Max Age :</span> <?=$tour->max_age?></td>
                                                </tr>
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Минимально. человек')?> Max Age :</span> <?=$tour->minimum_quantity?></td>
                                                </tr>
                                                </tbody>
                                                <tbody class="col-lg-12 col-xl-6 p-0">
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'trip')?> :</span> <?=$tour->trip; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Identity Proof')?> :</span> <?=$tour->identify; ?> </td>
                                                </tr>
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Место отправки')?> :</span> <?=$tour->landing; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Способы оплаты')?> :</span>
                                                        <?php
                                                        $paymentMethods = [];
                                                        if(!empty($tour->payment_methods->getValue())){
                                                            foreach ($tour->payment_methods->getValue() as $paymentMethod){
                                                                $paymentMethods[] = Yii::$app->params['payment_methods'][$paymentMethod];
                                                            }
                                                        }
                                                        echo implode(', ', $paymentMethods);
                                                        ?>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    Included / Excluded
                                </a>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="table-responsive item-card2-desc">
                                            <table class="table row table-borderless w-100 m-0 text-nowrap">
                                                <?php $included = Yii::$app->params['included']; ?>
                                                <?php $excluded = Yii::$app->params['excluded']; ?>
                                                <?php $countParams = count($included) + count($excluded); ?>
                                                <?php $counterParams = 0; ?>
                                                <tbody class="col-lg-12 col-xl-6 p-0">
                                                <?php if($tour->included && !empty($tour->included->getValue())):?>
                                                <?php foreach ($tour->included->getValue() as $includedId):?>
                                                <tr>
                                                    <td class="px-0"><i class="fe fe-check text-success d-inline-block bg-success-transparent mr-2"></i>  <?=$included[$includedId]?></td>
                                                </tr>
                                                <?php unset($included[$includedId]);?>
                                                <?php $counterParams++;?>
                                                <?php if(ceil($countParams/2) == $counterParams):?>
                                                </tbody>
                                                <tbody>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                                <?php if($tour->excluded && !empty($tour->excluded->getValue())):?>
                                                <?php foreach ($tour->excluded->getValue() as $excludedId):?>
                                                <tr>
                                                    <td class="px-0"><i class="fe fe-check text-success d-inline-block bg-success-transparent mr-2"></i>  <?=$excluded[$excludedId]?></td>
                                                </tr>
                                                <?php unset($excluded[$excludedId]);?>
                                                <?php $counterParams++;?>
                                                <?php if(ceil($countParams/2) == $counterParams):?>
                                                </tbody>
                                                <tbody>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                                <?php foreach ($included as $val):?>
                                                <tr>
                                                    <td class="px-0"><i class="fe fe-x text-danger d-inline-block bg-danger-transparent mr-2"></i>  <?=$val?></td>
                                                </tr>
                                                <?php $counterParams++;?>
                                                <?php if(ceil($countParams/2) == $counterParams):?>
                                                </tbody>
                                                <tbody>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                                <?php foreach ($excluded as $val):?>
                                                <tr>
                                                    <td class="px-0"><i class="fe fe-x text-danger d-inline-block bg-danger-transparent mr-2"></i>  <?=$val?></td>
                                                </tr>
                                                <?php $counterParams++;?>
                                                <?php if(ceil($countParams/2) == $counterParams):?>
                                                </tbody>
                                                <tbody>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($tour->places): ?>
                                <div class="card">
                                    <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                        Tour Places
                                    </a>

                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                        <div class="card-body leading-normal-2">
                                            <div class="row row-deck">
                                                <?php foreach ($tour->places as $places): ?>
                                                    <div class="col-sm-6 col-lg-3">
                                                        <div class="text-center mb-4">
                                                            <a class="" href="#"><img src="<?=ImageHelper::path($places->image)?>" alt="img" class="br-tl-5 br-tr-5"></a>
                                                            <h6 class="mb-0 p-3 border border-top-0 br-bl-5 br-br-5"><?=$places->title?></h6>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php endif; ?>
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                    Special Tour Price
                                </a>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="table-responsive">
                                            <table class="table mb-0 table-bordered border-top table-striped text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th class="border-bottom-0">Category</th>
                                                    <th class="border-bottom-0">Couple</th>
                                                    <th class="border-bottom-0">Adult Single</th>
                                                    <th class="border-bottom-0">Child Without Bed</th>
                                                    <th class="border-bottom-0">Child With Bed</th>
                                                    <th class="border-bottom-0">Infant</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="font-weight-semibold">Actual Price</td>
                                                    <td class="fs-16 font-weight-semibold">$1850</td>
                                                    <td class="fs-16 font-weight-semibold">$1000</td>
                                                    <td class="fs-16 font-weight-semibold">$650</td>
                                                    <td class="fs-16 font-weight-semibold">$850</td>
                                                    <td class="fs-16 font-weight-semibold">$350</td>
                                                </tr>
                                                <tr>
                                                    <td class="font-weight-semibold">Offer Price</td>
                                                    <td class="fs-16 font-weight-semibold">$1500 <del class="fs-12 text-danger">$1850</del></td>
                                                    <td class="fs-16 font-weight-semibold">$850 <del class="fs-12 text-danger">$1000</del></td>
                                                    <td class="fs-16 font-weight-semibold">$550 <del class="fs-12 text-danger">$650</del></td>
                                                    <td class="fs-16 font-weight-semibold">$700 <del class="fs-12 text-danger">$850</del></td>
                                                    <td class="fs-16 font-weight-semibold">$200 <del class="fs-12 text-danger">$350</del></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($tour->video): ?>
                                <div class="card mb-0">
                                    <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header" data-toggle="collapse" id="headingSeven" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                        Tour Video
                                    </a>
                                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                                        <div class="card-body leading-normal-2">
                                            <iframe width="560" height="400" src="<?=$tour->video?>"  allow="accelerometer; autoplay;" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="pt-4 pb-4 pl-5 pr-5 border-top border-top">
                        <div class="list-id">
                            <div class="row">
                                <div class="col">
                                    <a class="mb-0">Tours And Travels ID : #<?=$tour->id?></a>
                                </div>
                                <div class="col col-auto">
                                    <?php Yii::t('app', 'Опубликовано');?>  <?php \common\components\helper\aDate::getFormatter(strtotime($tour->date_create), 'short')?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="icons">
                            <a href="#" class="btn btn-primary px-5">Book This Tour</a>
                            <a href="#" class="btn btn-light icons"><i class="fe fe-share-2 fs-16 leading-normal"></i></a>
                            <a href="#" class="btn btn-light icons"><i class="fe fe-heart fs-16 leading-normal"></i></a>
                            <a href="#" class="btn btn-light icons"><i class="fe fe-printer fs-16 leading-normal"></i></a>
                            <a href="#" class="btn btn-light icons mb-1 mt-1" data-toggle="modal" data-target="#report"><i class="fe fe-info fs-16 leading-normal"></i></a>
                        </div>
                    </div>
                </div>
                <!--/Tours And Travels Overview-->

                <!--Faqs-->
                <h3 class="mt-6 mb-4 fs-20">FAQS</h3>
                <div class="details-accordion mb-5">
                    <div class="accordion" id="accordionExample2">
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 card-header bg-transparent" id="headingOne1" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                What does it Cost to advertise?
                            </a>
                            <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingTwo1" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo1">
                                What Business Categories do you offer?
                            </a>
                            <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingThree1" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="true" aria-controls="collapseThree1">
                                How do I pay for my tour Listing ad?
                            </a>
                            <div id="collapseThree1" class="collapse" aria-labelledby="headingThree1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFour1" data-toggle="collapse" data-target="#collapseFour1" aria-expanded="true" aria-controls="collapseFour1">
                                Do you offer frequency Discounts?
                            </a>
                            <div id="collapseFour1" class="collapse" aria-labelledby="headingFour1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFive1" data-toggle="collapse" data-target="#collapseFive1" aria-expanded="true" aria-controls="collapseFive1">
                                How will I know if people are responding to my tour Listing ad?
                            </a>
                            <div id="collapseFive1" class="collapse" aria-labelledby="headingFive1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingSix1" data-toggle="collapse" data-target="#collapseSix1" aria-expanded="true" aria-controls="collapseSix1">
                                I Know I want to place  an ad,but I still have questions.What should I do?
                            </a>
                            <div id="collapseSix1" class="collapse" aria-labelledby="headingSix1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Faqs-->

                <!--Section-->
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Tour Program</div>
                    </div>
                    <div class="card-body">
                        <ul class="timeline-tour">
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/1.png" alt="" class="w-8 h-8 brround tour-before bg-primary">
                                <div class="timeline-data ml-5">
                                    <div class="">
                                        <div class="fs-16 font-weight-semibold"><b>Day 01:</b> <span class="text-secondary">08am-10am</span></div>
                                        <h3 class="card-title font-weight-semibold2 mt-1">Moraine Lake</h3>
                                        <div class="leading-loose">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                            quora plaxo ideeli hulu weebly balihoo...
                                        </div>
                                    </div>
                                    <div class="mt-5">
                                        <div class="fs-16 font-weight-semibold"><span class="text-secondary">10am-3pm</span></div>
                                        <h3 class="card-title font-weight-semibold2 mt-1">National Park</h3>
                                        <div class="leading-loose">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                            quora plaxo ideeli hulu weebly balihoo...
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/2.png" alt="" class="w-8 h-8 brround tour-before bg-secondary">
                                <div class="timeline-data ml-5">
                                    <div class="fs-16 font-weight-semibold"><b>Day 02:</b> <span class="text-secondary">08am-10pm</span></div>
                                    <h3 class="card-title font-weight-semibold2 mt-1">Suspension Bridge Park</h3>
                                    <div class="leading-loose">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/3.png" alt="" class="w-8 h-8 brround tour-before bg-success">
                                <div class="timeline-data ml-5">
                                    <div class="">
                                        <div class="fs-16 font-weight-semibold"><b>Day 03:</b> <span class="text-secondary">08am-11pm</span></div>
                                        <h3 class="card-title font-weight-semibold2 mt-1">New Travel location</h3>
                                        <div class="leading-loose">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                            quora plaxo ideeli hulu weebly balihoo...
                                        </div>
                                    </div>
                                    <div class="mt-5">
                                        <div class="fs-16 font-weight-semibold"><span class="text-secondary">11am-4pm</span></div>
                                        <h3 class="card-title font-weight-semibold2 mt-1">National Park</h3>
                                        <div class="leading-loose">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                            quora plaxo ideeli hulu weebly balihoo...
                                        </div>
                                    </div>
                                    <div class="mt-5">
                                        <div class="fs-16 font-weight-semibold"><span class="text-secondary">4pm-10pm</span></div>
                                        <h3 class="card-title font-weight-semibold2 mt-1">National Park2</h3>
                                        <div class="leading-loose">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                            quora plaxo ideeli hulu weebly balihoo...
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/4.png" alt="" class="w-8 h-8 brround tour-before bg-warning">
                                <div class="timeline-data ml-5">
                                    <div class="fs-16 font-weight-semibold"><b>Day 04:</b> <span class="text-secondary">08am-10pm</span></div>
                                    <h3 class="card-title font-weight-semibold2 mt-1">National Park</h3>
                                    <div class="leading-loose">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/5.png" alt="" class="w-8 h-8 brround tour-before bg-info">
                                <div class="timeline-data ml-5">
                                    <div class="fs-16 font-weight-semibold"><b>Day 05:</b> <span class="text-secondary">08am-10pm</span></div>
                                    <h3 class="card-title font-weight-semibold2 mt-1">CN Tower</h3>
                                    <div class="leading-loose">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/6.png" alt="" class="w-8 h-8 brround tour-before bg-danger">
                                <div class="timeline-data ml-5">
                                    <div class="fs-16 font-weight-semibold"><b>Day 06:</b> <span class="text-secondary">08am-10pm</span></div>
                                    <h3 class="card-title font-weight-semibold2 mt-1">Tour location</h3>
                                    <div class="leading-loose">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <img src="../../assets/images/categories/category/7.png" alt="" class="w-8 h-8 brround tour-before bg-blue">
                                <div class="timeline-data ml-5">
                                    <div class="fs-16 font-weight-semibold"><b>Day 07:</b> <span class="text-secondary">09am-12pm</span></div>
                                    <h3 class="card-title font-weight-semibold2 mt-1">Adventure Place</h3>
                                    <div class="leading-loose">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <?php if($relatedTours):?>
                    <h3 class="mt-6 mb-4 fs-20">Related Tours</h3>

                    <!--Related Posts-->
                    <div class="row">
                        <div id="myCarousel5" class="owl-carousel owl-carousel-icons3">
                            <!-- Wrapper for carousel items -->

                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div id="image-slider" class="carousel" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src="../../assets/images/categories/01.png" alt="img" class="cover-image">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="../../assets/images/categories/02.png" alt="img" class="cover-image">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="../../assets/images/categories/03.png" alt="img" class="cover-image">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="../../assets/images/categories/04.png" alt="img" class="cover-image">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="../../assets/images/categories/05.png" alt="img" class="cover-image">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev left-0" href="#image-slider" role="button" data-slide="prev">
                                            <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next right-0" href="#image-slider" role="button" data-slide="next">
                                            <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                            <span class="sr-only">Next</span>
                                        </a>
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$145</h4>
                                        </div>
                                        <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                            <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 5</span>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-car"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Chicago Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>3 Days</b>, <b>2 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin text-muted mr-1"></i> Chicago - 3 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto mt-4 mt-md-0">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (44)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <img src="../../assets/images/categories/05.png" alt="img" class="cover-image">
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$234</h4>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-car"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Australia Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>3 Days</b>, <b>2 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex mt-4 mt-md-0">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> Australia - 2 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (54)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <div id="image-slider22" class="carousel" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev left-0" href="#image-slider22" role="button" data-slide="prev">
                                                <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next right-0" href="#image-slider22" role="button" data-slide="next">
                                                <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                                <span class="sr-only">Next</span>
                                            </a>
                                            <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 2</span>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$320</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-bus"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Germany Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>5 Days</b>, <b>4 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> Germany- 7 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto mt-4 mt-md-0">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (78)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="ribbon ribbon-top-left text-danger"><span class="bg-danger">Urgent</span></div>
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <img src="../../assets/images/categories/03.png" alt="img" class="cover-image">
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$442</h4>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-bus"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">India Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>5 Days</b>, <b>4 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> India - 6 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto mt-4 mt-md-0">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (87)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <img src="../../assets/images/categories/04.png" alt="img" class="cover-image">
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$540</h4>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-ship"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Japan Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>12 Days</b>, <b>11 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex mt-4 mt-md-0">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> Japan - 12 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (67)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="ribbon ribbon-top-left text-primary"><span class="bg-primary">Collections</span></div>
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <img src="../../assets/images/categories/8.png" alt="img" class="cover-image">
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$320</h4>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-plane"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">London Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>5 Days</b>, <b>4 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> London - 3 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto mt-4 mt-md-0">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (78)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <img src="../../assets/images/categories/06.png" alt="img" class="cover-image">
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$315</h4>

                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-ship"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Los Angels Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>21 Days</b>, <b>20 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> USA - 21 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto mt-4 mt-md-0">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (65)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card mb-0 item-card2-card">
                                    <div class="item-card2-img">
                                        <a href="tours.html" class="absolute-link2"></a>
                                        <img src="../../assets/images/categories/07.png" alt="img" class="cover-image">
                                        <div class="item-card7-overlaytext">
                                            <h4 class="mb-0">$460</h4>
                                        </div>
                                    </div>
                                    <div class="item-card2-icons">
                                        <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-car"></i></a>
                                        <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text">
                                                    <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Spain Beautiful Places</h4></a>
                                                </div>
                                                <p class="fs-14 mb-1"><b>20 Days</b>, <b>19 Nights</b> Travel Trip</p>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer d-md-flex">
                                        <ul class="d-flex mb-1">
                                            <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> USA - 20 Places</a></li>
                                        </ul>
                                        <div class="item-card2-rating mb-0 ml-auto mt-4 mt-md-0">
                                            <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected>4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div> (43)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Related Posts-->
                <?php endif; ?>
                <!--Comments-->
                <div class="card mt-6">
                    <?php if($ratingStars): ?>
                        <div class="card-header">
                            <h3 class="card-title">Rating And Reviews</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $sumCountReviews = 0;?>
                                    <?php foreach ($ratingStars as $ratingStar): ?>
                                        <?php $sumCountReviews += $ratingStar['count_reviews'];?>
                                    <?php endforeach;?>
                                    <?php foreach ($ratingStars as $ratingStar): ?>
                                        <div class="mb-5">
                                            <div class="d-flex"><div class="badge badge-default bg-light mb-2 text-primary"><?=$ratingStar['count_stars']?> <i class="fa fa-star"></i></div> <div class="ml-auto font-weight-semibold2 fs-16"><?=$ratingStar['count_reviews']?></div></div>
                                            <div class="progress progress-md mb-4 h-1">
                                                <div class="progress-bar bg-primary" style="width: <?=ceil($ratingStar['count_reviews'] * 100 / $sumCountReviews )?>%"></div>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php if($countComments):?>
                        <div class="card-body">
                            <h4 class="font-weight-semibold2 text-secondary"><?=$countComments?> Reviews</h4>
                            <ul class="timeline-tour mt-5">
                                <?= $this->render('_comments', ['comments' => $comments])?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <!--/Comments-->

                <div class="card mb-lg-0">
                    <div class="card-header">
                        <h3 class="card-title">Add a Review</h3>
                    </div>
                    <?php $modelRatingStars = new \common\models\RatingStars(); ?>
                    <?php $modelRatingStars->tour_id = $tour->id; ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'form-stars'
                    ]);?>
                    <?=$form->field($modelRatingStars, 'tour_id')->hiddenInput()->label(false); ?>
                    <div class="card-body">
                        <div class="d-flex">
                            <p class="mb-0 fs-14 font-weight-semibold">Over All Rating</p>
                            <div class="star-ratings start-ratings-main clearfix ml-5">
                                <div class="stars stars-example-fontawesome">
                                    <select  class="example-fontawesome" name="Rating[count_stars]" autocomplete="off" id="rating-stars">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div class="card-body">
                        <div id="block-form-comment">
                            <?php $modelComments = new \common\models\Comments(); ?>
                            <?php $modelComments->tour_id = $tour->id; ?>
                            <?php echo $this->render('../page/_form_comment', ['model' => $modelComments]); ?>
                        </div>
                        <a href="#" class="btn btn-secondary add-comment" >Send Reply</a>
                    </div>
                </div>
            </div>

            <!--Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Tour Organizer</h3>
                    </div>
                    <div class="card-body item-user">
                        <div class="profile-details">
                            <div class="profile-pic mb-0 mx-5">
                                <img src="../../assets/images/other/logo.jpg" class="brround w-150 h-150" alt="user">
                            </div>
                        </div>
                        <div class="text-center mt-2">
                            <a href="userprofile.html" class="text-dark text-center"><h4 class="mt-0 mb-1 font-weight-semibold2">Robert McLean</h4></a>
                            <span class="text-muted mt-1">Tour Organizer <b> Since November 2008</b></span>
                            <div><small class="text-muted">Listing Tour Id <b>365241</b></small></div>
                        </div>
                    </div>
                    <div class="profile-user-tabs">
                        <div class="tab-menu-heading border-0 p-0">
                            <div class="tabs-menu1">
                                <ul class="nav">
                                    <li class=""><a href="#tab-contact" class="active" data-toggle="tab">Contat</a></li>
                                    <li><a href="#tab-timings" data-toggle="tab">New Tours</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content border-0 bg-white">
                        <div class="tab-pane active" id="tab-contact">
                            <div class="card-body item-user">
                                <h4 class="mb-4">Contact Info</h4>
                                <div>
                                    <h6><span class="font-weight-semibold"><i class="fa fa-map mr-3 mb-2"></i></span><a href="#" class="text-body"> Mp-214, New York, NY 10012</a></h6>
                                    <h6><span class="font-weight-semibold"><i class="fa fa-envelope mr-3 mb-2"></i></span><a href="#" class="text-body"> robert123@gmail.com</a></h6>
                                    <h6><span class="font-weight-semibold"><i class="fa fa-phone mr-3 mb-2"></i></span>0-235-657-24587</h6>
                                    <h6><span class="font-weight-semibold"><i class="fa fa-link mr-3 "></i></span><a href="#" class="text-secondary">https://spruko.com/</a></h6>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-timings">
                            <div class="table-responsive card-body">
                                <table class="table table-bordered border-top mb-0 text-nowrap">
                                    <tbody>
                                    <tr>
                                        <td>Canada Tour</td>
                                        <td class="font-weight-semibold">18th Dec - 10th Jan</td>
                                    </tr>
                                    <tr>
                                        <td>Europe Tour</td>
                                        <td class="font-weight-semibold">10th Nov - 21st Dec</td>
                                    </tr>
                                    <tr>
                                        <td>France Tour</td>
                                        <td class="font-weight-semibold">15th Dec - 6th Feb</td>
                                    </tr>
                                    <tr>
                                        <td>Sydney Tour</td>
                                        <td class="font-weight-semibold">18th Nov - 21st Jan</td>
                                    </tr>
                                    <tr>
                                        <td>Australia Tour</td>
                                        <td class="font-weight-semibold">16th Sep - 10th Nov</td>
                                    </tr>
                                    <tr>
                                        <td>USA Tour</td>
                                        <td class="font-weight-semibold">5th Oct - 16th Dec</td>
                                    </tr>
                                    <tr>
                                        <td>Italy Tour</td>
                                        <td class="font-weight-semibold">14th Oct - 19th Nov</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="">
                                <a href="#" class="btn btn-secondary mt-1 mb-1" data-toggle="modal" data-target="#contact"><i class="fa fa-user mr-1"></i> Contact Me</a>
                                <a href="<?=Url::to(['tours/'])?>" class="btn btn-light mt-1 mb-1"><i class="fa fa-eye mr-1"></i>All Tours</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Search Tours</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <input type="text" class="form-control keywords-input" id="search-text" placeholder="Search Tours">
                        </div>
                        <div class="form-group">
                            <select name="country" id="select-countries" class="form-control custom-select select2-show-search">
                                <option value="1" selected>Select Category</option>
                                <option value="2">Mountains</option>
                                <option value="3">Flyings</option>
                                <option value="4">Seas</option>
                                <option value="5">Histrorical</option>
                                <option value="6">Climbing</option>
                                <option value="7">Snow Veiws</option>
                                <option value="8">Pyramids</option>
                                <option value="9">Deserts</option>
                                <option value="10">Forests</option>
                            </select>
                        </div>
                        <div >
                            <a href="#" class="btn  btn-block btn-secondary">Search</a>
                        </div>
                    </div>
                </div>
                <?php if($recentTours):?>
                    <h3 class="mt-6 mb-4 fs-20">Recent Tours</h3>
                    <div class="recent-posts overflow-hidden">
                        <div class="">
                            <div id="myCarousel4" class="owl-carousel testimonial-owl-carousel2">
                                <?php foreach ($recentTours as $recentTour):?>
                                    <div class="item">
                                        <div class="card mb-0 item-card2-card ieh-100">
                                            <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                            <div id="image-slider1" class="carousel ieh-100" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <?php if($recentTour->images && !empty($recentTour->images->getValue())): ?>
                                                        <?php foreach ($recentTour->images->getValue() as $key => $image):?>
                                                            <div class="carousel-item <?=$key == 0 ? 'active' : ''?>">
                                                                <img src="<?=ImageHelper::path($image)?>" alt="img" class="cover-image">
                                                            </div>
                                                        <?php endforeach;?>
                                                    <?php endif; ?>
                                                </div>
                                                <a class="carousel-control-prev left-0" href="#image-slider1" role="button" data-slide="prev">
                                                    <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next right-0" href="#image-slider1" role="button" data-slide="next">
                                                    <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                                <div class="item-card7-overlaytext">
                                                    <h4 class="mb-0">$145</h4>
                                                </div>
                                                <?php if($recentTour->images && !empty($recentTour->images->getValue())): ?>
                                                    <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                        <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> <?=count($recentTour->images->getValue())?></span>
                                                    </div>
                                                <?php endif;?>
                                            </div>
                                            <div class="item-card2-icons">
                                                <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-plane"></i></a>
                                                <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                            <div class="card-body">
                                                <div class="item-card2">
                                                    <div class="item-card2-desc">
                                                        <div class="item-card2-text">
                                                            <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3"><?=$recentTour->title?></h4></a>
                                                        </div>
                                                        <p class="fs-14 mb-1"><b><?=$recentTour->trip?> Days</b>, <b><?=(int)$recentTour->trip ? (int)$recentTour->trip - 1 : '';?> Nights</b> Travel Trip</p>
                                                        <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer d-flex">
                                                <ul class="d-flex mb-1">
                                                    <li class=""><a href="#" class="icons"><i class="fe fe-map-pin text-muted mr-1"></i> Chicago - 3 Places</a></li>
                                                </ul>
                                                <div class="item-card2-rating mb-0 ml-auto">
                                                    <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                        <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                            <select class="example-fontawesome" name="rating" autocomplete="off">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4" selected>4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div> (44)
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card">
                                        <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                        <div class="item-card2-img">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <img src="../../assets/images/categories/05.png" alt="img" class="cover-image">
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$234</h4>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-plane"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Australia Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>3 Days</b>, <b>2 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> Australia - 2 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (54)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card ieh-100">
                                        <div class="item-card2-img ieh-100">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <div id="image-slider12" class="carousel ieh-100" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="../../assets/images/categories/09.png" alt="img" class="cover-image">
                                                    </div>
                                                </div>
                                                <a class="carousel-control-prev left-0" href="#image-slider12" role="button" data-slide="prev">
                                                    <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next right-0" href="#image-slider12" role="button" data-slide="next">
                                                    <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                                <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                    <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 2</span>
                                                </div>
                                                <div class="item-card7-overlaytext">
                                                    <h4 class="mb-0">$320</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-car"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Germany Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>5 Days</b>, <b>4 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> Germany- 7 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (78)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card">
                                        <div class="ribbon ribbon-top-left text-danger"><span class="bg-danger">Urgent</span></div>
                                        <div class="item-card2-img">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <img src="../../assets/images/categories/03.png" alt="img" class="cover-image">
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$442</h4>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-bus"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">India Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>5 Days</b>, <b>4 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> India - 6 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (87)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card">
                                        <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                        <div class="item-card2-img">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <img src="../../assets/images/categories/04.png" alt="img" class="cover-image">
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$540</h4>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-plane"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Japan Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>12 Days</b>, <b>11 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> Japan - 12 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (67)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card">
                                        <div class="ribbon ribbon-top-left text-primary"><span class="bg-primary">Collections</span></div>
                                        <div class="item-card2-img">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <img src="../../assets/images/categories/8.png" alt="img" class="cover-image">
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$320</h4>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-ship"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">London Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>5 Days</b>, <b>4 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> London - 3 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (78)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card">
                                        <div class="item-card2-img">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <img src="../../assets/images/categories/06.png" alt="img" class="cover-image">
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$315</h4>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-plane"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Los Angels Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>21 Days</b>, <b>20 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> USA - 21 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (65)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card mb-0 item-card2-card">
                                        <div class="item-card2-img">
                                            <a href="tours.html" class="absolute-link2"></a>
                                            <img src="../../assets/images/categories/07.png" alt="img" class="cover-image">
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0">$460</h4>
                                            </div>
                                        </div>
                                        <div class="item-card2-icons">
                                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-plane"></i></a>
                                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-card2">
                                                <div class="item-card2-desc">
                                                    <div class="item-card2-text">
                                                        <a href="tours.html" class="text-dark"><h4 class="font-weight-bold mb-3">Spain Beautiful Places</h4></a>
                                                    </div>
                                                    <p class="fs-14 mb-1"><b>20 Days</b>, <b>19 Nights</b> Travel Trip</p>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, quis int nostrum exercitationem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <ul class="d-flex mb-1">
                                                <li class=""><a href="#" class="icons"><i class="fe fe-map-pin fs-14 text-muted mr-1"></i> USA - 20 Places</a></li>
                                            </ul>
                                            <div class="item-card2-rating mb-0 ml-auto">
                                                <div class="star-ratings start-ratings-main clearfix d-inline-flex">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> (43)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="card mt-5">
                    <div class="card-body">
                        <h4 class="font-weight-semibold2">Need Help for any Details?</h4>
                        <div class="support-service bg-light br-5 mt-4">
                            <i class="fa fa-phone bg-primary text-white"></i>
                            <h6 class="text-default font-weight-bold mt-1">+68 872-627-9735</h6>
                            <p class="text-default mb-0 fs-12">Free Support!</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Side Content-->
        </div>
    </div>
</section>

<!--Comment Modal -->
<div class="modal fade" id="Comment" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Leave a Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="reply-comment">
                <?php echo $this->render('../page/_form_comment', ['model' => $modelComments]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success add-reply-comment">Send</button>
            </div>
        </div>
    </div>
</div>

<!--Map Modal -->
<div class="modal fade" id="Map-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Direction</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="h-400">
                    <!--Map-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" class="h-100 w-100 border-0"></iframe>
                    <!--/Map-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Report Modal -->
<div class="modal fade" id="report" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplereportLongTitle">Report Abuse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php  $modelComplaint = new \common\models\Complaint(); ?>
            <div id="block-form-report">
                <?php echo $this->render('../page/_form_complaint', ['modelComplaint' => $modelComplaint]); ?>
            </div>

        </div>
    </div>
</div>

<!-- Message Modal -->
<div class="modal fade" id="contact" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplecontactLongTitle">Send Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" id="contact-name" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="contact-email" placeholder="Email Address">
                </div>
                <div class="form-group mb-0">
                    <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Message"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Send</button>
            </div>
        </div>
    </div>
</div>

<!--  Modal Popup -->
<div class="modal fade" id="homeVideo" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="pauseVid()"><i class="fe fe-x"></i></button>
            <!--<iframe width="560" height="400" src="https://www.youtube.com/embed/kFjETSa9N7A"  allow="accelerometer; autoplay;" allowfullscreen></iframe>-->
            <div class="embed-responsive embed-responsive-16by9">
                <video id="gossVideo" class="embed-responsive-item" controls="controls">
                    <source src="../../assets/video/1.mp4" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
</div>

<!--  Modal to login -->
<div class="modal fade" id="to-login" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            Для оценки отеля вам нужно <a href="<?=Url::to(['login/'])?>">авторизоваться</a> или <a href="<?=Url::to(['registration/'])?>">зарегистрироваться</a>
        </div>
    </div>
</div>

<!--  Modal to login -->
<div class="modal fade" id="success-comment" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            спасибо за комент
        </div>
    </div>
</div>

<!--  Modal Popup -->
<div class="modal fade" id="gallery" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="product-slider modal-body p-2">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <a class="gallery-close-button" href="#" data-dismiss="modal" aria-label="Close"><i class="fe fe-x"></i></a>
                    <div class="carousel-inner">
                        <div class="carousel-item active"><img src="../../assets/images/places/1.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/2.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/3.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/4.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/5.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/1.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/2.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/3.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/4.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/5.jpg" alt="img"> </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="clearfix">
                    <div id="thumbcarousel" class="carousel slide product-slide-thumb" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="../../assets/images/places/1.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="../../assets/images/places/2.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="../../assets/images/places/3.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="../../assets/images/places/4.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="../../assets/images/places/5.jpg" alt="img"></div>

                            </div>
                            <div class="carousel-item ">
                                <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="../../assets/images/places/1.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="../../assets/images/places/2.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="../../assets/images/places/3.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="../../assets/images/places/4.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="../../assets/images/places/5.jpg" alt="img"></div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#thumbcarousel" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a>
                        <a class="carousel-control-next" href="#thumbcarousel" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" ><i class="fe fe-arrow-up"></i></a>

<?php
$this->registerJs('        
        $(document).ready(function() { 
            
            $("#add-complaint").on("click", function () {
                $.ajax({
                    method: "POST",
                    url: "'.Url::to(['page/add-complaint']).'",
                    data: $("#form-complaint").serialize(),
                    success: function(res){
                        console.log(res);
                        if(res == "ok"){
                            $("#report").modal("hide");
                        } else {
                            $("#block-form-report").html(res);
                        }                    
                    }
                })
            });
            
            $("#rating-stars").on("change", function(){
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: "'.Url::to(['page/add-rating-stars']).'",
                    data: $("#form-stars").serialize(),
                    success: function(res){
                        console.log(res);
                        if(res.status != "ok"){
                            $("#to-login").modal({
                                show: true
                            });
                        }                   
                    }
                });
            });
            
            $("body").on("click", ".add-comment",function(e){
                e.preventDefault();
                var data = $(this).parent().find("form").serialize();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: "'.Url::to(['page/add-comment']).'",
                    data: data,
                    success: function(res){
                        $("#block-form-comment").html(res.form);
                        if(res.status == "ok"){                        
                            $("#success-comment").modal({
                                show: true
                            });
                        }                   
                    }
                });
            });
        });
        
        $(".reply-comment").on("click", function(e){
            e.preventDefault();
            var commentId = $(this).data("id");
            $("#reply-comment").find(".reply_parent").val(commentId);            
            $("#Comment").modal({
                show: true
            });
        });
        
        $(".add-reply-comment").on("click", function(e){
            e.preventDefault();
            data = $(this).parents(".modal-content:first").find("form").serialize();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: "'.Url::to(['page/add-comment']).'",
                data: data,
                success: function(res){
                    $("#reply-comment").html(res.form);
                    if(res.status == "ok"){                        
                        $("#Comment").modal("hide");                        
                        $("#success-comment").modal({
                            show: true
                        });
                    }                   
                }
            });
        });
        ',
    \yii\web\View::POS_END,
    'form-complaint'
);
?>
<script>

</script>