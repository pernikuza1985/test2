<?php
use \common\models\RoomType;
use \common\models\Room;
?>

<div class="col-xl-3 col-lg-3 col-md-5">
    <form id="block-filter">
        <div class="card mb-0">
        <div class="card-header">
            <h3 class="card-title"><i class="fe fe-sliders"></i> <?=Yii::t('app', 'Фильтр')?></h3>
        </div>
        <?php if(isset($filters['type_room']) && count($filters['type_room']) > 0): ?>
            <div class="card-body pb-0">
                <h5 class="font-weight-semibold2 mb-3"><?=Yii::t('app', 'Тип комнаты')?></h5>
                <div class="">
                    <div class="filter-product-checkboxs">
                        <?php foreach ($filters['type_room'] as $filter): ?>
                            <label class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" value="<?=$filter['id']; ?>" name="type_room">
                                <span class="custom-control-label">
                                    <a class="text-dark"><?=$filter['title']?><span class="label label-light float-right"><?=$filter['count_hotel']?></span></a>
                                </span>
                            </label>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <div class="card-body border-top-0 pb-2">
                <h5 class="font-weight-semibold2 mb-3"><?=Yii::t('app', 'Цена')?></h5>
                <input type="text" name="price" id="price" class="h6">
                <div id="mySlider"></div>
            </div>
        <?php endif; ?>
        <div class="card-body border-top-0 pt-0">
            <button class="btn btn-secondary btn-block" id="apply_filter" ><?=Yii::t('app', 'Применить филтр')?></button>
        </div>
    </div>
    </form>
</div>


<script src="/assets/plugins/jquery-uislider/jquery-ui.js"></script>
<?php
if(isset($filters['price_min']) && isset($filters['price_max'])){
    $this->registerJs('
        (function($) {
            "use strict";
            $( "#mySlider" ).slider({
                range: true,
                min: '.(int)$filters['price_min'].',
                max: '.(int)$filters['price_max'].',
                values: ['.(int)$filters['price_min'].', '.(int)$filters['price_max'].'],
                slide: function( event, ui ) {
                    $( "#price" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                }
            });

            $( "#price" ).val( "$" + $( "#mySlider" ).slider( "values", 0 ) +
                       " - $" + $( "#mySlider" ).slider( "values", 1 ) );

            })(jQuery);
        ',
        \yii\web\View::POS_READY,
        'slider-price'
    );
    $this->registerJs('
        
        $("#apply_filter").on("click", function(e){
            e.preventDefault();
            var data = $("#block-filter").serializeArray();
            var filters = new Array();
            for(key in data){
                console.log(data[key]);
                if(filters[data[key].name] == undefined){
                    filters[data[key].name] = data[key].value
                } else {
                    filters[data[key].name] += ","+data[key].value
                }                
            }
            url = getUrlMainSearch();
            for(key in filters){
                if(key == "price"){
                    price = filters[key].replace(/\s/g, "");
                    prices = price.split("-");
                    url += "price_from="+prices[0].match(/\d+/)+"/price_to="+prices[1].match(/\d+/);
                } else {
                    url += key+"="+filters[key]+"/";
                }
                
            }
            location = "'.\yii\helpers\Url::to('/hotels').'/"+url;
        });
        ',
        \yii\web\View::POS_READY,
        'apply-filter'
    );
}

?>