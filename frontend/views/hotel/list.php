<?php
use \common\models\Region;
use \common\models\Hotel;
use \common\models\Category;
?>
<link href="/assets/plugins/jquery-uislider/jquery-ui.css" rel="stylesheet">
<div class="cover-image sptb  py-9 bg-background" data-image-src="/assets/images/banners/banner16.jpg">
    <div class="header-text1 mb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
                    <div class="text-center text-white ">
                        <h2 class="fs-40">
<!--                            <span class="font-weight-bold">96,758</span>-->
                            Hotels available
                        </h2>
                    </div>
                    <div class="search-background">
                        <form id="main-search">
                            <div class="form row no-gutters">
                                <div class="form-group col-xl-8 col-lg-8 col-md-12 mb-0 location" style="flex:inherit;max-width:inherit;">
                                    <div class="row no-gutters">
                                        <div class="form-group  col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg" style="min-width: 275px;margin-left: 22px;">
                                            <?php $regions = Region::find()
                                                ->join('JOIN', Hotel::tableName(), Region::tableName().'.id = '.Hotel::tableName().'.region_id')
                                                ->select(Region::tableName().".url, ".Region::tableName().".title")
                                                ->orderBy('title')
                                                ->groupBy(Region::tableName().'.id')
                                                ->all();
                                            ?>
                                            <select class="form-control select2-show-search border-bottom-0 w-200" data-placeholder="Регионы" id="filter-region">
                                                <optgroup>
                                                    <option value="">Регионы</option>
                                                    <?php foreach ($regions as $region):?>
                                                        <option value="<?=$region->url?>" <?=isset($filter['region']) && $filter['region'] == $category->id ? 'selected=selected' : '' ?> ><?=$region->title?></option>
                                                    <?php endforeach;?>
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="form-group  col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg" style="min-width: 275px">
                                            <?php $categories = Category::find()
                                                ->join('JOIN', Hotel::tableName(), Category::tableName().'.id = ANY('.Hotel::tableName().'.category_id)')
                                                ->select(Category::tableName().'.url, '.Category::tableName().'.title')->orderBy('title')
                                                ->all();
                                            ?>
                                            <select class="form-control select2-show-search border-bottom-0 w-100" data-placeholder="Категории" id="filter-category">
                                                <optgroup>
                                                    <option value="">Категории</option>
                                                    <?php foreach ($categories as $category):?>
                                                        <option value="<?=$category->url?>" <?=isset($filter['category']) && $filter['category'] == $category->id ? 'selected=selected' : '' ?>><?=$category->title?></option>
                                                    <?php endforeach;?>
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-3 col-lg-3 col-md-12 mb-0">
                                            <input class="form-control input-lg fc-datepicker br-0 border-right border-left" placeholder="Travel Date" type="text" id="filter-date-from"
                                                   value="<?=isset($filter['date_from']) ? $filter['date_from'] : '' ?>">
                                        </div>
                                        <div class="col-xl-2 col-lg-2 col-md-12 mb-0">
                                            <a class="btn btn-block btn-secondary btn-lg fs-14 br-tl-0 br-bl-0 shadow-none" id="search-filter" href="#">Search</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /header-text -->
</div>
<!--/Sliders Section-->

<!--Breadcrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title"><?=Yii::t('app', 'Санатории'); ?></h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><?=Yii::t('app', 'Главная'); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=Yii::t('app', 'Санатории'); ?></li>
            </ol>
        </div>
    </div>
</div>
<!--/Breadcrumb-->

<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!--Add lists-->
                <?php
                $dataList = $dataPagination;
                $dataList['hotels'] = $hotels;
                ?>
                <?php echo $this->render('_list', $dataList);?>
                <!--/Add lists-->
            </div>
            <?php echo $this->render('_filter', ['filters' => $filter]);?>
        </div>
    </div>
</section>
<!--/Add Listings-->
<?php
    $this->registerJs('        
        $("#search-filter").on("click", function () {
            url = getUrlMainSearch();
            location = "'.\yii\helpers\Url::to('/hotels').'/"+url;
        });
        function getUrlMainSearch(){
            var url = "";
            var category = $("#filter-category").val();
            var region = $("#filter-region").val();
            var dateFrom = $("#filter-date-from").val();
            if(category){
                url += category+"/";
            }   
            if(region){
                url += region+"/";
            }   
            if(dateFrom){
                date = dateFrom.replace(/\s/g, "");
                date = dateFrom.replace(/\//g, ":");
                url += "date_from="+date+"/";
            }   
            
            return url;
        }
        
        '

    );
?>

<a href="#top" id="back-to-top" ><i class="fe fe-arrow-up"></i></a>

<!--<script src="/assets/plugins/jquery-uislider/jquery-ui.js"></script>-->
<!---->
<!--<script src="/assets/js/price-range.js"></script>-->