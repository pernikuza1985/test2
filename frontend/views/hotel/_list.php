<?php
use \common\components\helper\ImageHelper;
use \common\models\RatingStars;
use \common\components\helper\FilterHelper;
use \yii\helpers\Url;
?>
<div class=" mb-lg-0">
    <div class="">
        <div class="item2-gl ">
            <div class="">
                <div class="bg-white p-5 item2-gl-nav d-flex">
                    <div class="selectgroup">
                        <label class="selectgroup-item mb-md-0">
                            <input type="radio" name="value" value="All" class="selectgroup-input" checked="">
                            <span class="selectgroup-button d-md-flex">All <i class="fa fa-sort ml-2 mt-1"></i></span>
                        </label>
                        <label class="selectgroup-item mb-md-0">
                            <input type="radio" name="value" value="Distance" class="selectgroup-input">
                            <span class="selectgroup-button">Famous</span>
                        </label>
                        <label class="selectgroup-item mb-0">
                            <input type="radio" name="value" value="Latest" class="selectgroup-input">
                            <span class="selectgroup-button">Latest</span>
                        </label>
                        <label class="selectgroup-item mb-0">
                            <input type="radio" name="value" value="Rating" class="selectgroup-input">
                            <span class="selectgroup-button">Rating</span>
                        </label>
                    </div>
                    <ul class="nav item2-gl-menu ml-auto mt-1">
                        <li class=""><a href="#tab-11" data-toggle="tab" class="active show" title="List style"><i class="fe fe-list"></i></a></li>
                        <li><a href="#tab-12" data-toggle="tab"  title="Grid"><i class="fe fe-grid"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-11">
                    <div class="row">
                        <?php foreach ($hotels as $hotel):?>
                            <div class="col-xl-12 col-lg-6 col-md-12">
                            <div class="card overflow-hidden">
                                <div class="d-xl-flex ieh-100">
                                    <div class="item-card9-img w-100">
                                        <div class="item-card9-imgs">
                                            <img src="<?=ImageHelper::path($hotel->image);?>" alt="img" class="cover-image">
                                            <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 5</span>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <?php $price = \common\models\PriceRoom::find()->where(['hotel_id' => $hotel->id])->min('price');?>
                                                <?php $hotel->minPrice = $price; ?>
                                                <?php if($price): ?>
                                                    <h4 class="mb-0 py-2 fs-20"><?=\common\components\helper\PriceHelper::Currency($price); ?></h4>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                        <div class="item-card9-icons">
                                            <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                    </div>
                                    <div class="card border-0 mb-0 br-0">
                                        <div class="card-body">
                                            <div class="item-card9">
                                                <div class="d-sm-flex">
                                                    <div class="">
                                                        <?php $counterCategory = 0;?>
                                                        <?php foreach ($hotel->categories as $category):?>
                                                            <?php if($counterCategory == 2): ?>
                                                                <?php break; ?>
                                                            <?php endif; ?>
                                                            <?php $counterCategory++; ?>
                                                            <span class="badge badge-pill badge-light" style="text-align:inherit;margin-bottom: 7px;white-space:inherit"><?=$category->title?></span>
                                                        <?php endforeach; ?>
                                                        <a href="<?=Url::to(['hotel/'.$hotel->url]);?>" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal"><?=$hotel->title; ?> </h4></a>
                                                        <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i>
                                                            <?=trim($hotel->country->title)?><?=$hotel->region ? ', '.trim($hotel->region->title) : '' ?><?=$hotel->city ? ', '.trim($hotel->city->title) : '' ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="fs-14 mt-1 mb-2 leading-normal"><?=$hotel->description; ?></p>
                                            <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <?php $rating = RatingStars::find()->where(['hotel_id' => $hotel->id])->average('count_stars'); ?>
                                                    <?php $rating = ceil($rating); ?>
                                                    <?php $hotel->rating = $rating; ?>
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1" <?=$rating==1 ? 'selected' : ''; ?>>1</option>
                                                        <option value="2" <?=$rating==2 ? 'selected' : ''; ?>>2</option>
                                                        <option value="3" <?=$rating==3 ? 'selected' : ''; ?>>3</option>
                                                        <option value="4" <?=$rating==4 ? 'selected' : ''; ?>>4</option>
                                                        <option value="5" <?=$rating==5 ? 'selected' : ''; ?>>5</option>
                                                    </select>
                                                </div>
                                                    <a class="fs-13 leading-tight mt-1" href="#"><?=$hotel->countComments?> <?=Yii::t('app', 'Отзыва')?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="tab-pane" id="tab-12">
                    <div class="row">
                        <?php foreach ($hotels as $hotel):?>
                            <div class="col-lg-6 col-md-12">
                            <div class="card overflow-hidden">
                                <div class="ieh-100">
                                    <div class="item-card9-img w-100">
                                        <div class="item-card9-imgs">
                                            <img src="<?=ImageHelper::path($hotel->image);?>" alt="img" class="cover-image">
                                            <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 5</span>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <?php if($hotel->minPrice):?>
                                                    <h4 class="mb-0 py-2 fs-20"><?=$hotel->minPrice?></h4>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                        <div class="item-card9-icons">
                                            <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                        </div>
                                    </div>
                                    <div class="card border-0 mb-0 br-0">
                                        <div class="card-body">
                                            <div class="item-card9">
                                                <div class="d-sm-flex">
                                                    <div class="">
                                                        <?php $counterCategory = 0;?>
                                                        <?php foreach ($hotel->categories as $category):?>
                                                            <?php if($counterCategory == 2): ?>
                                                                <?php break; ?>
                                                            <?php endif; ?>
                                                            <?php $counterCategory++; ?>
                                                            <span class="badge badge-pill badge-light" style="text-align:inherit;margin-bottom: 7px;white-space:inherit" ><?=$category->title?></span>
                                                        <?php endforeach; ?>
                                                        <a href="<?=Url::to(['hotel/'.$hotel->url]);?>" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal"><?=$hotel->title?> </h4></a>
                                                        <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i>
                                                            <?=trim($hotel->country->title)?><?=$hotel->region ? ', '.trim($hotel->region->title) : '' ?><?=$hotel->city ? ', '.trim($hotel->city->title) : '' ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="fs-14 mt-1 mb-2 leading-normal"><?=$hotel->description ?></p>
                                            <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                    <select class="example-fontawesome" name="rating" autocomplete="off">
                                                        <option value="1" <?=$hotel->rating==1 ? 'selected' : ''; ?>>1</option>
                                                        <option value="2" <?=$hotel->rating==2 ? 'selected' : ''; ?>>2</option>
                                                        <option value="3" <?=$hotel->rating==3 ? 'selected' : ''; ?>>3</option>
                                                        <option value="4" <?=$hotel->rating==4 ? 'selected' : ''; ?>>4</option>
                                                        <option value="5" <?=$hotel->rating==5 ? 'selected' : ''; ?>>5</option>
                                                    </select>
                                                </div> <a class="fs-13 leading-tight mt-1" href="#"><?=$hotel->countComments?> <?=Yii::t('app', 'Отзыва')?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-sm-flex">
            <h6 class="mb-4 mb-sm-0 mt-3">Showing <b><?=($currentPage - 1) * $limit + 1; ?> to <?=$currentPage * $limit > $countHotels ? ($currentPage - 1) * $limit + ($countHotels - (($currentPage - 1) * $limit)) : $currentPage * $limit; ?></b> of <?=$countHotels; ?> entries</h6>
            <?php if($countPage > 1): ?>
                <ul class="pagination mb-5 ml-auto">
                    <?php if($currentPage > 1):?>
                        <li class="page-item page-prev disabled">
                            <a class="page-link" href="#" tabindex="-1">Prev</a>
                        </li>
                    <?php endif; ?>
                    <?php for($i = 1; $i < $countPage; $i++):?>
                        <li class="page-item <?=$i == $currentPage ? 'active' : ''?>"><a class="page-link" href="#">$i</a></li>
                    <?php endfor; ?>
                    <?php if($currentPage + 1 > $countPage):?>
                        <li class="page-item page-next">
                            <a class="page-link" href="<?=FilterHelper::urlPage($currentPage + 1)?>">Next</a>
                        </li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>