<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Бронирование');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Санатории'), 'url' => Url::to(['/hotels'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $hotel->title), 'url' => Url::to(['/hotel/'.$hotel->url])];
$this->params['breadcrumbs'][] = $this->title;
?>

<!--Add posts-section-->
<section class="sptb">
    <div class="container">
        <div class="row ">
            <div class="col-lg-8 col-md-12">
                <?php $form = ActiveForm::begin();?>
                    <div class="card">
                        <div class="card-header ">
                            <h3 class="card-title"><?=Yii::t('app', 'Забронировать') ?></h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="form-label text-dark"><?=Yii::t('app', 'От');?></label>
                                        <?=$form->field($model, 'date_from')->textInput(['class' => 'form-control fc-datepicker'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="form-label text-dark"><?=Yii::t('app', 'До');?></label>
                                        <?=$form->field($model, 'date_to')->textInput(['class' => 'form-control fc-datepicker'])->label(false); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="form-label text-dark"><label class="form-label text-dark"><?=Yii::t('app', 'Взрослых');?></label></label>
                                        <?php $countPeople = ['' => ''];
                                            for($i = 1; $i <= 15; $i++){$countPeople[$i] = $i;}
                                            echo $form->field($model, 'adults')->dropDownList($countPeople)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="form-label text-dark"><?=Yii::t('app', 'Детей');?></label>
                                        <?php echo $form->field($model, 'children')->dropDownList($countPeople)->label(false);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label"><?=Yii::t('app','Имя')?></label>
                                        <?=$form->field($model, 'name')->textInput(['placeholder' => Yii::t('app','Имя')])->label(false); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <?=$form->field($model, 'email')->textInput(['placeholder' => 'Email', 'type' => 'email'])->label(false); ?>
<!--                                        <input type="email" class="form-control" placeholder="Email Address">-->
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label"><?=Yii::t('app','Телефонный номер')?></label>
                                        <?=$form->field($model, 'phone')->textInput(['placeholder' => 'Телефонный номер', 'type' => 'number'])->label(false); ?>
<!--                                        <input type="number" class="form-control" placeholder="Number">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="float-right mb-5 mb-lg-0">
                        <button class="btn btn-lg btn-success"><?=Yii::t('app', 'Отправить')?></button>
                    </div>
                <?php ActiveForm::end();?>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Terms And Conditions</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-unstyled widget-spec mb-0">
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Money Not Refundable
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>You can renew your Premium ad after experted.
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings are active for depend on package.
                            </li>
                            <li class="ml-5 mb-0">
                                <a href="tips.html"> View more..</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Benefits Of Premium Ad</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-unstyled widget-spec mb-0">
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings Active
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings are displayed on top
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>Premium listings will be Show in Google results
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-0 overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Safety Tips</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i> Meet Seller at public Place
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i> Check item before you buy
                            </li>
                            <li>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i> Pay only after collecting item
                            </li>
                            <li class="ml-5 mb-0">
                                <a href="tips.html"> View more..</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Add posts-section-->