<?php foreach ($comments as $comment):?>
    <li class="d-flex">
        <?php if($comment->user && $comment->user->image): ?>
            <img class="w-7 h-7 brround tour-before bg-primary" alt="64x64" src="/image/<?=$comment->user->image?>">
        <?php else: ?>
            <img src="../../assets/images/faces/male/1.jpg" alt="" class="w-8 h-8 brround tour-before bg-primary">
        <?php endif; ?>
        <div class="timeline-data ml-5" style="width:100%">
            <div class="media-body" style="display:inherit">
                <h5 class="mt-0 mb-1 font-weight-semibold2"><?php echo $comment->name ?>
                    <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="verified"><i class="fa fa-check-circle-o text-success"></i></span>
                    <?php if($comment->user && $comment->user->rating): ?>
                        <span class="fs-14 ml-2"> <?=$comment->user->rating->count_stars?> <i class="fa fa-star text-yellow"></i></span>
                    <?php endif;?>
                </h5>
                <small class="text-muted"><i class="fe fe-calendar"></i> <?=common\components\helper\aDate::getFormatter(strtotime($comment->date), 'short')?>  <i class=" ml-3 fe fe-clock"></i> <?=date('H:m', strtotime($comment->date))?> </small>
                <p class="font-13  mb-2 mt-2">
                    <?=$comment->message?>
                </p>
                <div class="ml-auto float-sm-right mt-1">
                    <a class="btn btn-light btn-sm reply-comment" data-id="<?=$comment->id?>" href="#" ><i class="fa fa-reply"></i> Reply</a>
                </div>
            </div>
            <?php if($comment->child): ?>
                <?php foreach ($comment->child as $comChild): ?>
                    <div class="d-sm-flex p-4 border br-5 mt-3 timeline-reply-data">
                        <div class="d-flex mr-3">
                            <?php if($comChild->user && $comChild->user->image): ?>
                                <img class="w-7 h-7 brround tour-before bg-primary" alt="64x64" src="/image/<?=$comChild->user->image?>">
                            <?php else: ?>
                                <img class="w-7 h-7 brround tour-before bg-primary" alt="64x64" src="../../assets/images/faces/female/2.jpg">
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                            <h5 class="mt-0 mb-1 font-weight-semibold2"><?=$comChild->name?> <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="verified"><i class="fa fa-check-circle-o text-success"></i></span></h5>
                            <p class="font-13  mb-2 mt-2">
                                <?=$comChild->message; ?>
                            </p>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif; ?>
        </div>
    </li>
<?php endforeach; ?>
