<?php

use \yii\helpers\Url;
use \yii\widgets\ActiveForm;
use \common\models\Rating;

?>
<!--Add listing-->
<div class="relative pt-10 pb-10 pattern2 bg-background-color bg-background cover-image pb-9" data-image-src="../../assets/images/banners/banner16.jpg">
    <div class="header-text1 mb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 d-block mx-auto">
                    <div class="text-center text-white ">
                        <h1 class="mb-2"><span class="font-weight-semibold"><?=$hotel->title?></span></h1>
                        <div class="star-ratings start-ratings-main clearfix mx-auto mb-2 mt-3 d-flex banner-ratings">
                            <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                <?php
                                $countReviews = 0;
                                if ($ratingStars):
                                    foreach ($ratingStars as $ratingStar){
                                        $countReviews++;
                                    }
                                endif; ?>
                                <select class="example-fontawesome" name="rating" autocomplete="off">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4" selected>4</option>
                                    <option value="5">5</option>
                                </select>
                            </div> <a class="text-white" href="#"><?=$countReviews?> <?=Yii::t('app', 'отзыва');?></a>
                        </div>
                        <ul class="social-icons mb-4 ml-auto">
                            <li>
                                <a class="social-icon" href=""><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a class="social-icon" href=""><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a class="social-icon" href=""><i class="fa fa-youtube"></i></a>
                            </li>
                            <li>
                                <a class="social-icon" href=""><i class="fa fa-map-marker"></i></a>
                            </li>
                        </ul>
<!--                        <a class="btn btn-info mb-1 mt-1" href="#"><i class="fa fa-heart-o"></i> Add Wishlist</a>-->
                        <a class="btn btn-success mb-1 mt-1 reply-comment" href="#" data-id="0"><i class="fa fa-star"></i> <?=Yii::t('app', 'Написать отзыв')?></a>
                        <a href="#" class="btn btn-danger icons mb-1 mt-1" data-toggle="modal" data-target="#report"><i class="icon icon-exclamation mr-1"></i> <?=Yii::t('app', 'Сообщить о нарушении')?></a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /header-text -->
    <div class="details-absolute">
        <div class="d-sm-flex container">
            <div><a href="#" class="text-white d-inline-block mr-4 "><i class="fe fe-map-pin text-white mr-2"></i><?=$hotel->country->title ?>,<?= $hotel->region->title?></a></div>
            <div class="ml-auto"><a href="#" class="text-white d-inline-block mt-2 mt-sm-0"><i class="fe fe-phone text-white mr-2 fs-14"></i>468 865 9658</a></div>
        </div>
    </div>
</div>
<!--/Sliders Section-->

<!--BreadCrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title"><?=Yii::t('app', 'Санаторий'); ?> <?=$hotel->title ?></h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['/'])?>"><?=Yii::t('app', 'Главная'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?=Url::to(['/hotels'])?>"><?=Yii::t('app', 'Санатории'); ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=Yii::t('app', 'Санаторий'); ?> <?=$hotel->title ?></li>
            </ol>
        </div>
    </div>
</div>
<!--/BreadCrumb-->

<!--Add listing-->
<section class="sptb">
    <div class="container body-content">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">
                <!--Tours And Travels Overview-->
                <div class="card overflow-hidden details-accordion">
                    <div class="card-body">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 card-header bg-transparent" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <?=Yii::t('app', 'Описание'); ?>
                                </a>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="mb-0">
                                            <p class="leading-loose"><?=$hotel->body; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <?=Yii::t('app', 'Характеристики'); ?>
                                </a>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="table-responsive">
                                            <table class="table row table-borderless w-100 m-0 text-nowrap">
                                                <tbody class="col-lg-12 col-xl-6 p-0">
                                                    <tr>
<!--                                                        --><?php //die(pr($hotel->type_hotel));?>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Тип отеля')?> :</span> <?=$hotel->type_hotel && isset(Yii::$app->params['type_hotel'][$hotel->type_hotel]) ? Yii::$app->params["type_hotel"][$hotel->type_hotel]: ' -' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Локация')?> :</span> <?=$hotel->country->title?><?=$hotel->city ? ' - '.$hotel->city->title : ''; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'мин. возраст')?> :</span> <?=$hotel->min_age ? $hotel->min_age : '-'; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'макс. возраст')?> :</span> <?=$hotel->max_age ? $hotel->max_age : '-'; ?></td>
                                                    </tr>
                                                </tbody>
                                                <tbody class="col-lg-12 col-xl-6 p-0">
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Extra people')?> :</span> <?=$hotel->extra; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Identity Proof')?> :</span> <?=$hotel->identify; ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Минимальное пребывание')?> :</span> 2 Nights</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="px-0"><span class="font-weight-semibold"><?=Yii::t('app', 'Предоплата')?> :</span> <?=$hotel->deposite; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($hotel->included && !empty($hotel->included->getValue())):?>
                                <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <?=Yii::t('app', 'Включено'); ?>
                                </a>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="table-responsive item-card2-desc">
                                            <table class="table row table-borderless w-100 m-0 text-nowrap">
                                                <?php $included = Yii::$app->params['included']; ?>
                                                <?php $countParams = count($included); ?>
                                                <?php $counterParams = 0; ?>
                                                <tbody class="col-lg-12 col-xl-6 p-0">
                                                    <?php if($hotel->included && !empty($hotel->included->getValue())):?>
                                                        <?php foreach ($hotel->included->getValue() as $includedId):?>
                                                            <tr>
                                                                <td class="px-0"><i class="fe fe-check text-success d-inline-block bg-success-transparent mr-2"></i>  <?=$included[$includedId]?></td>
                                                            </tr>
                                                            <?php unset($included[$includedId]);?>
                                                            <?php $counterParams++;?>
                                                            <?php if(ceil($countParams/2) == $counterParams):?>
                                                                </tbody>
                                                                <tbody>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if($typeRoomsHotel):?>
                                <div class="card">
                                    <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                        <?=Yii::t('app', 'Типы комнат'); ?>
                                    </a>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                        <div class="card-body leading-normal-2">
                                            <div class="row row-deck">
                                                <?php $counterTypeRoom = 1; ?>
                                                <?php foreach ($typeRoomsHotel as $typeRoom):?>
                                                    <div class="col-sm-6 col-lg-3">
                                                        <div class="text-center mb-4 mb-lg-0">
                                                            <?php if($typeRoom->image):?>
                                                                <a class=""><img src="<?=$typeRoom->image?>" alt="img"></a>
                                                            <?php endif; ?>
                                                            <h6 class="mb-0 p-3 border border-top-0"><?=$typeRoom->title?></h6>
                                                        </div>
                                                    </div>
                                                    <?php $counterTypeRoom++; ?>
                                                    <?php if($counterTypeRoom == 3 && $counterTypeRoom > 4):?>
                                                        <?php break; ?>
                                                    <?php endif; ?>
                                                <?php endforeach;?>
                                                <?php if($typeRoomsHotelCount > 4):?>
                                                    <div class="col-sm-6 col-lg-3">
                                                        <div class="text-center mb-4 mb-sm-0 bg-light border p-4 relative w-100">
                                                            <a class="absolute-link2" href="#"><span class="absolute-span">View all</span></a>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                    <?=Yii::t('app', 'Наличие комнат'); ?>
                                </a>

                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <?php foreach ($typeRoomsHotel as $typeRoom):?>
                                            <?php foreach ($typeRoom->rooms as $room):?>
                                                <div class="card shadow-none">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <?=$room->title ?>
                                                        </div>
                                                    </div>
                                                    <div class="card-body relative hotel-card">
                                                        <div class="hotel-absolute"></div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <?php if($room->image): ?>
                                                                    <div class="">
                                                                        <img src="<?=Yii::$app->params['uploadDir'].$room->image?>" alt="img" class="cover-image br-5">
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="card-body p-0">
                                                                    <p class="fs-14 mt-0 mb-2 leading-normal"><?=$room->description?></p>
                                                                    <div class="row  hotel-features ">
                                                                        <?php if($room->services && !empty($room->services->getValue())):?>
                                                                            <?php foreach ($room->services->getValue() as $service): ?>
                                                                                <div class="col-sm-6 mb-2 d-flex">
                                                                                    <div class="h6 ml-2 mt-2"> <?=Yii::$app->params['services'][$service]?></div>
                                                                                </div>
                                                                            <?php endforeach;?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach;?>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header" data-toggle="collapse" id="headingSix" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                    <?=Yii::t('app', 'Информация'); ?>
                                </a>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                                    <div class="card-body leading-normal-2">
                                        <div class="table-responsive">
                                            <table class="table mb-0 table-bordered-0 text-nowrap w-100">
                                                <tbody>
                                                <?php if($hotel->established): ?>
                                                    <tr>
                                                        <td class="font-weight-semibold px-0">Established Year</td>
                                                        <td class="px-0"><?=$hotel->established?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php if($hotel->services && !empty($hotel->services->getValue())): ?>
                                                    <tr>
                                                        <td class="font-weight-semibold px-0">Services</td>
                                                        <?php $services = array();?>
                                                        <?php foreach ($hotel->services->getValue() as $service): ?>
                                                            <?php $services[] = Yii::$app->params['services'][$service]; ?>
                                                        <?php endforeach; ?>
                                                        <td class="px-0"><?= implode(', ', $services) ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php if($hotel->payment_methods && !empty($hotel->payment_methods->getValue())): ?>
                                                    <tr>
                                                        <td class="font-weight-semibold px-0"><?=Yii::t('app', 'Метод оплаты'); ?></td>
                                                        <?php $paymentMethods = array();?>
                                                        <?php foreach ($hotel->payment_methods->getValue() as $paymentMethod): ?>
                                                            <?php $paymentMethods[] = Yii::$app->params['payment_methods'][$paymentMethod]; ?>
                                                        <?php endforeach; ?>
                                                        <td class="px-0"><?=implode(', ', $paymentMethods)?></td>
                                                    </tr>
                                                <?php endif;?>
                                                <?php if($hotel->certification): ?>
                                                    <tr>
                                                        <td class="font-weight-semibold px-0">Certification</td>
                                                        <td class="px-0"><?=$hotel->certification; ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($hotel->video): ?>
                                <div class="card">
                                    <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header" data-toggle="collapse" id="headingSeven" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                        Hotel Video
                                    </a>
                                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                                        <div class="card-body leading-normal-2">
                                            <iframe width="560" height="400" src="<?=$hotel->video?>  allow="accelerometer; autoplay;" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(!empty($hotel->images->getValue())): ?>
                                <div class="card mb-0">
                                    <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header" data-toggle="collapse" id="headingEight" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                        <?=Yii::t('app', 'Галерея'); ?>
                                    </a>
                                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                                        <div class="card-body leading-normal-2">
                                            <div class="product-slider">
                                                <div id="carousel2" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <?php $counter = 1; ?>
                                                        <?php foreach ($hotel->images->getValue() as $image): ?>
                                                            <div class="carousel-item <?=$counter == 1 ? 'active' : ''; ?> "><img src="<?=$image?>" alt="img"> </div>
                                                            <?php $counter++;?>
                                                        <?php endforeach; ?>
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carousel2" role="button" data-slide="prev">
                                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carousel2" role="button" data-slide="next">
                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="clearfix">
                                                    <div id="thumbcarousel2" class="carousel slide product-slide-thumb" data-interval="false">
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <?php $counter = 0;?>
                                                                <?php foreach ($hotel->images->getValue() as $image): ?>
                                                                    <div data-target="#carousel2" data-slide-to="<?=$counter++?>" class="thumb"><img src="<?=$image?>" alt="img"></div>
                                                                    <?php if($counter == 5):?>
                                                                        </div>
                                                                        <div class="carousel-item ">
                                                                    <?php  endif; ?>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                        <a class="carousel-control-prev" href="#thumbcarousel2" role="button" data-slide="prev">
                                                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                        </a>
                                                        <a class="carousel-control-next" href="#thumbcarousel2" role="button" data-slide="next">
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="icons">
                            <a href="<?=Url::to(['hotel/'.$hotel->url.'/order'])?>" class="btn btn-primary px-5"><?=Yii::t('app', 'Забронировать')?></a>
                            <a href="#" class="btn btn-light icons"><i class="fe fe-share-2 fs-16 leading-normal"></i></a>
<!--                            <a href="#" class="btn btn-light icons"><i class="fe fe-heart fs-16 leading-normal"></i></a>-->
                        </div>
                    </div>
                </div>

                <?php if($relatedHotels): ?>
                    <h3 class="mt-6 mb-4 fs-20">Related Hotels</h3>

                    <!--Related Posts-->
                    <div class="row">
                        <div id="myCarousel5" class="owl-carousel owl-carousel-icons3">
                            <!-- Wrapper for carousel items -->
                            <?php foreach($relatedHotels as $relatedHotel):?>
                                <div class="item">
                                    <div class="card overflow-hidden">
                                        <div class="ieh-100">
                                            <div class="item-card9-img w-100">
                                                <?php if($relatedHotel->video):?>
                                                    <div class="card">
                                                        <a href="#" class="fs-16 font-weight-semibold2 collapsed card-header" data-toggle="collapse" id="headingSeven" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                                            Hotel Video
                                                        </a>
                                                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                                                            <div class="card-body leading-normal-2">
                                                                <iframe width="560" height="400" src="<?=$hotel->video?>  allow="accelerometer; autoplay;" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php elseif(count($relatedHotel->images->getValue()) > 1):?>
                                                    <div id="image-slider1" class="carousel" data-ride="carousel">
                                                        <?php $counter = 1; ?>
                                                        <?php foreach ($relatedHotel->images->getValue() as $image):?>
                                                            <div class="carousel-inner">
                                                                <div class="carousel-item <?=$counter == 1 ? 'active' : ''?>">
                                                                    <img src="<?=$image?>" alt="img" class="cover-image">
                                                                </div>
                                                            </div>
                                                            <?php $counter++; ?>
                                                        <?php endforeach;?>
                                                        <a class="carousel-control-prev left-0" href="#image-slider1" role="button" data-slide="prev">
                                                            <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="carousel-control-next right-0" href="#image-slider1" role="button" data-slide="next">
                                                            <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                <?php elseif($relatedHotel->image):?>
                                                    <div class="item-card9-imgs">
                                                        <img src="<?=$relatedHotel->image?>" alt="img" class="cover-image">
                                                        <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                            <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image"></i> 5</span>
                                                        </div>
                                                        <?php if($relatedHotel->priceRoom): ?>
                                                            <div class="item-card7-overlaytext">
                                                                <h4 class="mb-0 py-2 fs-20"><?=$relatedHotel->priceRoom[0]->price?></h4>
                                                            </div>
                                                        <?php endif;?>

                                                    </div>
                                                <?php endif; ?>
                                                <div class="item-card9-icons">
                                                    <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                                </div>
                                            </div>
                                            <div class="card border-0 mb-0 br-0">
                                                <div class="card-body">
                                                    <div class="item-card9">
                                                        <div class="d-sm-flex">
                                                            <div class="">
                                                                <?php if($relatedHotel->categories):?>
                                                                    <?php $counter = 1;?>
                                                                    <?php foreach ($relatedHotel->categories as $category):?>
                                                                        <span class="badge badge-pill badge-light" style="text-align:inherit;margin-bottom: 7px;white-space:inherit;width: 330px;"><?=$category->title?></span>
                                                                        <?php if($counter == 2){break;}?>
                                                                        <?php $counter++?>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                                <a href="<?=Url::to('hotel/'.$relatedHotel->url)?>" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal"><?=$relatedHotel->title?> </h4></a>
                                                                <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> <?=$relatedHotel->country->title?> <?=$relatedHotel->city ? $relatedHotel->city->title : ''; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="fs-14 mt-1 mb-2 leading-normal"><?=$relatedHotel->description?></p>
                                                    <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                        <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                            <select class="example-fontawesome" name="rating" autocomplete="off">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4" selected>4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div> <a class="fs-13 leading-tight mt-1" href="#">75 Reviews</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!--/Related Posts-->
                <?php endif; ?>
                <!--Faqs-->
                <h3 class="mt-6 mb-4 fs-20">FAQS</h3>
                <div class="details-accordion mb-5">
                    <div class="accordion" id="accordionExample2">
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 card-header bg-transparent" id="headingOne1" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                What does it Cost to advertise?
                            </a>
                            <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingTwo1" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo1">
                                What Business Categories do you offer?
                            </a>
                            <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingThree1" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="true" aria-controls="collapseThree1">
                                How do I pay for my tour Listing ad?
                            </a>
                            <div id="collapseThree1" class="collapse" aria-labelledby="headingThree1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFour1" data-toggle="collapse" data-target="#collapseFour1" aria-expanded="true" aria-controls="collapseFour1">
                                Do you offer frequency Discounts?
                            </a>
                            <div id="collapseFour1" class="collapse" aria-labelledby="headingFour1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingFive1" data-toggle="collapse" data-target="#collapseFive1" aria-expanded="true" aria-controls="collapseFive1">
                                How will I know if people are responding to my tour Listing ad?
                            </a>
                            <div id="collapseFive1" class="collapse" aria-labelledby="headingFive1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <a href="#" class="btn-link fs-16 font-weight-semibold2 collapsed card-header bg-transparent" id="headingSix1" data-toggle="collapse" data-target="#collapseSix1" aria-expanded="true" aria-controls="collapseSix1">
                                I Know I want to place  an ad,but I still have questions.What should I do?
                            </a>
                            <div id="collapseSix1" class="collapse" aria-labelledby="headingSix1" data-parent="#accordionExample2">
                                <div class="card-body leading-normal-2">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words </p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Faqs-->

                <!--Comments-->
                <div class="card mt-6">
                    <div class="card-header">
                        <h3 class="card-title">Rating And Reviews</h3>
                    </div>
                    <?php if($ratingStars): ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $sumCountReviews = 0;?>
                                    <?php foreach ($ratingStars as $ratingStar): ?>
                                        <?php $sumCountReviews += $ratingStar['count_reviews'];?>
                                    <?php endforeach;?>
                                    <?php foreach ($ratingStars as $ratingStar): ?>
                                        <div class="mb-5">
                                            <div class="d-flex"><div class="badge badge-default bg-light mb-2 text-primary"><?=$ratingStar['count_stars']?> <i class="fa fa-star"></i></div> <div class="ml-auto font-weight-semibold2 fs-16"><?=$ratingStar['count_reviews']?></div></div>
                                            <div class="progress progress-md mb-4 h-1">
                                                <div class="progress-bar bg-primary" style="width: <?=ceil($ratingStar['count_reviews'] * 100 / $sumCountReviews )?>%"></div>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php if($countComments):?>
                        <div class="card-body">
                            <h4 class="font-weight-semibold2 text-secondary"><?=$countComments?> Reviews</h4>
                            <ul class="timeline-tour mt-5">
                                <?= $this->render('_comments', ['comments' => $comments])?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <!--/Comments-->

                <div class="card mb-lg-0">
                    <div class="card-header">
                        <h3 class="card-title">Add a Review</h3>
                    </div>
                    <?php $modelRatingStars = new \common\models\RatingStars(); ?>
                    <?php $modelRatingStars->hotel_id = $hotel->id; ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'form-stars'
                    ]);?>
                        <?=$form->field($modelRatingStars, 'hotel_id')->hiddenInput()->label(false); ?>
                        <div class="card-body">
                            <div class="d-flex">
                                <p class="mb-0 fs-14 font-weight-semibold">Over All Rating</p>
                                <div class="star-ratings start-ratings-main clearfix ml-5">
                                    <div class="stars stars-example-fontawesome">
                                        <select  class="example-fontawesome" name="Rating[count_stars]" autocomplete="off" id="rating-stars">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                    <div class="card-body">
                        <div id="block-form-comment">
                            <?php $modelComments = new \common\models\Comments(); ?>
                            <?php $modelComments->hotel_id = $hotel->id; ?>
                            <?php echo $this->render('../page/_form_comment', ['model' => $modelComments]); ?>
                        </div>
                        <a href="#" class="btn btn-secondary add-comment" >Send Reply</a>
                    </div>
                </div>
            </div>

            <!--Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Hotel Organizer</h3>
                    </div>
                    <div class="card-body item-user">
                        <div class="profile-details">
                            <div class="profile-pic mb-0 mx-5">
                                <img src="../../assets/images/other/logo.jpg" class="brround w-150 h-150" alt="user">
                            </div>
                        </div>
                        <div class="text-center mt-2">
                            <a href="userprofile.html" class="text-dark text-center"><h4 class="mt-0 mb-1 font-weight-semibold2">Robert McLean</h4></a>
                            <span class="text-muted mt-1">Organizer <b> Since November 2008</b></span>
                            <div><small class="text-muted">Listing Hotel Id <b>365241</b></small></div>
                        </div>
                    </div>
                    <div class="card-body item-user">
                        <h4 class="mb-4">Contact Info</h4>
                        <div>
                            <h6><span class="font-weight-semibold"><i class="fa fa-map mr-3 mb-2"></i></span><a href="#" class="text-body"> Mp-214, New York, NY 10012</a></h6>
                            <h6><span class="font-weight-semibold"><i class="fa fa-envelope mr-3 mb-2"></i></span><a href="#" class="text-body"> robert123@gmail.com</a></h6>
                            <h6><span class="font-weight-semibold"><i class="fa fa-phone mr-3 mb-2"></i></span>0-235-657-24587</h6>
                            <h6><span class="font-weight-semibold"><i class="fa fa-link mr-3 "></i></span><a href="#" class="text-secondary">https://spruko.com/</a></h6>
                        </div>
                    </div>
                </div>
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Posted By</h3>
                    </div>
                    <div class="card-body item-user">
                        <div class="profile-details">
                            <div class="profile-pic mb-0 mx-5">
                                <img src="../../assets/images/faces/female/13.jpg" class="brround border w-auto" alt="user">
                            </div>
                        </div>
                        <div class="text-center mt-2 product-filter-desc">
                            <a href="userprofile.html" class="text-dark text-center"><h4 class="mt-0 mb-1 font-weight-semibold2">Lilly Jones </h4></a>
                            <span class="text-muted">Hotel Manager</span>
                            <div class="mt-3">
                                <a href="#" class="px-2"><i class="fa fa-facebook fs-18 text-primary"></i></a>
                                <a href="#" class="px-2"><i class="fa fa-linkedin fs-18 text-warning"></i></a>
                                <a href="#" class="px-2"><i class="fa fa-instagram fs-18 text-info"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <div class="">
                            <a href="#" class="btn btn-secondary mt-1 mb-1 mr-2" data-toggle="modal" data-target="#contact"><i class="fa fa-phone"></i> Contact Me</a>
                            <a href="personal-blog.html" class="btn btn-light mt-1 mb-1"><i class="fa fa-eye"></i> See All Post</a>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Details Shares Thorugh</h3>
                    </div>
                    <div class="card-body product-filter-desc">
                        <div class="product-filter-icons text-center">
                            <a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="bg-linkedin"><i class="fa fa-linkedin-square"></i></a>
                            <a href="#" class="btn-instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="btn-vk"><i class="fa fa-vk"></i></a>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Search Hotels</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <input type="text" class="form-control keywords-input" id="search-text" placeholder="Search Hotels">
                        </div>
                        <div class="form-group">
                            <select name="country" id="select-countries" class="form-control custom-select select2-show-search">
                                <option>Hotel Location</option>
                                <option>Australia</option>
                                <option>Chicago</option>
                                <option>Germany</option>
                                <option>India</option>
                                <option>Japan</option>
                                <option>London</option>
                                <option>Los angels</option>
                                <option>Spain</option>
                                <option>Sidney</option>
                            </select>
                        </div>
                        <div >
                            <a href="#" class="btn  btn-block btn-secondary">Search</a>
                        </div>
                    </div>
                </div>
                <h3 class="mt-6 mb-4 fs-20">Recent Posts</h3>
                <div class="recent-posts overflow-hidden">
                    <div class="">
                        <div id="myCarousel4" class="owl-carousel testimonial-owl-carousel2">
                            <div class="item">
                                <div class="card overflow-hidden mb-0">
                                    <div class="ieh-100">
                                        <div class="item-card9-img w-100">
                                            <div class="item-card9-imgs">
                                                <img src="../../assets/images/products/products/h1.jpg" alt="img" class="cover-image">
                                                <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                    <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 5</span>
                                                </div>
                                                <div class="item-card7-overlaytext">
                                                    <h4 class="mb-0 py-2 fs-20">$145</h4>
                                                </div>
                                            </div>
                                            <div class="item-card9-icons">
                                                <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                        <div class="card border-0 mb-0 br-0">
                                            <div class="card-body">
                                                <div class="item-card9">
                                                    <div class="d-sm-flex">
                                                        <div class="">
                                                            <span class="badge badge-pill badge-light">Accomdation</span>
                                                            <span class="badge badge-pill badge-light">Hotel</span>
                                                            <a href="hotels.html" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal">New Residential Somik Hotel </h4></a>
                                                            <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> Mp-214, New York, NY 10012</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="fs-14 mt-1 mb-2 leading-normal">We denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>
                                                <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> <a class="fs-13 leading-tight mt-1" href="#">75 Reviews</a>
                                                </div>
                                                <div class="text-dark font-weight-normal mb-0 mt-4 hotel-features ">
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="5 Star Hotel"><i class="fa fa-h-square d-inline-block text-secondary"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Wifi"><i class="fa fa-wifi d-inline-block text-success"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Shower Bathroom"><i class="fa fa-shower d-inline-block text-info"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Food"><i class="fa fa-cutlery d-inline-block text-blue"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card overflow-hidden mb-0">
                                    <div class="ieh-100">
                                        <div class="item-card9-img w-100">
                                            <div id="image-slider2" class="carousel" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="../../assets/images/products/products/h2.jpg" alt="img" class="cover-image">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="../../assets/images/products/products/h3.jpg" alt="img" class="cover-image">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="../../assets/images/products/products/h4.jpg" alt="img" class="cover-image">
                                                    </div>
                                                </div>
                                                <a class="carousel-control-prev left-0" href="#image-slider2" role="button" data-slide="prev">
                                                    <i class="carousel-control-prev-icon fa fa-angle-left"></i>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next right-0" href="#image-slider2" role="button" data-slide="next">
                                                    <i class="carousel-control-next-icon fa fa-angle-right"></i>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                            <div class="item-card9-icons">
                                                <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0 py-2 fs-20">$742</h4>
                                            </div>
                                        </div>
                                        <div class="card border-0 mb-0 br-0">
                                            <div class="card-body">
                                                <div class="item-card9">
                                                    <div class="d-sm-flex">
                                                        <div class="">
                                                            <span class="badge badge-pill badge-light">Accomdation</span>
                                                            <span class="badge badge-pill badge-light">Hotel</span>
                                                            <a href="hotels.html" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal">Special Resdential Hotels </h4></a>
                                                            <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> Mp-214, New York, NY 10012</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="fs-14 mt-1 mb-2 leading-normal">We denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>
                                                <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> <a class="fs-13 leading-tight mt-1" href="#">42 Reviews</a>
                                                </div>
                                                <div class="text-dark font-weight-normal mb-0 mt-4 hotel-features ">
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="5 Star Hotel"><i class="fa fa-h-square d-inline-block text-secondary"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Wifi"><i class="fa fa-wifi d-inline-block text-success"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Shower Bathroom"><i class="fa fa-shower d-inline-block text-info"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Food"><i class="fa fa-cutlery d-inline-block text-blue"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card overflow-hidden mb-0">
                                    <div class="ieh-100">
                                        <div class="item-card9-img w-100">
                                            <div class="item-card9-imgs">
                                                <a class="d-block video-btn1 mx-auto" href="#" data-toggle="modal" data-target="#homeVideo"><span class="fa fa-play text-white"></span></a>
                                                <img src="../../assets/images/products/products/h3.jpg" alt="img" class="cover-image">
                                            </div>
                                            <div class="item-card9-icons">
                                                <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0 py-2 fs-20">$547</h4>
                                            </div>
                                        </div>
                                        <div class="card border-0 mb-0 br-0">
                                            <div class="card-body">
                                                <div class="item-card9">
                                                    <div class="d-sm-flex">
                                                        <div class="">
                                                            <span class="badge badge-pill badge-light">Accomdation</span>
                                                            <span class="badge badge-pill badge-light">Hotel</span>
                                                            <a href="hotels.html" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal">New Somic Hotels </h4></a>
                                                            <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> Mp-214, New York, NY 10012</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="fs-14 mt-1 mb-2 leading-normal">We denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>
                                                <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> <a class="fs-13 leading-tight mt-1" href="#">161 Reviews</a>
                                                </div>
                                                <div class="text-dark font-weight-normal mb-0 mt-4 hotel-features ">
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="5 Star Hotel"><i class="fa fa-h-square d-inline-block text-secondary"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Wifi"><i class="fa fa-wifi d-inline-block text-success"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Shower Bathroom"><i class="fa fa-shower d-inline-block text-info"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Food"><i class="fa fa-cutlery d-inline-block text-blue"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card overflow-hidden mb-0">
                                    <div class="ieh-100">
                                        <div class="item-card9-img w-100">
                                            <div class="item-card9-imgs">
                                                <img src="../../assets/images/products/products/h4.jpg" alt="img" class="cover-image">
                                                <div class="item-card2-img1" data-toggle="modal" data-target="#gallery">
                                                    <span class="badge bg-dark-transparent6 text-white fs-14 font-weight-semibold2"><i class="fe fe-image "></i> 5</span>
                                                </div>
                                            </div>
                                            <div class="item-card9-icons">
                                                <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0 py-2 fs-20">$475</h4>
                                            </div>
                                        </div>
                                        <div class="card border-0 mb-0 br-0">
                                            <div class="card-body">
                                                <div class="item-card9">
                                                    <div class="d-sm-flex">
                                                        <div class="">
                                                            <span class="badge badge-pill badge-light">Accomdation</span>
                                                            <span class="badge badge-pill badge-light">Hotel</span>
                                                            <a href="hotels.html" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal">Five Star Hotel </h4></a>
                                                            <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> Mp-214, New York, NY 10012</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="fs-14 mt-1 mb-2 leading-normal">We denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>
                                                <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> <a class="fs-13 leading-tight mt-1" href="#">87 Reviews</a>
                                                </div>
                                                <div class="text-dark font-weight-normal mb-0 mt-4 hotel-features ">
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="5 Star Hotel"><i class="fa fa-h-square d-inline-block text-secondary"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Wifi"><i class="fa fa-wifi d-inline-block text-success"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Shower Bathroom"><i class="fa fa-shower d-inline-block text-info"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Food"><i class="fa fa-cutlery d-inline-block text-blue"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card overflow-hidden mb-0">
                                    <div class="ieh-100">
                                        <div class="item-card9-img w-100">
                                            <div class="item-card9-imgs">
                                                <img src="../../assets/images/products/products/h5.jpg" alt="img" class="cover-image">
                                            </div>
                                            <div class="item-card9-icons">
                                                <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0 py-2 fs-20">$635</h4>
                                            </div>
                                        </div>
                                        <div class="card border-0 mb-0 br-0">
                                            <div class="card-body">
                                                <div class="item-card9">
                                                    <div class="d-sm-flex">
                                                        <div class="">
                                                            <span class="badge badge-pill badge-light">Accomdation</span>
                                                            <span class="badge badge-pill badge-light">Hotel</span>
                                                            <a href="hotels.html" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal">Hotel Residential Smoik</h4></a>
                                                            <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> Mp-214, New York, NY 10012</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="fs-14 mt-1 mb-2 leading-normal">We denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>
                                                <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> <a class="fs-13 leading-tight mt-1" href="#">96 Reviews</a>
                                                </div>
                                                <div class="text-dark font-weight-normal mb-0 mt-4 hotel-features ">
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="5 Star Hotel"><i class="fa fa-h-square d-inline-block text-secondary"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Wifi"><i class="fa fa-wifi d-inline-block text-success"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Shower Bathroom"><i class="fa fa-shower d-inline-block text-info"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Food"><i class="fa fa-cutlery d-inline-block text-blue"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="card overflow-hidden mb-0">
                                    <div class="ieh-100">
                                        <div class="item-card9-img w-100">
                                            <div class="item-card9-imgs">
                                                <a class="d-block video-btn1 mx-auto" href="#" data-toggle="modal" data-target="#homeVideo"><span class="fa fa-play text-white"></span></a>
                                                <img src="../../assets/images/products/products/h1.jpg" alt="img" class="cover-image">
                                            </div>
                                            <div class="item-card9-icons">
                                                <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                                            </div>
                                            <div class="item-card7-overlaytext">
                                                <h4 class="mb-0 py-2 fs-20">$635</h4>
                                            </div>
                                        </div>
                                        <div class="card border-0 mb-0 br-0">
                                            <div class="card-body">
                                                <div class="item-card9">
                                                    <div class="d-sm-flex">
                                                        <div class="">
                                                            <span class="badge badge-pill badge-light">Accomdation</span>
                                                            <span class="badge badge-pill badge-light">Hotel</span>
                                                            <a href="hotels.html" class="text-dark"><h4 class="font-weight-semibold2 mt-1 mb-2 leading-normal">Residential Hotel Xina</h4></a>
                                                            <a href="#" class="item-card2-desc mt-1 mb-0 text-dark"><i class="fe fe-map-pin mr-1 d-inline-block"></i> Mp-214, New York, NY 10012</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="fs-14 mt-1 mb-2 leading-normal">We denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>
                                                <div class="star-ratings start-ratings-main clearfix d-flex mb-2">
                                                    <div class="stars stars-example-fontawesome stars-example-fontawesome-sm mr-2">
                                                        <select class="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected>4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div> <a class="fs-13 leading-tight mt-1" href="#">125 Reviews</a>
                                                </div>
                                                <div class="text-dark font-weight-normal mb-0 mt-4 hotel-features ">
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="5 Star Hotel"><i class="fa fa-h-square d-inline-block text-secondary"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Wifi"><i class="fa fa-wifi d-inline-block text-success"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Shower Bathroom"><i class="fa fa-shower d-inline-block text-info"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Free Food"><i class="fa fa-cutlery d-inline-block text-blue"></i></a>
                                                    <a class="" href="#" data-toggle="tooltip" data-placement="top" title="Get Directions"><i class="fa fa-map-signs d-inline-block text-warning"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body">
                        <h4 class="font-weight-semibold2">Need Help for any Details?</h4>
                        <div class="support-service bg-light br-5 mt-4">
                            <i class="fa fa-phone bg-primary text-white"></i>
                            <h6 class="text-default font-weight-bold mt-1">+68 872-627-9735</h6>
                            <p class="text-default mb-0 fs-12">Free Support!</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Side Content-->
        </div>
    </div>
</section

<!--Comment Modal -->
<div class="modal fade" id="Comment" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Leave a Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="reply-comment">
                <?php echo $this->render('../page/_form_comment', ['model' => $modelComments]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success add-reply-comment">Send</button>
            </div>
        </div>
    </div>
</div>

<!--Map Modal -->
<div class="modal fade" id="Map-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Direction</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="h-400">
                    <!--Map-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" class="h-100 w-100 border-0"></iframe>
                    <!--/Map-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Report Modal -->
<div class="modal fade" id="report" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplereportLongTitle">Report Abuse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php  $modelComplaint = new \common\models\Complaint(); ?>
            <div id="block-form-report">
                <?php echo $this->render('../page/_form_complaint', ['modelComplaint' => $modelComplaint]); ?>
            </div>
        </div>
    </div>
</div>

<!-- Message Modal -->
<div class="modal fade" id="contact" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplecontactLongTitle">Send Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" id="contact-name" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="contact-email" placeholder="Email Address">
                </div>
                <div class="form-group mb-0">
                    <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Message"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Send</button>
            </div>
        </div>
    </div>
</div>

<!--  Modal Popup -->
<div class="modal fade" id="homeVideo" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="pauseVid()"><i class="fe fe-x"></i></button>
            <!--<iframe width="560" height="400" src="https://www.youtube.com/embed/kFjETSa9N7A"  allow="accelerometer; autoplay;" allowfullscreen></iframe>-->
            <div class="embed-responsive embed-responsive-16by9">
                <video id="gossVideo" class="embed-responsive-item" controls="controls">
                    <source src="../../assets/video/1.mp4" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
</div>

<!--  Modal to login -->
<div class="modal fade" id="to-login" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            Для оценки отеля вам нужно <a href="<?=Url::to(['login/'])?>">авторизоваться</a> или <a href="<?=Url::to(['registration/'])?>">зарегистрироваться</a>
        </div>
    </div>
</div>

<!--  Modal to login -->
<div class="modal fade" id="success-comment" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            спасибо за комент
        </div>
    </div>
</div>

<!--  Modal Popup -->
<div class="modal fade" id="gallery" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="product-slider modal-body p-2">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <a class="gallery-close-button" href="#" data-dismiss="modal" aria-label="Close"><i class="fe fe-x"></i></a>
                    <div class="carousel-inner">
                        <div class="carousel-item active"><img src="../../assets/images/places/1.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/2.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/3.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/4.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/5.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/1.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/2.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/3.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/4.jpg" alt="img"> </div>
                        <div class="carousel-item"><img src="../../assets/images/places/5.jpg" alt="img"> </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="clearfix">
                    <div id="thumbcarousel" class="carousel slide product-slide-thumb" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="../../assets/images/places/1.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="../../assets/images/places/2.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="../../assets/images/places/3.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="../../assets/images/places/4.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="../../assets/images/places/5.jpg" alt="img"></div>

                            </div>
                            <div class="carousel-item ">
                                <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="../../assets/images/places/1.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="../../assets/images/places/2.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="../../assets/images/places/3.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="../../assets/images/places/4.jpg" alt="img"></div>
                                <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="../../assets/images/places/5.jpg" alt="img"></div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#thumbcarousel" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a>
                        <a class="carousel-control-next" href="#thumbcarousel" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" ><i class="fe fe-arrow-up"></i></a>

<?php
    $this->registerJs('
        $(document).ready(function() { 
            $("#add-complaint").on("click", function () {
                $.ajax({
                    method: "POST",
                    url: "'.Url::to(['page/add-complaint']).'",
                    data: $("#form-complaint").serialize(),
                    success: function(res){
                        console.log(res);
                        if(res == "ok"){
                            $("#report").modal("hide");
                        } else {
                            $("#block-form-report").html(res);
                        }                    
                    }
                })
            });
            
            $("#rating-stars").on("change", function(){
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: "'.Url::to(['page/add-rating-stars']).'",
                    data: $("#form-stars").serialize(),
                    success: function(res){
                        console.log(res);
                        if(res.status != "ok"){
                            $("#to-login").modal({
                                show: true
                            });
                        }                   
                    }
                });
            });
            
            $("body").on("click", ".add-comment",function(e){
                e.preventDefault();
                var data = $(this).parent().find("form").serialize();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: "'.Url::to(['page/add-comment']).'",
                    data: data,
                    success: function(res){
                        $("#block-form-comment").html(res.form);
                        if(res.status == "ok"){                        
                            $("#success-comment").modal({
                                show: true
                            });
                        }                   
                    }
                });
            });
        });
        
        $(".reply-comment").on("click", function(e){
            e.preventDefault();
            var commentId = $(this).data("id");
            $("#reply-comment").find(".reply_parent").val(commentId);            
            $("#Comment").modal({
                show: true
            });
        });
        
        $(".add-reply-comment").on("click", function(e){
            e.preventDefault();
            data = $(this).parents(".modal-content:first").find("form").serialize();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: "'.Url::to(['page/add-comment']).'",
                data: data,
                success: function(res){
                    $("#reply-comment").html(res.form);
                    if(res.status == "ok"){                        
                        $("#Comment").modal("hide");                        
                        $("#success-comment").modal({
                            show: true
                        });
                    }                   
                }
            });
        });
        ',
        \yii\web\View::POS_END,
        'form-complaint'
    );
?>
<script>

</script>