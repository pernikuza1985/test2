<div class="cover-image sptb  py-9 bg-background" data-image-src="../../assets/images/banners/banner16.jpg">
    <div class="header-text1 mb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
                    <div class="text-center text-white ">
                        <h2 class="fs-40">
<!--                            <span class="font-weight-bold">96,758</span>-->
                            Hotels available
                        </h2>
                    </div>
                    <div class="search-background">
                        <div class="form row no-gutters" id="data-search">
                            <div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0">
                                <input type="text" class="form-control location-input border-right br-tr-0 br-br-0 input-lg" placeholder="Search for Hotels">
                                <span><img src="../../assets/images/svgs/gps.svg" class="location-gps" alt="img"></span>
                            </div>
                            <div class="form-group  col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg border-right bg-light-30">
                                <div class="input-group d-flex align-items-center">
                                    <span class="label mr-2 text-default mt-2 fs-14">Adults</span>
                                    <span class="input-group-btn minus"><i class="fe fe-minus"></i></span>
                                    <input type="text" name="adults" id="filter-adults" class="form-control input-lg text-center qty px-0 form-filter" value="1">
                                    <span class="input-group-btn add mr-2"><i class="fe fe-plus"></i></span>
                                </div>
                            </div>
                            <div class="form-group  col-xl-2 col-lg-2 col-md-12 mb-0 select2-lg bg-light-30">
                                <div class="input-group d-flex align-items-center">
                                    <span class="label mr-2 text-default mt-2 fs-14">Child</span>
                                    <span class="input-group-btn minus"><i class="fe fe-minus"></i></span>
                                    <input type="text" name="child" id="filter-child" class="form-control input-lg text-center qty px-0  form-filter" value="1">
                                    <span class="input-group-btn add mr-2"><i class="fe fe-plus"></i></span>
                                </div>
                            </div>
                            <div class="form-group col-xl-5 col-lg-5  col-md-12 mb-0 location">
                                <div class="row no-gutters">
                                    <div class="form-group  col-xl-6 col-lg-6 col-md-12 mb-0 border-right border-left">
                                        <input class="form-control daterange-btn br-0 input-lg form-filter" name="date"  id="filter-date"  placeholder="Checkin" type="text">
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 mb-0">
                                        <span class="btn btn-block btn-secondary fs-14 br-tl-0 br-bl-0 shadow-none btn-lg" id="starch-filter" href="#">Search</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /header-text -->
</div>
<!--/Sliders Section-->

<!--Breadcrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Hotel List 02</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Hotel List 02</li>
            </ol>
        </div>
    </div>
</div>
<!--/Breadcrumb-->

<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!--Add lists-->
                <?php echo $this->render('_list', ['hotels' => $hotels]);?>
                <!--/Add lists-->
            </div>
            <?php echo $this->render('_filter');?>
        </div>
    </div>
</section>
<!--/Add Listings-->
<?php
    $this->registerJs('        
        $("#starch-filter").on("click", function () {
            var url = "";
            var adults = $("#filter-adults").val();
            var child = $("#filter-child").val();
            var date = $("#filter-date").val();
            if(adults){
                url += "adults="+adults+"/";
            }   
            if(child){
                url += "child="+child+"/";
            }   
            if(date){
                date = date.replace(/\s/g, "");
                date = date.replace(/\//g, ":");
                url += "date="+date+"/";
            }
            alert("'.\yii\helpers\Url::to('hotels/').'"+url);
            location = "'.\yii\helpers\Url::to('/hotels').'/"+url;
            
        })'

    );
?>
