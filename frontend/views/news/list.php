<?php
use \yii\helpers\Url;

$this->title = Yii::t('app', 'Новости');

$this->params['breadcrumbs'][] = $this->title;
?>
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">
                <!--Add lists-->
                <div class="row">
                    <?php foreach ($newsList as $news):?>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-6">
                        <div class="card overflow-hidden">
                            <div class="d-md-flex  ieh-100">
                                <div class="item2-card-img relative br-tr-0">
                                    <a href="blog-details.html"></a>
                                    <img src="<?=\common\components\helper\ImageHelper::path($news->image)?>" alt="img" class="cover-image h-100 w-100 br-tr-0">
                                    <div class="date-blog bg-primary">
                                        <h4 class="mb-0 font-weight-semibold2"><?=date('d', strtotime($news->date_create));?></h4>
<!--                                        <small>--><?//=\common\components\helper\aDate:: ?><!--</small>-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <small class="text-muted">Posted By: <span class="text-default">Royal Hamblin</span></small>
                                    <a href="blog-details.html" class="text-dark leading-normal"><h4 class="font-weight-semibold2 mt-1 leading-normal">We had lot of Enjoyment in Tour. Its A Great Trip</h4></a>
                                    <p>On the other hand, we denounce with righteous indignation and dislike </p>
                                    <a class="btn-link" href="<?=Url::to(['news/'.$news->url]);?>"><?=Yii::t('app', 'Подробнее');?> <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
<!--                <div class="d-sm-flex">-->
<!--                    <h6 class="mt-3 mb-4 mb-sm-0">Showing 1 to 6 of 30 entries</h6>-->
<!--                    <ul class="pagination mb-lg-0 mb-5 ml-auto">-->
<!--                        <li class="page-item page-prev disabled">-->
<!--                            <a class="page-link" href="#" tabindex="-1">Prev</a>-->
<!--                        </li>-->
<!--                        <li class="page-item active"><a class="page-link" href="#">1</a></li>-->
<!--                        <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--                        <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--                        <li class="page-item page-next">-->
<!--                            <a class="page-link" href="#">Next</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                ]);
                ?>
                <!--/Add lists-->
            </div>

            <!--Side Content-->
<!--            <div class="col-xl-4 col-lg-4 col-md-12">-->
<!--                <div class="card">-->
<!--                    <div class="card-body">-->
<!--                        <div class="input-group">-->
<!--                            <input type="text" class="form-control br-tl-5  br-bl-5" placeholder="Search">-->
<!--                            <div class="input-group-append ">-->
<!--                                <button type="button" class="btn btn-secondary br-tr-5  br-br-5">-->
<!--                                    Search-->
<!--                                </button>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="card">-->
<!--                    <div class="card-header">-->
<!--                        <h3 class="card-title">Categories</h3>-->
<!--                    </div>-->
<!--                    <div class="card-body p-0">-->
<!--                        <div class="list-catergory">-->
<!--                            <div class="item-list">-->
<!--                                <ul class="list-group mb-0">-->
<!--                                    <li class="list-group-item">-->
<!--                                        <a href="#" class="text-dark">-->
<!--                                            <i class="fa fa-hotel bg-secondary text-secondary"></i> Hotels-->
<!--                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1 mt-1">14</span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="list-group-item">-->
<!--                                        <a href="#" class="text-dark">-->
<!--                                            <i class="fa fa-cutlery bg-success text-success"></i> Restaurants-->
<!--                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">63</span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="list-group-item">-->
<!--                                        <a href="#" class="text-dark">-->
<!--                                            <i class="fa fa-car bg-info text-info"></i> Car-->
<!--                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">25</span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="list-group-item">-->
<!--                                        <a href="#" class="text-dark">-->
<!--                                            <i class="fa fa-ship bg-warning text-warning"></i> Ships-->
<!--                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">74</span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="list-group-item">-->
<!--                                        <a href="#" class="text-dark">-->
<!--                                            <i class="fa fa-train bg-danger text-danger"></i> Train-->
<!--                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">18</span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="list-group-item">-->
<!--                                        <a href="#" class="text-dark">-->
<!--                                            <i class="fa fa-plane bg-blue text-blue"></i> Flight-->
<!--                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">32</span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <!--/ Side Content-->
        </div>
    </div>
</section>
<!--All Listing-->
