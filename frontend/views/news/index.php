<?php
use yii\widgets\ActiveForm;
?>
<!--Add listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="font-weight-semibold2 text-capitalize"><?=$newsModel->title?></h4>
                        <?=$newsModel->seo_text?>
                    </div>
                </div>


                <!--Comments-->
                <div class="card mt-6">
                    <div class="card-header">
                        <h3 class="card-title"><?=Yii::t('app', 'Рейтинг и отзывы')?></h3>
                    </div>
                    <?php if($ratingStars): ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $sumCountReviews = 0;?>
                                    <?php foreach ($ratingStars as $ratingStar): ?>
                                        <?php $sumCountReviews += $ratingStar['count_reviews'];?>
                                    <?php endforeach;?>
                                    <?php foreach ($ratingStars as $ratingStar): ?>
                                        <div class="mb-5">
                                            <div class="d-flex"><div class="badge badge-default bg-light mb-2 text-primary"><?=$ratingStar['count_stars']?> <i class="fa fa-star"></i></div> <div class="ml-auto font-weight-semibold2 fs-16"><?=$ratingStar['count_reviews']?></div></div>
                                            <div class="progress progress-md mb-4 h-1">
                                                <div class="progress-bar bg-primary" style="width: <?=ceil($ratingStar['count_reviews'] * 100 / $sumCountReviews )?>%"></div>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php if($countComments):?>
                        <div class="card-body">
                            <h4 class="font-weight-semibold2 text-secondary"><?=$countComments?> <?=Yii::t('app', 'Отзывы')?></h4>
                            <ul class="timeline-tour mt-5">
                                <?= $this->render('_comments', ['comments' => $comments])?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <!--/Comments-->

                <div class="card mb-lg-0">
                    <div class="card-header">
                        <h3 class="card-title"><?=Yii::t('app', 'Добавить отзыв');?></h3>
                    </div>
                    <?php $modelRatingStars = new \common\models\RatingStars(); ?>
                    <?php $modelRatingStars->hotel_id = $hotel->id; ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'form-stars'
                    ]);?>
                    <?=$form->field($modelRatingStars, 'hotel_id')->hiddenInput()->label(false); ?>
                    <div class="card-body">
                        <div class="d-flex">
                            <p class="mb-0 fs-14 font-weight-semibold"><?=Yii::t('app', 'Рейтинг');?></p>
                            <div class="star-ratings start-ratings-main clearfix ml-5">
                                <div class="stars stars-example-fontawesome">
                                    <select  class="example-fontawesome" name="Rating[count_stars]" autocomplete="off" id="rating-stars">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div class="card-body">
                        <div id="block-form-comment">
                            <?php $modelComments = new \common\models\Comments(); ?>
                            <?php $modelComments->news_id = $newsModel->id; ?>
                            <?php echo $this->render('../page/_form_comment', ['model' => $modelComments]); ?>
                        </div>
                        <a href="#" class="btn btn-secondary add-comment" ><?=Yii::t('app','Отправить')?></a>
                    </div>
                </div>
            </div>
            <!--Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="input-group">
                            <input type="text" class="form-control br-tl-5  br-bl-5" placeholder="Search">
                            <div class="input-group-append ">
                                <button type="button" class="btn btn-secondary br-tr-5  br-br-5">
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Categories</h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="list-catergory">
                            <div class="item-list">
                                <ul class="list-group mb-0">
                                    <li class="list-group-item">
                                        <a href="#" class="text-dark">
                                            <i class="fa fa-hotel bg-secondary text-secondary"></i> Hotels
                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1 mt-1">14</span>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#" class="text-dark">
                                            <i class="fa fa-cutlery bg-success text-success"></i> Restaurants
                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">63</span>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#" class="text-dark">
                                            <i class="fa fa-car bg-info text-info"></i> Car
                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">25</span>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#" class="text-dark">
                                            <i class="fa fa-ship bg-warning text-warning"></i> Ships
                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">74</span>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#" class="text-dark">
                                            <i class="fa fa-train bg-danger text-danger"></i> Train
                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">18</span>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#" class="text-dark">
                                            <i class="fa fa-plane bg-blue text-blue"></i> Flight
                                            <span class="badgetext badge badge-pill badge-light mb-0 mt-1">32</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Popular Tour Tags</h3>
                    </div>
                    <div class="card-body">
                        <div class="product-tags clearfix">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#">Mountains</a></li>
                                <li><a href="#">Flyings</a></li>
                                <li><a href="#">Seas</a></li>
                                <li><a href="#">Historical</a></li>
                                <li><a href="#">Climbing</a></li>
                                <li><a href="#">Snow Views</a></li>
                                <li><a href="#">7 Wonders</a></li>
                                <li><a href="#">Pyramids</a></li>
                                <li><a href="#">Deserts</a></li>
                                <li><a href="#">Forests</a></li>
                                <li><a href="#" class="mb-0">World Tour</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Side Content-->
        </div>
    </div>
</section>
<!--/Add listing-->
